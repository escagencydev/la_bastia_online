<?php
/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file viene utilizzato, durante l’installazione, dallo script
 * di creazione di wp-config.php. Non è necessario utilizzarlo solo via
 * web, è anche possibile copiare questo file in «wp-config.php» e
 * riempire i valori corretti.
 *
 * Questo file definisce le seguenti configurazioni:
 *
 * * Impostazioni MySQL
 * * Prefisso Tabella
 * * Chiavi Segrete
 * * ABSPATH
 *
 * È possibile trovare ulteriori informazioni visitando la pagina del Codex:
 *
 * @link https://codex.wordpress.org/it:Modificare_wp-config.php
 *
 * È possibile ottenere le impostazioni per MySQL dal proprio fornitore di hosting.
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - È possibile ottenere queste informazioni dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define( 'DB_NAME', "la_bastia_online" );

/** Nome utente del database MySQL */
define( 'DB_USER', "root" );

/** Password del database MySQL */
define( 'DB_PASSWORD', "" );

/** Hostname MySQL  */
define( 'DB_HOST', "localhost" );

/** Charset del Database da utilizzare nella creazione delle tabelle. */
define( 'DB_CHARSET', 'utf8mb4' );


/** Il tipo di Collazione del Database. Da non modificare se non si ha idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * È possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * È possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ciò forzerà tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Ru=P@Le+ES+@.2Z3(A:;^w`/a%t`&MDlxmgy8K5z_CU:,6k/d-=cb&_`)6S8!^QL' );
define( 'SECURE_AUTH_KEY',  'ADbr=J3ryMW[nu.m&eHf^G_1Xtu0L`)Mt4ZnMHq2ms8KDyF2/.V[ kdur~OT3|xt' );
define( 'LOGGED_IN_KEY',    'O@`5aWF-bPs>ti/]~2(R_2}*js69;#yn d[Y@?]CpE%j(A:]FFvs%`q?RnOY|Wnv' );
define( 'NONCE_KEY',        'v3{lE{?^,v-JdW$c~o+U[gkMB-u|.8/KbznOb6_L#tIQ9BiH Dc7:yK4B[ Axdis' );
define( 'AUTH_SALT',        'u9]P7~ @=7qb9{6/<ojK!UI47FR21y`uG^@FgltXLWq;vG0%5x63X(^S4YH@R%h#' );
define( 'SECURE_AUTH_SALT', 'zF:jx`^zM3|f.w!686NS2(H=GUCG51[x>sFiN/&5$2|XhE#vrm81?Ki79QCUgz+j' );
define( 'LOGGED_IN_SALT',   'Os9.n%p%wE>12MY1p;@jO&GTi4mD=J>P+x$o&i  W:6uRsM_w )f}AP-kZ2MU%U,' );
define( 'NONCE_SALT',       'cRR/B%>lfEnKJ!Gc T[[76l;w0E]y!7C[MkjRC8,|I ?J4o{cS7|RQd2R-hga#Z|' );


/**#@-*/

/**
 * Prefisso Tabella del Database WordPress.
 *
 * È possibile avere installazioni multiple su di un unico database
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix = 'wp_';


/**
 * Per gli sviluppatori: modalità di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi
 * durante lo sviluppo.
 * È fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all’interno dei loro ambienti di sviluppo.
 */
define('WP_DEBUG', false);
define('DISALLOW_FILE_EDIT', true);
define('FS_METHOD', 'direct');

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta le variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');
