<?php
/**
 * @var OTF_Menu_Setup $this
 * @var string          $welcome
 */
?>
<div class="wrap about-wrap opal-wrap">
    <h1><?php esc_attr_e( 'Welcome to', 'opal-theme-framework' ); ?> <?php echo ucwords( apply_filters( 'pbrthemer_import_theme', 'pbrthemer' )); ?>!</h1>
    <div class="opal-welcome-tour">
      <a href="http://www.wpopal.com" title="Welcome to Opal WordPress Themes">
      <img src="<?php echo plugins_url( 'pbrthemer/assets/css/opal_banner.jpg' );?>">
    </a>
        <div class="col three-col">
            <div class="col">
                <h3>Welcome To Opal Team</h3>
                <p>Since our inception in 2014 we have managed to collect several awards by releasing high-quality WordPress theme that exceed expectations and push boundaries. Our biggest secret in unprece-dented customer support</p>
            </div>
            <div class="col">
                <h3>Powerful Customization Tools</h3>
                <p>We are WordPress provider professional WordPress Themes that are simple to use and customize. Our themes are built in our great framework theme and widgets, plugins. </p>
            </div>
            <div class="col last-feature last">
                <h3>5 Star Customer Support</h3>
                <p>Since we are living across different time-zones. It is possible that you will find us online and yet not replying to your queries. To enable better support we have our ticketing system installed and email channel. We’ll have you a reply within 24 hours except during weekends.</p>
            </div>
        </div>
        <div class="">
            <h3>Contact with us</h3>
              If you need any technical support please <a href="https://wpopal.ticksy.com/submit/" rel="nofollow">submit a ticket</a>
              <br>
              <h5>Quick Email Contact</h5>
              <a href="mailto:info@wpopal.com">info@wpopal.com – Contact To Get Any Information</a><br>
              <a href="mailto:help@wpopal.com">help@wpopal.com – Contact To Get Help/Support</a>

          </div>
    </div>
</div>