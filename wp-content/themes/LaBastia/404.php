<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WPOPAL
 * @subpackage royanwine
 * @since Royanwine 1.0
 */
/*
*Template Name: 404 Page
*/

get_header( apply_filters( 'royanwine_fnc_get_header_layout', null ) ); ?>

<?php do_action( 'royanwine_template_main_before' ); ?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php
				if (royanwine_fnc_theme_options( '404_post' )):
					$query = new WP_Query( 'page_id=' . royanwine_fnc_theme_options( '404_post' ) );
					if ($query->have_posts()):
						while ($query->have_posts()) : $query->the_post();
							the_content();
						endwhile;
					endif;
				else: ?>
					<div class="title">
						<span><?php esc_html_e('404', 'royanwine'); ?></span>
						<span class="sub"><?php esc_html_e( 'Not Found', 'royanwine' ); ?></span>
					</div>
					<div class="error-description">
						<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try a search?', 'royanwine' ); ?></p>
					</div><!-- .page-content -->

					<div class="page-action text-center">
						<a class="btn btn-lg btn-default" href="javascript: history.go(-1)"><?php esc_html_e('Go back to previous page', 'royanwine'); ?></a>
						<a class="btn btn-lg btn-default" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e('Return to homepage', 'royanwine'); ?></a>
					</div>
				<?php endif;
			?>
		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_footer();
