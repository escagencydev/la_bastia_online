<?php
/**
 * The Template for displaying all single posts
 *
 * @package WPOPAL
 * @subpackage royanwine
 * @since Royanwine 1.0
 */

$royanwine_page_layouts = apply_filters( 'royanwine_fnc_get_single_sidebar_configs', null );

get_header( apply_filters( 'royanwine_fnc_get_header_layout', null ) );
//do_action( 'royanwine_template_main_before' );
?>
<!-- header -->

<div class="wp-block-getwid-section header-sezione getwid-section-content-full-width">
	<div class="wp-block-getwid-section__wrapper">
		<div class="wp-block-getwid-section__inner-wrapper">
			<div class="wp-block-getwid-section__background-holder">
				<div class="wp-block-getwid-section__background" style="background-image:url('<?php echo wp_get_attachment_url(9651); ?>')">
					<div class="wp-block-getwid-section__background-image-wrapper">
						<img class="wp-block-getwid-section__background-image" src="<?php echo wp_get_attachment_url(9651); ?>" alt="">
					</div>
				</div>
				<div class="wp-block-getwid-section__foreground"></div>
			</div>
			<div class="wp-block-getwid-section__content">
				<div class="wp-block-getwid-section__inner-content">
					<div class="wp-block-getwid-section getwid-section-content-custom-width">
						<div class="wp-block-getwid-section__wrapper">
							<div class="wp-block-getwid-section__inner-wrapper" style="max-width:1200px">
								<div class="wp-block-getwid-section__background-holder">
									<div class="wp-block-getwid-section__background"></div>
									<div class="wp-block-getwid-section__foreground"></div>
								</div>
								<div class="wp-block-getwid-section__content">
									<div class="wp-block-getwid-section__inner-content">
										<h2 class="has-very-light-gray-color has-text-color title-cover-claim">Blog</h2>
										<div class="breadcrumbs " typeof="BreadcrumbList" vocab="https://schema.org/">
										<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to La Bastìa." href="<?php echo site_url(); ?>" class="home"><span property="name">La Bastìa</span></a><meta property="position" content="1"></span> &gt; <span class="post post-page"><a href="<?php echo get_permalink(9420); ?>">Blog</a></span> &gt; <span class="post post-page current-item"><?php echo get_the_title(); ?></span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<section id="main-container" class="container <?php echo apply_filters( 'royanwine_template_main_content_class', royanwine_fnc_theme_options('blog-single-layout') ); ?>">
	<div class="row">

		<div id="main-content" class="main-content col-sm-12 <?php echo esc_attr($royanwine_page_layouts['main']['class']); ?>">

			<div id="primary" class="content-area">
				<div id="content" class="site-content" role="main">
					
					<?php
						// Start the Loop.
						while ( have_posts() ) : the_post();

							/*
							 * Include the post format-specific template for the content. If you want to
							 * use this in a child theme, then include a file called called content-___.php
							 * (where ___ is the post format) and that will be used instead.
							 */
							get_template_part( 'content', get_post_format() );

                            // if(get_the_author_meta('description')){
                            //     get_template_part( 'page-templates/parts/author-bio');
                            // }

                            // Previous/next post navigation.
                            // royanwine_fnc_post_nav();

                            // If comments are open or we have at least one comment, load up the comment template.
                            // if ( comments_open() || get_comments_number() ) {
                            //     comments_template();
                            // }

							/*if( royanwine_fnc_theme_options('blog-show-related-post', true) ): ?>
							<div class="related-posts">
								<?php
				                    $relate_count = royanwine_fnc_theme_options('blog-items-show', 3);
				                    royanwine_fnc_related_post($relate_count, 'post', 'category');
			                    ?>
                            </div>
							<?php endif;*/ ?>
			                <?php

						endwhile;
					?>
				</div><!-- #content -->
			</div><!-- #primary -->
		</div>

        <?php if( isset($royanwine_page_layouts['sidebars']) && !empty($royanwine_page_layouts['sidebars']) ) : ?>
            <?php get_sidebar(); ?>
        <?php endif; ?>

	</div>
</section>

<!-- contatti -->

<?php echo do_shortcode("[reblex id='9558']"); ?>

<!-- footer -->

<div class="wp-block-getwid-section footer getwid-section-content-full-width">
	<div class="wp-block-getwid-section__wrapper">
		<div class="wp-block-getwid-section__inner-wrapper">
			<div class="wp-block-getwid-section__background-holder">
				<div class="wp-block-getwid-section__background has-background" style="background-color:#1d232a"></div>
				<div class="wp-block-getwid-section__foreground"></div>
			</div>
			<div class="wp-block-getwid-section__content">
				<?php echo do_shortcode("[reblex id='9444']"); ?>
			</div>
		</div>
	</div>
</div>
<?php
get_footer();
