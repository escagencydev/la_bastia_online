<?php
/**
 * The template for displaying Category pages
 *
 * @link http://wpopal.com/themes/royanwine
 *
 * @package WPOPAL
 * @subpackage royanwine
 * @since Royanwine 1.0
 */
global $royanwine_page_layouts;
$royanwine_page_layouts = apply_filters( 'royanwine_fnc_get_category_sidebar_configs', null );
$columns = royanwine_fnc_theme_options('blog-archive-column', 1);
$bscol = floor( 12 / $columns );
$_count  = 0;


get_header( apply_filters( 'royanwine_fnc_get_header_layout', null ) ); ?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
		<?php if ( have_posts() ) : ?>

			<!-- header -->

			<div class="wp-block-getwid-section header-sezione getwid-section-content-full-width">
				<div class="wp-block-getwid-section__wrapper">
					<div class="wp-block-getwid-section__inner-wrapper">
						<div class="wp-block-getwid-section__background-holder">
							<div class="wp-block-getwid-section__background" style="background-image:url('<?php echo wp_get_attachment_url(9651); ?>')">
								<div class="wp-block-getwid-section__background-image-wrapper">
									<img class="wp-block-getwid-section__background-image" src="<?php echo wp_get_attachment_url(9651); ?>" alt="">
								</div>
							</div>
							<div class="wp-block-getwid-section__foreground"></div>
						</div>
						<div class="wp-block-getwid-section__content">
							<div class="wp-block-getwid-section__inner-content">
								<div class="wp-block-getwid-section getwid-section-content-custom-width">
									<div class="wp-block-getwid-section__wrapper">
										<div class="wp-block-getwid-section__inner-wrapper" style="max-width:1200px">
											<div class="wp-block-getwid-section__background-holder">
												<div class="wp-block-getwid-section__background"></div>
												<div class="wp-block-getwid-section__foreground"></div>
											</div>
											<div class="wp-block-getwid-section__content">
												<div class="wp-block-getwid-section__inner-content">
													<h2 class="has-very-light-gray-color has-text-color title-cover-claim">Blog</h2>
													<div class="breadcrumbs " typeof="BreadcrumbList" vocab="https://schema.org/">
													<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to La Bastìa." href="<?php echo site_url(); ?>" class="home"><span property="name">La Bastìa</span></a><meta property="position" content="1"></span> &gt; <span class="post post-page"><a href="<?php echo get_permalink(9420); ?>">Blog</a></span> &gt; <span class="post post-page current-item"><?php echo single_cat_title( '', false ); ?></span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<!-- contenuto -->

			<div class="wp-block-getwid-section lista-blog getwid-section-content-custom-width">
				<div class="wp-block-getwid-section__wrapper">
					<div class="wp-block-getwid-section__inner-wrapper" style="max-width:1200px">
					<div class="wp-block-getwid-section__background-holder">
						<div class="wp-block-getwid-section__background"></div>
						<div class="wp-block-getwid-section__foreground"></div>
					</div>
					<div class="wp-block-getwid-section__content">
						<div class="wp-block-getwid-section__inner-content">
						<?php 
						echo do_shortcode('[ajax_load_more theme_repeater="css-grid.php" button_label="Mostra altri post" button_loading_label="Caricando..." container_type="div" transition_container_classes="css-grid" posts_per_page="9"]'); 
						?>
						</div>
					</div>
				</div>
			</div>

		<?php else :
			// If no content, include the "No posts found" template.
			get_template_part( 'content', 'none' );

		endif;
		?>

		<!-- contatti -->

		<?php echo do_shortcode("[reblex id='9558']"); ?>
					

		<!-- footer -->

		<div class="wp-block-getwid-section footer getwid-section-content-full-width">
			<div class="wp-block-getwid-section__wrapper">
				<div class="wp-block-getwid-section__inner-wrapper">
					<div class="wp-block-getwid-section__background-holder">
						<div class="wp-block-getwid-section__background has-background" style="background-color:#1d232a"></div>
						<div class="wp-block-getwid-section__foreground"></div>
					</div>
					<div class="wp-block-getwid-section__content">
						<?php echo do_shortcode("[reblex id='9444']"); ?>
					</div>
				</div>
			</div>
		</div>

		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_footer();