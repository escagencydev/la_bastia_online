<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WPOPAL
 * @subpackage royanwine
 * @since Royanwine 1.0
 */
$footer_profile = apply_filters( 'royanwine_fnc_get_footer_profile', 'default' );
if ($footer_profile){
    $footer       = get_post( $footer_profile );
    $class_footer = isset( $footer->post_name ) ? $footer->post_name : 'default';
} else{
    $class_footer = 'default';
}
?>

</section><!-- #main -->
<?php do_action( 'royanwine_template_main_after' ); ?>
<footer id="pbr-footer" class="pbr-footer <?php echo esc_attr( $class_footer ); ?>" role="contentinfo">
    <div class="inner">
        <?php if ($footer_profile && $footer_profile != 'default') : ?>
            <div class="pbr-footer-profile">
                <?php royanwine_fnc_render_post_content( $footer_profile ); ?>
            </div>
        <?php else: ?>
            <?php get_sidebar( 'footer' ); ?>
        <?php endif; ?>
    </div>
    <a href="#" class="scrollup">
    <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2020/04/wheat.svg" alt="" class="img-scrollup">
    </a>
</footer><!-- #colophon -->


<?php do_action( 'royanwine_template_footer_after' ); ?>
<?php get_sidebar( 'offcanvas' ); ?>
</div>
</div>
<!-- #page -->

<?php wp_footer(); ?>
</body>
</html>