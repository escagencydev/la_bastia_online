jQuery(document).ready(function($) {
    $('a[href^="#"]').on('click', function(event) {
        if ($(this).hasClass('ui-tabs-anchor')) {
            event.preventDefault();
            return false;
        } else {
            var target = $(this.getAttribute('href'));
            if( target.length ) {
                event.preventDefault();
                $('html, body').stop().animate({
                    scrollTop: target.offset().top
                }, 1000);
            }
        }
    });
}); 