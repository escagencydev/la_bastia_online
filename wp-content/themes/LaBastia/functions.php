<?php
add_action( 'wp_enqueue_scripts', 'load_my_child_styles', 20 );
function load_my_child_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri().'/style.css', array('royanwine-style'), '1.0.0' );
    wp_enqueue_script( 'child-js', get_stylesheet_directory_uri().'/js/main.js', array ( 'jquery' ), '1.0.0', true);
}

// new widget area
function wpb_widgets_init() {
    register_sidebar( array(
        'name'          => 'Top mobile menu',
        'id'            => 'top-mobile-menu',
        'before_widget' => '<div class="chw-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '',
        'after_title'   => '',
    ) );
}
add_action( 'widgets_init', 'wpb_widgets_init' );

// sicurezza
function fjarrett_remove_wp_version_strings( $src ) {
	global $wp_version;
	parse_str(parse_url($src, PHP_URL_QUERY), $query);
	if ( !empty($query['ver']) && $query['ver'] === $wp_version ) {
		$src = remove_query_arg('ver', $src);
	}
	return $src;
}
add_filter( 'script_loader_src', 'fjarrett_remove_wp_version_strings' );
add_filter( 'style_loader_src', 'fjarrett_remove_wp_version_strings' );

function wpbeginner_remove_version() {
	return '';
}
add_filter('the_generator', 'wpbeginner_remove_version');

function plc_comment_post( $incoming_comment ) {
	$incoming_comment['comment_content'] = htmlspecialchars( $incoming_comment['comment_content'] );
	$incoming_comment['comment_content'] = str_replace( "'", '&apos;', $incoming_comment['comment_content'] );
	return( $incoming_comment );
}
function plc_comment_display( $comment_to_display ) {
	$comment_to_display = str_replace( '&apos;', "&#039;", $comment_to_display );
	return $comment_to_display;
}
add_filter( 'preprocess_comment', 'plc_comment_post', '', 1);
add_filter( 'comment_text', 'plc_comment_display', '', 1);
add_filter( 'comment_text_rss', 'plc_comment_display', '', 1);
add_filter( 'comment_excerpt', 'plc_comment_display', '', 1);
remove_filter( 'comment_text', 'make_clickable', 9 );
// end sicurezza

// fix blocchi gutemberg
add_action('admin_head', 'my_custom_fonts');
function my_custom_fonts() {
  echo '<style>
    .block-editor .wp-block {
    	max-width: 70%;
	}
	.block-editor .wp-block {
		background-color: rgba(0,0,0,0.05);
	}
  </style>';
}