<section id="pbr-topbar" class="pbr-topbar hidden-xs hidden-sm">
	<div class="container">
        <div class="topbar-inner d-flex justify-content-between flex-wrap">
            <?php if (has_nav_menu( 'social' )) : ?>
                <nav class="social-navigation" aria-label="<?php esc_attr_e( 'Topbar Social Links Menu', 'royanwine' ); ?>">
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'social',
                        'menu_class'     => 'social-links-menu',
                        'depth'          => 1,
                        'link_before'    => '<span class="screen-reader-text">',
                        'link_after'     => '</span><i class="fa fa-link" aria-hidden="true"></i>',
                    ) );
                    ?>
                </nav><!-- .social-navigation -->
            <?php endif; ?>
            <div class="box-user open">
                <span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
                  role="link"><?php esc_html_e( 'Account ', 'royanwine' ); ?><span
                        class="caret"></span></span>
                <?php do_action( 'opal-account-buttons' ); ?>
            </div>
        </div>
    </div>
</section>

