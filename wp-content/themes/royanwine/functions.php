<?php
/**
 * Royanwine functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * @link https://codex.wordpress.org/Plugin_API
 *
 * @package WPOPAL
 * @subpackage royanwine
 * @since Royanwine 1.0
 */
define( 'ROYANWINE_THEME_VERSION', '1.0' );
/**
 * Set up the content width value based on the theme's design.
 *
 * @see royanwine_fnc_content_width()
 *
 * @since Royanwine 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 474;
}

if ( ! function_exists( 'royanwine_fnc_setup' ) ) :
/**
 * Royanwine setup.
 *
 * Set up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support post thumbnails.
 *
 * @since Royanwine 1.0
 */
function royanwine_fnc_setup() {

	/*
	 * Make Royanwine available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on Royanwine, use a find and
	 * replace to change 'royanwine' to the name of your theme in all
	 * template files.
	 */
	load_theme_textdomain( 'royanwine', get_template_directory() . '/languages' );

	// This theme styles the visual editor to resemble the theme style.
 

	// Add RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// Enable support for Post Thumbnails, and declare two sizes.
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 672, 372, true );
	add_image_size( 'royanwine-fullwidth', 1170, 658, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary'   => esc_html__( 'Main menu', 'royanwine' ),
		'topmenu'	=> esc_html__( 'Menu Topbar', 'royanwine' ),
		'social'	=> esc_html__( 'Social Links Menu', 'royanwine' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
	) );

	// This theme allows users to set a custom background.
	add_theme_support( 'custom-background', apply_filters( 'royanwine_fnc_custom_background_args', array(
		'default-color' => 'f5f5f5',
	) ) );

	
	// add support for display browser title
	add_theme_support( 'title-tag' );
	// This theme uses its own gallery styles.
	add_filter( 'use_default_gallery_style', '__return_false' );
	
	royanwine_fnc_get_load_plugins();

}
endif; // royanwine_fnc_setup
add_action( 'after_setup_theme', 'royanwine_fnc_setup' );


/**
 * Get Theme Option Value.
 * @param String $name : name of prameters 
 */
function royanwine_fnc_theme_options($name, $default = false) {
  
    // get the meta from the database
    $options = ( get_option( 'pbr_theme_options' ) ) ? get_option( 'pbr_theme_options' ) : null;

    
   
    // return the option if it exists
    if ( isset( $options[$name] ) ) {
        return apply_filters( "pbr_theme_options_$name", $options[ $name ] );
    }
    if( get_option( $name ) ){
        return get_option( $name );
    }
    // return default if nothing else
    return apply_filters( "pbr_theme_options_$name", $default );
}



/**
 * Adjust content_width value for image attachment template.
 *
 * @since Royanwine 1.0
 */
function royanwine_fnc_content_width() {
	if ( is_attachment() && wp_attachment_is_image() ) {
		$GLOBALS['content_width'] = 810;
	}
}
add_action( 'template_redirect', 'royanwine_fnc_content_width' );


/**
 * Require function for including 3rd plugins
 *
 */
include_once(  get_template_directory() . '/inc/plugins/class-tgm-plugin-activation.php' );
function royanwine_fnc_get_load_plugins(){

	$plugins[] =(array(
		'name'                     => esc_html__('MetaBox','royanwine'), // The plugin name
	    'slug'                     => 'meta-box', // The plugin slug (typically the folder name)
	    'required'                 => true, // If false, the plugin is only 'recommended' instead of required
	));

	$plugins[] =(array(
		'name'                     => esc_html__('WooCommerce','royanwine'), // The plugin name
	    'slug'                     => 'woocommerce', // The plugin slug (typically the folder name)
	    'required'                 => true, // If false, the plugin is only 'recommended' instead of required
	));

	$plugins[] =(array(
		'name'                     => esc_html__('MailChimp', 'royanwine'),// The plugin name
	    'slug'                     => 'mailchimp-for-wp', // The plugin slug (typically the folder name)
	    'required'                 =>  true
	));

	$plugins[] =(array(
		'name'                     => esc_html__('Contact Form 7', 'royanwine'), // The plugin name
	    'slug'                     => 'contact-form-7', // The plugin slug (typically the folder name)
	    'required'                 => true, // If false, the plugin is only 'recommended' instead of required
	));

	$plugins[] =(array(
		'name'                     => esc_html__('WPBakery Visual Composer','royanwine'), // The plugin name
		'slug'                     => 'js_composer', // The plugin slug (typically the folder name)
		'required'                 => true,
		'source'				   => esc_url('http://www.wpopal.com/thememods/js_composer.zip')
	));

	$plugins[] =(array(
		'name'                     => esc_html__('Revolution Slider', 'royanwine'), // The plugin name
        'slug'                     => 'revslider', // The plugin slug (typically the folder name)
        'required'                 => true ,
        'source'				   => get_template_directory() . '/plugins/revslider.zip',
	));

	$plugins[] =(array(
		'name'                     => esc_html__('Wpopal Themer For Themes', 'royanwine'),// The plugin name
        'slug'                     => 'pbrthemer', // The plugin slug (typically the folder name)
        'required'                 => true ,
        'source'				   => 'http://www.wpopal.com/themeframework/pbrthemer.zip'
	));	

	$plugins[] =(array(
		'name'                     => esc_html__('YITH WooCommerce Wishlist', 'royanwine'), // The plugin name
	    'slug'                     => 'yith-woocommerce-wishlist', // The plugin slug (typically the folder name)
	    'required'                 =>  true
	));

	$plugins[] =(array(
		'name'                     => esc_html__('YITH Woocommerce Compare', 'royanwine'), // The plugin name
        'slug'                     => 'yith-woocommerce-compare', // The plugin slug (typically the folder name)
        'required'                 => true
	));


	$plugins[] =(array(
		'name'                     => esc_html__('Google Web Fonts Customizer', 'royanwine'), // The plugin name
        'slug'                     => 'google-web-fonts-customizer-gwfc', // The plugin slug (typically the folder name)
        'required'                 => false ,
	));

	tgmpa( $plugins );
}

/**
 * Register three Royanwine widget areas.
 *
 * @since Royanwine 1.0
 */
function royanwine_fnc_registart_widgets_sidebars() {
	 
	register_sidebar( 
	array(
		'name'          => esc_html__( 'Sidebar Default', 'royanwine' ),
		'id'            => 'sidebar-default',
		'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'royanwine'),
		'before_widget' => '<aside id="%1$s" class="widget  clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span><span>',
		'after_title'   => '</span></span></h3>',
	));

    register_sidebar(
        array(
            'name'          => esc_html__( 'Schedule Visit Sidebar' , 'royanwine'),
            'id'            => 'schedule-visit',
            'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'royanwine'),
            'before_widget' => '<aside id="%1$s" class="widget widget-style clearfix %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        ));
	
	register_sidebar( 
	array(
		'name'          => esc_html__( 'Left Sidebar' , 'royanwine'),
		'id'            => 'sidebar-left',
		'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'royanwine'),
		'before_widget' => '<aside id="%1$s" class="widget widget-style  clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span><span>',
		'after_title'   => '</span></span></h3>',
	));
	register_sidebar(
	array(
		'name'          => esc_html__( 'Right Sidebar' , 'royanwine'),
		'id'            => 'sidebar-right',
		'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'royanwine'),
		'before_widget' => '<aside id="%1$s" class="widget widget-style clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span><span>',
		'after_title'   => '</span></span></h3>',
	));

	register_sidebar( 
	array(
		'name'          => esc_html__( 'Blog Left Sidebar' , 'royanwine'),
		'id'            => 'blog-sidebar-left',
		'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'royanwine'),
		'before_widget' => '<aside id="%1$s" class="widget widget-style clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span><span>',
		'after_title'   => '</span></span></h3>',
	));

	register_sidebar( 
	array(
		'name'          => esc_html__( 'Blog Right Sidebar', 'royanwine'),
		'id'            => 'blog-sidebar-right',
		'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'royanwine'),
		'before_widget' => '<aside id="%1$s" class="widget widget-style clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span><span>',
		'after_title'   => '</span></span></h3>',
	));

	register_sidebar( 
	array(
		'name'          => esc_html__( 'Footer Social' , 'royanwine'),
		'id'            => 'footer-social',
		'description'   => esc_html__( 'Appears in the footer section of the site.', 'royanwine'),
		'before_widget' => '<aside id="%1$s" class="clearfix %2$s footer-social">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	));


	register_sidebar( 
	array(
		'name'          => esc_html__( 'Footer 1' , 'royanwine'),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Appears in the footer section of the site.', 'royanwine'),
		'before_widget' => '<aside id="%1$s" class="widget-footer clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	));
	register_sidebar( 
	array(
		'name'          => esc_html__( 'Footer 2' , 'royanwine'),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Appears in the footer section of the site.', 'royanwine'),
		'before_widget' => '<aside id="%1$s" class="widget-footer clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	));
	register_sidebar( 
	array(
		'name'          => esc_html__( 'Footer 3' , 'royanwine'),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Appears in the footer section of the site.', 'royanwine'),
		'before_widget' => '<aside id="%1$s" class="widget-footer clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	));
}
add_action( 'widgets_init', 'royanwine_fnc_registart_widgets_sidebars' );

/**
 * Register Lato Google font for Royanwine.
 *
 * @since Royanwine 1.0
 *
 * @return string
 */
function royanwine_fnc_font_url() {
	 
	$fonts_url = '';

    $montserrat = _x( 'on', 'Montserrat font: on or off', 'royanwine' );
    $abril = _x( 'on', 'Abril Fatface font: on or off', 'royanwine' );
    $notoserif = _x( 'on', 'Noto Serif font: on or off', 'royanwine' );
    $fredoka = _x( 'on', 'Fredoka font: on or off', 'royanwine' );
    $yesteryear = _x( 'on', 'Yesteryear font: on or off', 'royanwine' );


    $font_families = array();

    $font_families[] = 'Montserrat:100,200,300,400,500,700';
    $font_families[] = 'Abril+Fatface:400';
    $font_families[] = 'Noto+Serif:400,700';
    $font_families[] = 'Fredoka+One';
	$font_families[] = 'Yesteryear';

    $query_args = array(
        'family' => ( implode( '%7C', $font_families ) ),
        'subset' => urlencode( 'latin,latin-ext' ),
    );
 		
 		 
	$protocol = is_ssl() ? 'https:' : 'http:';
    $fonts_url = add_query_arg( $query_args, $protocol .'//fonts.googleapis.com/css' );


    return esc_url_raw( $fonts_url );
}
function royanwine_fnc_admin_fonts() {
	wp_enqueue_style( 'royanwine-lato', royanwine_fnc_font_url(), array(), null );
}
add_action( 'admin_print_scripts-appearance_page_custom-header', 'royanwine_fnc_admin_fonts' );


/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Royanwine 1.0
 */
function royanwine_fnc_scripts() {
	// Add Lato font, used in the main stylesheet.
	wp_enqueue_style( 'font-family', royanwine_fnc_font_url(), array(), null );

	// Add Genericons font, used in the main stylesheet.
	wp_enqueue_style( 'font-fa', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.7.0' );
	wp_enqueue_style( 'font-moo', get_template_directory_uri() . '/css/iconmoo.css', array());

	if(isset($_GET['opal-skin']) && $_GET['opal-skin']) {
		$currentSkin = $_GET['opal-skin'];
	}else{
		$currentSkin = str_replace( '.css','', royanwine_fnc_theme_options('skin','default') );
	}

	$prefixRTL = '';
    if (is_rtl()) {
        $prefixRTL = 'rtl-';
    }

	if( !empty($currentSkin) && $currentSkin != 'default' ){ 
		wp_enqueue_style( 'royanwine-'.$currentSkin.'-style', get_template_directory_uri() . '/css/skins/'.$currentSkin.'/'.$prefixRTL.'style.css' );
	}else {
		// Load our main stylesheet.
		wp_enqueue_style( 'royanwine-style', get_template_directory_uri() . '/css/'.$prefixRTL.'style.css' );
	}
	
	
	wp_enqueue_script( 'bootstrap-min', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '20130402' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20130402' );
	}

	// Load the Internet Explorer specific stylesheet.
    wp_enqueue_style( 'royanwine-ie', get_template_directory_uri() . '/css/' . $prefixRTL . 'ie.css', array( 'royanwine-style' ), '20131205' );
    wp_style_add_data( 'royanwine-ie', 'conditional', 'lt IE 9' );

    wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/js/owl-carousel/owl.carousel.js', array( 'jquery' ), '20150315', true );
	wp_enqueue_script('prettyphoto',	get_template_directory_uri().'/js/jquery.prettyPhoto.js',array(),false,true);
	wp_enqueue_style( 'prettyphoto', get_template_directory_uri() . '/css/'.$prefixRTL.'prettyPhoto.css');
    wp_enqueue_script('datetimepicker', get_template_directory_uri() . '/js/jquery.datetimepicker.full.min.js');
    wp_enqueue_style( 'datetimepicker', get_template_directory_uri() . '/css/jquery.datetimepicker.min.css');

	wp_register_script( 'royanwine-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150315', true );
	wp_localize_script( 'royanwine-script', 'royanwineajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
	wp_enqueue_script( 'royanwine-script' );
}
add_action( 'wp_enqueue_scripts', 'royanwine_fnc_scripts' );


/**
 * Enqueue Google fonts style to admin screen for custom header display.
 *
 * @since Royanwine 1.0
 */


require_once(  get_template_directory() . '/inc/custom-header.php' );
require_once(  get_template_directory() . '/inc/customizer.php' );
require_once(  get_template_directory() . '/inc/custom-customizer.php' );
require_once(  get_template_directory() . '/inc/function-post.php' );
require_once(  get_template_directory() . '/inc/function-unilty.php' );
require_once(  get_template_directory() . '/inc/functions-import.php' );
require_once(  get_template_directory() . '/inc/template-hook.php' );
require_once(  get_template_directory() . '/inc/template-tags.php' );
require_once(  get_template_directory() . '/inc/template.php' );
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

if( is_admin() ){
	require_once(  get_template_directory() . '/inc/admin/function.php' );
	if(class_exists('RW_Meta_Box')){
		require_once(  get_template_directory() . '/inc/admin/metabox/pagepost.php' );
	}
}

//Plugin pbrthemer active
if(is_plugin_active( 'pbrthemer/pbrthemer.php' ) && function_exists('pbrthemer_fnc_theme_options')){
	if(class_exists('PBR_User_Account')){
		new PBR_User_Account();
	}
	
}

/**
 * Check and load to support visual composer
 */
if(is_plugin_active( 'js_composer/js_composer.php' ) && class_exists('Vc_Manager')){
	require_once(  get_template_directory() . '/inc/vendors/visualcomposer/class-vc-elements.php' );
	require_once(  get_template_directory() . '/inc/vendors/visualcomposer/class-vc-theme.php' );
	require_once(  get_template_directory() . '/inc/vendors/visualcomposer/class-vc-extends.php' );
	require_once(  get_template_directory() . '/inc/vendors/visualcomposer/function-vc.php' );
	require_once(  get_template_directory() . '/inc/vendors/visualcomposer/class-visual-composer.php' );

	function royanwine_fnc_set_post_types_vc(){
		if ( function_exists( 'vc_editor_set_post_types' ) ) {
			$posttype = array( 'megamenu_profile', 'footer', 'product', 'page');
			vc_editor_set_post_types( $posttype );
		}
	}
	add_action( 'after_setup_theme', 'royanwine_fnc_set_post_types_vc' );
}

if(is_plugin_active( 'contact-form-7/wp-contact-form-7.php' )){
    require_once(  get_template_directory() . '/inc/widgets/schedulepopup.php' );
}
/**
 * Check to support woocommerce
 */
if( is_plugin_active( 'woocommerce/woocommerce.php') ){
	add_theme_support( "woocommerce" );
	require_once(  get_template_directory() . '/inc/vendors/woocommerce/class-vc-woocommerce-extends.php' );
	require_once(  get_template_directory() . '/inc/vendors/woocommerce/class-vc-woocommerce.php' );
	require_once(  get_template_directory() . '/inc/vendors/woocommerce/class-woocommerce.php' );
	require_once(  get_template_directory() . '/inc/vendors/woocommerce/function-hook.php' );
	require_once(  get_template_directory() . '/inc/vendors/woocommerce/function-logic.php' );
}
