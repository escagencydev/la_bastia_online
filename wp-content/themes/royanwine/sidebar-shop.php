<?php
/**
 * The Sidebar containing the main widget area
 *
 * @package WPOPAL
 * @subpackage royanwine
 * @since Royanwine 1.0
 */
 
$royanwine_page_layouts = apply_filters( 'royanwine_fnc_get_woocommerce_sidebar_configs', null );
if( isset($royanwine_page_layouts['sidebars']) ): $sidebars = $royanwine_page_layouts['sidebars']; 
?> 
	<?php if ( $sidebars['left']['show'] ) : ?>
	<div class="<?php echo esc_attr($sidebars['left']['class']) ;?> pull-left">
	  <aside class="sidebar sidebar-left" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
	   	<?php dynamic_sidebar( $sidebars['left']['sidebar'] ); ?>
	  </aside>
	</div>
	<?php endif; ?>
 	
 	<?php if ( $sidebars['right']['show'] ) : ?>
	<div class="<?php echo esc_attr($sidebars['right']['class']) ;?> pull-right">
	  <aside class="sidebar sidebar-right" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
	   	<?php dynamic_sidebar( $sidebars['right']['sidebar'] ); ?>
	  </aside>
	</div>
	<?php endif; ?>
<?php endif; ?> 

