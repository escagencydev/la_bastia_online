'use strict';

const gulp       = require('gulp'),
      cssnano    = require('cssnano'),
      sass       = require('gulp-sass'),
      sourcemaps = require('gulp-sourcemaps');
const glob       = require("glob");
const path       = require('path');
const fs         = require('fs-extra');
const rtlcss     = require('rtlcss');

const babel  = require('gulp-babel');
const concat = require('gulp-concat');
const inject = require('gulp-inject');
const iconfont = require('gulp-iconfont');
const iconfontCss = require('gulp-iconfont-css');
const uglify = require('gulp-uglify');

const browserSync = require('browser-sync');

//path
const linkWebsite = 'http://localhost/royanwine';
const fontName = 'LexusIcon';

let supported = [
    'last 2 versions',
    'safari >= 8',
    'ie >= 10',
    'ff >= 20',
    'ios 6',
    'android 4'
];

gulp.task('css', function () {
    return gulp.src(['sass/style.scss'])
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./css'));
});

gulp.task('css-woo', () => {
    return gulp.src(['sass/woocommerce.scss'])
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        //.pipe(cssnano({
        //    autoprefixer: {browsers: supported, add: true}
        //}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./css'));
});

//gulp.task('css-skins', () => {
//    return gulp.src(['sass/skins/**/*.scss'])
//        .pipe(sourcemaps.init())
//        .pipe(sass().on('error', sass.logError))
//        //.pipe(cssnano({
//        //    autoprefixer: {browsers: supported, add: true}
//        //}))
//        .pipe(sourcemaps.write('.'))
//        .pipe(gulp.dest('./css/skins'));
//});

// ========================================================================
// ========================================================================
//                                RTL
// ========================================================================
// ========================================================================
gulp.task('rtlcss', () => {
    let contentCss = fs.readFileSync('./css/style.css', 'utf8');
    fs.writeFileSync('css/rtl-style.css', rtlcss.process(contentCss), 'utf8');

let wooCss = fs.readFileSync('./css/woocommerce.css', 'utf8');
fs.writeFileSync('css/rtl-woocommerce.css', rtlcss.process(wooCss), 'utf8');

let ppCss = fs.readFileSync('./css/prettyPhoto.css', 'utf8');
fs.writeFileSync('css/rtl-prettyPhoto.css', rtlcss.process(ppCss), 'utf8');
});

// ========================================================================
// ========================================================================
//                             Gen icon font from SVG
// ========================================================================
// ========================================================================
gulp.task('iconfont', () => {
    return gulp.src(['svg/*.svg'])
        .pipe(iconfontCss({
            fontName: fontName,
            normalize: true,
            prependUnicode: true, // recommended option
            fontHeight: 1001, // Tried lot of values, <1000 and also 10000, and 100000 :P but no success
            centerHorizontally: true,
            fontPath: ('fonts/'),
            path: ('svg/template.scss'),
            targetPath: '../sass/components/_icons.scss',
            cssClass: 'lexus-icon'
        }))
        .pipe(iconfont({
            fontName: fontName
        }))
        .pipe(gulp.dest('fonts/'));
});


gulp.task('watch', [
    'css',
    'css-woo',
    'iconfont',
]);


// ========================================================================
// ========================================================================
//                            Customize
// ========================================================================
// ========================================================================


gulp.task('start', ['css', 'css-woo', 'browser-sync', 'watch', 'iconfont'], function () {
    gulp.watch('sass/**/*.scss', ['css']);
    gulp.watch('sass/**/*.scss', ['css-woo']);
    gulp.watch('svg/*.svg', ['iconfont']);
    gulp.watch('svg/*.scss', ['iconfont']);
    // Other watchers
});

// ========================================================================
// ========================================================================
//                                Live Reload
// ========================================================================
// ========================================================================
gulp.task('browser-sync', () => {
    let files = [
        './**/*.css',
        './**/*.php',
        './**/*.js',
    ];
browserSync.init(files, {
    proxy    : linkWebsite,
    watchTask: true,
    port     : 8888,
    force    : true,
    open     : "local",
    ui       : false
    // browser: "google chrome",
});
});
//
//gulp.task('default', ['watch']);
