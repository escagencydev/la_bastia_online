<?php
/**
 * The template for displaying posts in the Video post format
 *
 * @package WPOPAL
 * @subpackage royanwine
 * @since Royanwine 1.0
 */
$videolink =  get_post_meta( get_the_ID(),'royanwine_video_link', true );
?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="post-single">
        <div class="post-preview">
            <?php if( $videolink ) : ?>
                <div class="video-thumb video-responsive"><?php echo wp_oembed_get( $videolink ); ?></div>
            <?php else : ?>
                <?php if ( has_post_thumbnail() ) : ?>
                    <figure class="entry-thumb zoom-2"><?php royanwine_fnc_post_thumbnail(); ?></figure>
                <?php endif; ?>
            <?php endif; ?>
            <span class="first-category hidden">
                <?php
                    $categories = get_the_category();
                    if ( ! empty( $categories ) ) {
                      echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
                    }
                ?> 
            </span>
        </div>
        <div class="entry-meta clearfix">
            <div class="entry-category pull-left">
                <?php the_category(); ?>
            </div>
            <div class="entry-date pull-left">
                <span><em>,</em>&nbsp;<?php the_time( 'j F' ); ?>,&nbsp;<?php the_time( 'Y' ); ?>,&nbsp;</span>
            </div>
            <span class="author pull-left"><?php esc_html_e( 'by', 'royanwine' ); ?>
                &nbsp;<?php the_author_posts_link(); ?></span>
        </div><!-- .entry-meta -->

        <header class="entry-header">
            <?php
            if (is_single()) :
                the_title( '<h1 class="entry-title">', '</h1>' );
            else :
                the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
            endif;
            ?>
        </header><!-- .entry-header -->

        <?php if (is_search()) : ?>
            <div class="entry-summary">
                <?php the_excerpt(); ?>
            </div><!-- .entry-summary -->
        <?php else : ?>
            <div class="entry-content">
                <?php
                /* translators: %s: Name of current post */
                if (is_single()){
                    the_content( sprintf(
                        esc_html__( 'Continue reading %s', 'royanwine' ) . '<span class="meta-nav">&rarr;</span>',
                        the_title( '<span class="screen-reader-text">', '</span>', false )
                    ) );
                } else{
                    the_excerpt();
                }

                wp_link_pages( array(
                    'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'royanwine' ) . '</span>',
                    'after'       => '</div>',
                    'link_before' => '<span>',
                    'link_after'  => '</span>',
                ) );
                ?>
            </div><!-- .entry-content -->
        <?php endif; ?>

        <?php if (is_single()): ?>
            <div class="tag-links-wrapper">
                <div class="row">
                    <div class="col-sm-6">
                        <?php if (function_exists( 'pbr_themer_get_template_part' ) && royanwine_fnc_theme_options( 'blog-show-share-post', true )){
                            pbr_themer_get_template_part( 'sharebox' );
                        } ?>
                    </div>
                    <?php the_tags( '<div class="tag-links col-sm-6 text-right"><span>' . esc_html__('Tags:', 'royanwine') . '</span>', ', ', '</div>' ); ?>
                </div>
            </div>
        <?php endif; ?>
        </div>
    </article><!-- #post-## -->

