<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOPAL Team <opalwordpress@gmail.com?>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/questions/
 */

if ( post_password_required() ){
    return;
}
?>
<div id="comments" class="comments">
    <header class="header-title">
        <h6 class="comments-title title-heading text-center"><?php comments_number( esc_html__('0 Comment', 'royanwine'), esc_html__('1 Comment', 'royanwine'), esc_html__('% Comments', 'royanwine') ); ?></h6>
    </header><!-- /header -->

    <?php if ( have_comments() ) { ?>
        <div class="pbr-commentlists">
    	    <ol class="commentlists">
    	        <?php wp_list_comments('callback=royanwine_fnc_theme_comment'); ?>
    	    </ol>
    	    <?php
    	    	// Are there comments to navigate through?
    	    if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
    	    ?>
    	    <footer class="navigation comment-navigation">
    	        <div class="previous"><?php previous_comments_link( esc_html__( '&larr; Older Comments', 'royanwine' ) ); ?></div>
    	        <div class="next right"><?php next_comments_link( esc_html__( 'Newer Comments &rarr;', 'royanwine' ) ); ?></div>
    	    </footer><!-- .comment-navigation -->
    	    <?php endif; // Check for comment navigation ?>

    	    <?php if ( ! comments_open() && get_comments_number() ) : ?>
    	        <p class="no-comments"><?php esc_html_e( 'Comments are closed.' , 'royanwine' ); ?></p>
    	    <?php endif; ?>
        </div>
    <?php } ?> 

	<?php
        $aria_req = ( $req ? " aria-required='true'" : '' );
        $comment_args = array(
                        'title_reply'=> ('<h6 class="title title-heading text-center">'.esc_html__('Leave a Comment','royanwine').'</h6>'),
                        'comment_field' => '<div class="form-group col-sm-12">
                                                <label class="field-label" for="comment">'. esc_html__('Comment:', 'royanwine').'</label>
                                                <textarea rows="8" id="comment" class="form-control" placeholder="'.esc_attr__('Your Comment', 'royanwine').'" name="comment"'.$aria_req.'></textarea>
                                            </div>',
                        'fields' => apply_filters(
                        	'comment_form_default_fields',
                    		array(
                                'author' => '<div class="form-group col-sm-6">
                                            <label for="author">'. esc_html__('Name:', 'royanwine').'</label>
                                            <input type="text" name="author" class="form-control" id="author" placeholder="'.esc_attr__('Your Name', 'royanwine').'" value="' . esc_attr( $commenter['comment_author'] ) . '" ' . $aria_req . ' />
                                            </div>',
                                'email' => ' <div class="form-group col-sm-6">
                                            <label for="email">'. esc_html__('Email:', 'royanwine').'</label>
                                            <input id="email" name="email" class="form-control" type="text" placeholder="'.esc_attr__('Your Email', 'royanwine').'" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" ' . $aria_req . ' />
                                            </div>',
                            )),
                        'label_submit' => esc_html__('Post Comment', 'royanwine'),
						'comment_notes_before' => '<div class="form-group col-sm-12 h-info">'.esc_html__('Your email address will not be published.','royanwine').'</div>',
						'comment_notes_after' => '',
                        );
    ?>
	<?php global $post; ?>
	<?php if('open' == $post->comment_status){ ?>
	<div class="commentform row reset-button-default">
    	<div class="col-sm-12">
			<?php royanwine_fnc_comment_form($comment_args); ?>
    	</div>
    </div><!-- end commentform -->
	<?php } ?>
</div><!-- end comments -->