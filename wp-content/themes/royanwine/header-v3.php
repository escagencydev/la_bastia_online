<?php
/**
 * The Header for our theme: Top has Logo left + search right . Below is horizal main menu
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WPOPAL
 * @subpackage royanwine
 * @since Royanwine 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site"><div class="pbr-page-inner row-offcanvas row-offcanvas-left">
	<?php if ( get_header_image() ) : ?>
	<div id="site-header" class="hidden-xs hidden-sm">
		<a href="<?php echo esc_url( get_option('header_image_link','#') ); ?>" rel="home">
			<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
		</a>
	</div>
	<?php endif; ?>
	<?php do_action( 'royanwine_template_header_before' ); ?>
	<header id="pbr-masthead" class="site-header pbr-header-v3">
		<div class="<?php echo royanwine_fnc_theme_options('keepheader') ? 'has-sticky' : 'no-sticky'; ?>">
            <div class="header-top">
                <div class="container">
                    <div class="inner clearfix">
                        <?php get_template_part( 'page-templates/parts/logo-v2' ); ?>
                        <div class="header-right">
                            <div class="cart-wrapper d-flex hidden-md hidden-sm hidden-xs">
                                <?php if( class_exists( 'YITH_WCWL' ) ): ?>
                                    <div class="wishlist">
                                        <a class="opal-btn-wishlist" href="<?php echo esc_url( get_permalink( get_option('yith_wcwl_wishlist_page_id') ) ); ?>"><i class="fa fa-heart-o"></i><span class="count"><?php $wishlist_count = YITH_WCWL()->count_products();echo trim( $wishlist_count ); ?></span> </a>
                                    </div>
                                <?php endif; ?>
                                <?php do_action( 'royanwine_template_header_right' ); ?>
                            </div>
                            <div id="pbr-mainmenu" class="pbr-mainmenu">
                                <div class="inner"><?php get_template_part( 'page-templates/parts/nav' ); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</header><!-- #masthead -->

	<?php do_action( 'royanwine_template_header_after' ); ?>
	
	<section id="main" class="site-main">
