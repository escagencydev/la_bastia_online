<?php
/**
 * The Header for our theme: Top has Logo left + search right . Below is horizal main menu
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WPOPAL
 * @subpackage royanwine
 * @since Royanwine 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site"><div class="pbr-page-inner row-offcanvas row-offcanvas-left">
        <?php if ( get_header_image() ) : ?>
            <div id="site-header" class="hidden-xs hidden-sm">
                <a href="<?php echo esc_url( get_option('header_image_link','#') ); ?>" rel="home">
                    <img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                </a>
            </div>
        <?php endif; ?>
        <?php do_action( 'royanwine_template_header_before' ); ?>
        <header id="pbr-masthead" class="site-header pbr-header-v2">
            <div class="<?php echo royanwine_fnc_theme_options('keepheader') ? 'has-sticky' : 'no-sticky'; ?>">
                <div class="header-top">
                    <div class="container">
                        <div class="header-top-inner">
                            <?php get_template_part( 'page-templates/parts/logo' ); ?>
                            <div id="pbr-mainmenu" class="pbr-mainmenu hidden-md">
                                <div class="inner"><?php get_template_part( 'page-templates/parts/nav' ); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header><!-- #masthead -->

        <?php do_action( 'royanwine_template_header_after' ); ?>

        <section id="main" class="site-main">
