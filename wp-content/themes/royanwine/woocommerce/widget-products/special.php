<?php 
$classes = array();
$classes[] = $class_column;
$thumbsize = ($image_size === 'catalog') ? '600x400' : $image_size;

?>
<div class="product_special_widget clearfix">
	<div class="row">
		<?php while ( $loop->have_posts() ) : $loop->the_post(); $product = wc_get_product(get_the_ID());?>
			<div <?php post_class( $classes ); ?>>
				<div class="product-block" data-product-id="<?php echo esc_attr($product->get_id()); ?>">
				    <figure class="image">
				        <a title="<?php the_title(); ?>" href="<?php the_permalink() ?>">
				            <?php 
	                            $thumbnail_data = wpb_getImageBySize( array(
	                                'attach_id' => (int) get_post_thumbnail_id( get_the_ID() ),
	                                'thumb_size' => $thumbsize,
	                            ) );
	                        	echo trim( $thumbnail_data['thumbnail'] );
				            ?>
				        </a>
				        <div class="action-special clearfix">

				        	<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>

			                <?php
			                if( class_exists( 'YITH_WCWL' ) ) {
			                    echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
			                }
			                ?>

			                <?php if( class_exists( 'YITH_Woocompare' ) ) { ?>
			                    <?php
			                    $action_add = 'yith-woocompare-add-product';
			                    $url_args = array(
			                        'action' => $action_add,
			                        'id' => $product->get_id()
			                    );
			                    ?>
			                    <div class="yith-compare">
			                        <a href="<?php echo wp_nonce_url( add_query_arg( $url_args ), $action_add ); ?>" class="compare" data-product_id="<?php echo esc_attr($product->get_id()); ?>">
			                            <em class="fa fa-exchange"></em>
			                        </a>
			                    </div>
			                <?php } ?>
			                
			                <?php if(royanwine_fnc_theme_options('is-quickview', true)){ ?>
			                    <div class="quick-view">
			                        <a href="#" class="quickview" data-productid="<?php echo esc_attr($product->get_id()); ?>" data-toggle="modal" data-target="#pbr-quickview-modal">
			                            <i class="fa fa-eye"> </i><span><?php esc_html_e( 'Quick view', 'royanwine' ); ?></span>
			                        </a>
			                    </div>
			                <?php } ?>

			            </div>
				    </figure>

				    <div class="special-product">
				        <div class="meta">
		        			<h3 class="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		        			<div class="description"><?php echo royanwine_fnc_excerpt(15, '...'); ?></div>
		        			<?php woocommerce_template_single_price(); ?>
				            
				        </div>
				    </div>
				</div>

			</div>
		<?php endwhile; ?>
	</div>
		
</div>
<?php wp_reset_postdata(); ?>