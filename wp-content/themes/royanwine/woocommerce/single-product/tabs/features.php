<?php
/**
 * Description tab
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version       3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

$heading = esc_html( apply_filters( 'royanwine_woocommerce_product_features_heading', esc_html__( 'Product Features', 'royanwine' ) ) );
$features = get_post_meta( get_the_ID(), 'royanwine_product_features', true);
?>

<?php if ( $heading ): ?>
  <h2><?php echo trim( $heading ); ?></h2>
<?php endif; ?>

<?php echo trim( $features ); ?>
