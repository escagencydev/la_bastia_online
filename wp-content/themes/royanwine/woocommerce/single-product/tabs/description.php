<?php
/**
 * Description tab
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version       3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

$heading = esc_html( apply_filters( 'woocommerce_product_description_heading', esc_html__( 'Product Description', 'royanwine' ) ) );

?>

<?php if ( $heading ): ?>
  <h2><?php echo trim( $heading ); ?></h2>
<?php endif; ?>

<?php the_content(); ?>

<?php wc_get_template_part( 'single-product/gallery' ); ?>