<?php
/**
 * Description tab
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version       3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

$galleries = get_post_meta( get_the_ID(), 'royanwine_product_galleries', false);
$_id = royanwine_fnc_makeid();
?>
<?php if ( !empty($galleries) ): ?>
	<div id="carousel-<?php echo esc_attr($_id); ?>" class="owl-carousel-play" data-ride="owlcarousel">         
	    
	    <div class="owl-carousel" data-slide="1" data-pagination="false" data-navigation="false" data-custompagin="true">
	        <?php foreach ($galleries as $image_id) :?>
	        	<div class="item">
                    <?php echo wp_get_attachment_image( $image_id, 'full' ); ?>
                </div>
        	<?php endforeach; ?>

	    </div>
	    <ul class="pbr-owl-thumbs">
		    <?php foreach ($galleries as $image_id) :?>
	        	<li>
	                <a href="#" title=""><?php echo wp_get_attachment_image( $image_id, 'thumbnail' ); ?></a>
	            </li>
	    	<?php endforeach; ?>
    	</ul>
	</div>    
<?php endif; ?>
