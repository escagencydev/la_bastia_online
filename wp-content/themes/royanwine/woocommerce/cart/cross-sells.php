<?php
/**
 * Cross-sells
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cross-sells.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version       3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$_count = 1;
$_id = royanwine_fnc_makeid();

if ( $cross_sells ) : ?>

	<div class="cross-sells">

		<h2><?php esc_html_e( 'You may be interested in&hellip;', 'royanwine' ) ?></h2>

		<?php woocommerce_product_loop_start(); ?>
			
			<div id="carousel-<?php echo esc_attr($_id); ?>" class="owl-carousel-play" data-ride="owlcarousel">         
    
                <?php if(count($cross_sells)>2 ){ ?>
                    <div class="carousel-controls carousel-controls-v3 carousel-hidden">
                        <a class="left carousel-control carousel-md" href="#post-slide-<?php the_ID(); ?>" data-slide="prev">
                                <span class="fa fa-angle-left"></span>
                        </a>
                        <a class="right carousel-control carousel-md" href="#post-slide-<?php the_ID(); ?>" data-slide="next">
                                <span class="fa fa-angle-right"></span>
                        </a>
                    </div> 
                <?php } ?>
                 <div class="owl-carousel products" data-slide="2" data-pagination="false" data-navigation="true">
                    <?php foreach ( $cross_sells as $cross_sell ) : ?>
                    	<?php
        				 	$post_object = get_post( $cross_sell->get_id() );

        					setup_postdata( $GLOBALS['post'] =& $post_object );

        				?>
                        <div class="item">
                            <div class="products-grid product">
                                <?php wc_get_template_part( 'content', 'product-inner' ); ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>    
		<?php woocommerce_product_loop_end(); ?>

	</div>

<?php endif;

wp_reset_postdata();
