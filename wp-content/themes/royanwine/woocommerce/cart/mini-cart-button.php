<?php global $woocommerce; ?>
<div class="pbr-topcart">
    <div id="cart" class="dropdown">

        <a class="dropdown-toggle mini-cart d-flex" data-toggle="dropdown" aria-expanded="true" role="button" aria-haspopup="true" data-delay="0" href="#" title="<?php esc_html_e( 'View your shopping cart', 'royanwine' ); ?>">
            <span class="text-skin cart-icon">
                <i class="fa fa-shopping-bag"></i>
                <?php echo sprintf( _n( ' <span class="mini-cart-items"> %d  </span> ', ' <span class="mini-cart-items"> %d <em>item</em> </span> ', $woocommerce->cart->cart_contents_count, 'royanwine' ), $woocommerce->cart->cart_contents_count ); ?>
            </span>
            <div class="cart-right">
                <span class="title-cart"><?php esc_html_e( 'Shopping Cart ', 'royanwine' ); ?></span>

                <span class="mini-cart-total"> <?php echo trim( $woocommerce->cart->get_cart_total() ); ?> </span>
            </div>
        </a>
        <div class="dropdown-menu">
            <div class="widget_shopping_cart_content">
                <?php woocommerce_mini_cart(); ?>
            </div>
        </div>
    </div>
</div>    