<?php 
global $product;
$image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id($product->get_id() ), 'blog-thumbnails' );
?>
<div class="product-block" data-product-id="<?php echo esc_attr($product->get_id()); ?>">
    <figure class="image">
        <a title="<?php the_title(); ?>" href="<?php the_permalink() ?>">
            <?php
                /**
                * woocommerce_before_shop_loop_item_title hook
                *
                * @hooked woocommerce_show_product_loop_sale_flash - 10
                * @hooked woocommerce_template_loop_product_thumbnail - 10
                */
                //do_action( 'woocommerce_before_shop_loop_item_title' );
                echo $product->get_image( 'woocommerce_thumbnail' );
            ?>
        </a>
    </figure>

    <div class="caption">
        
        <div class="meta">
            
                <?php
                    /**
                    * woocommerce_after_shop_loop_item_title hook
                    *
                    * @hooked woocommerce_template_loop_rating - 5
                    * @hooked woocommerce_template_loop_price - 10
                    */
                    add_action( 'woocommerce_single_product_summary','woocommerce_template_single_excerpt', 1);
                    //do_action( 'woocommerce_single_product_summary' );

                ?>
            
        </div>
        <?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
    </div>
</div>
