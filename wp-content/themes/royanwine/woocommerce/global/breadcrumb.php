<?php
/**
 * Shop breadcrumb
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version       3.5.0
 * @see         woocommerce_breadcrumb()
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//if(is_single()) return; 
if ( ! empty( $breadcrumb )) {

	$delimiter = '<span>&nbsp;&#62;&nbsp;</span>';

	echo trim($wrap_before);

	$end = '' ;
	$title = '';
	foreach ( $breadcrumb as $key => $crumb ) {

		echo trim($before);
		echo '<li>';

		if ( ! empty( $crumb[1] ) && sizeof( $breadcrumb ) !== $key + 1 ) {
			echo '<a href="' . esc_url( $crumb[1] ) . '">' . esc_html( $crumb[0] ) . '</a>';
		} else {
			echo esc_html( $crumb[0] );
			$title = esc_html( $crumb[0] );
		}

		echo trim($after);
		if ( sizeof( $breadcrumb ) !== $key + 1 ) {
			echo trim( $delimiter );
		}
		echo '</li>';

		$end = esc_html( $crumb[0] );
	}
	if ( is_product() ) {
        $title = esc_html__('Our Products', 'royanwine');
	}
	printf('<li  class="active"><h1>%s</h1></li>', $title);
	echo trim($wrap_after);

}
?>
