<?php

	if( $relates->have_posts() ): ?>
    <div class="widget widget-style">
        <h4 class="related-post-title widget-title">
            <span><?php esc_html_e( 'Related posts', 'royanwine' ); ?></span>
        </h4>

        <div class="related-posts-content">
            <div class="row">
            <?php
                $class_column = 12/$relate_count;
                while ( $relates->have_posts() ) : $relates->the_post();
                    ?>
                    <div class="col-sm-<?php echo esc_attr( $class_column ); ?> col-md-<?php echo esc_attr( $class_column ); ?> col-lg-<?php echo esc_attr( $class_column ); ?>">
                        <article class="post">
                            <?php
                            if (has_post_thumbnail()){
                                ?>
                                <figure class="entry-thumb zoom-2">
                                    <a href="<?php the_permalink(); ?>" title="" class="entry-image">
                                        <?php the_post_thumbnail( ); ?>
                                    </a>
                                    <!-- vote    -->
                                    <?php do_action( 'royanwine_fnc_rating' ) ?>
                                    <span class="first-category hidden">
                                        <?php
                                            $categories = get_the_category();
                                            if ( ! empty( $categories ) ) {
                                              echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
                                            }
                                        ?> 
                                    </span>
                                </figure>
                                <?php
                            }
                            ?>
                            <div class="entry-content">

                                <div class="entry-meta clearfix">
                                    <div class="entry-category pull-left">
                                        <?php the_category(); ?>
                                    </div>
                                    <div class="entry-date pull-left">
                                        <span><em>,</em>&nbsp;<?php the_time( 'j F' ); ?>,&nbsp;<?php the_time( 'Y' ); ?>,&nbsp;</span>
                                    </div>
                                    <span class="author pull-left"><?php esc_html_e( 'by', 'royanwine' ); ?>&nbsp;<?php the_author_posts_link(); ?></span>
                                </div>

                                <?php if (get_the_title()){ ?>
                                    <h4 class="entry-title">
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </h4>
                                <?php } ?>

                            </div>
                        </article>
                    </div>
                    <?php
                endwhile; ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
        
    </div>
        <?php
    endif;
?>