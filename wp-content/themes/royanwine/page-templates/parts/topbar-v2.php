<section id="pbr-topbar" class="pbr-topbar pbr-topbar-v2 hidden-xs hidden-sm">
    <div class="container">
        <div class="topbar-inner d-flex justify-content-between flex-wrap">
            <div class="box-user open">
                <span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
                      role="link"><?php esc_html_e( 'Account ', 'royanwine' ); ?><span
                            class="caret"></span></span>
                <?php do_action( 'opal-account-buttons' ); ?>
            </div>
            <div class="cart-wrapper d-flex hidden-md hidden-sm hidden-xs">
                <?php if( class_exists( 'YITH_WCWL' ) ): ?>
                    <div class="wishlist">
                        <a class="opal-btn-wishlist" href="<?php echo esc_url( get_permalink( get_option('yith_wcwl_wishlist_page_id') ) ); ?>"><i class="fa fa-heart-o"></i><span class="count"><?php $wishlist_count = YITH_WCWL()->count_products();echo trim( $wishlist_count ); ?></span> </a>
                    </div>
                <?php endif; ?>
                <?php do_action( 'royanwine_template_header_right' ); ?>
            </div>
        </div>
    </div>
</section>
