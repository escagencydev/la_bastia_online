<?php
$header_info_title =  royanwine_fnc_theme_options('header_info_title', '');
$header_info_number =  royanwine_fnc_theme_options('header_info_number', '');
?>
<div class="header-info d-flex">
    <div class="header-info-icon">
        <i class="fa fa-phone" aria-hidden="true"></i>
    </div>
    <div class="header-info-text">
        <div class="title"><?php echo trim($header_info_title);?></div>
        <div class="phone"><?php echo trim($header_info_number);?></div>
    </div>
</div>