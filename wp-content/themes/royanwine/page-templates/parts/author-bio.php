<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <opalwordpress@gmail.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/questions/
 */
?>

<div class="author-info text-center">
    <div class="avatar-img">
        <?php echo get_avatar( get_the_author_meta( 'user_email' ),72 ); ?>
    </div>
    <!-- .author-avatar -->
    <div class="description">
        <h6><?php esc_html_e('About the author', 'royanwine')?></h6>
        <h5 class="author-title">
            <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>">
                <?php echo get_the_author(); ?>
            </a>
        </h5>
        <div class="author-description text-left"><?php the_author_meta( 'description' ); ?></div>
    </div>
</div>