<section id="pbr-topbar" class="pbr-topbar pbr-topbar-v3 hidden-xs hidden-sm">
    <div class="container">
        <div class="topbar-inner d-flex justify-content-between flex-wrap">
            <div class="header-info-wrapper hidden-xs">
                <?php get_template_part( 'page-templates/parts/header-info' ); ?>
            </div>

            <div class="box-user open">
                <span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
                      role="link"><?php esc_html_e( 'Account ', 'royanwine' ); ?><span
                            class="caret"></span></span>
                <?php do_action( 'opal-account-buttons' ); ?>
            </div>
        </div>
    </div>
</section>
