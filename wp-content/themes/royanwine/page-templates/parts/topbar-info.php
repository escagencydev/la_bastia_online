<?php
$topbar_info_location =  royanwine_fnc_theme_options('topbar_info_location', '254 Street Avenue, Los Angeles, LA 2415 US');
$topbar_info_number =  royanwine_fnc_theme_options('topbar_info_number', '123-456-7899');
$topbar_info_email =  royanwine_fnc_theme_options('topbar_info_email', 'contact@example.com');
?>
<div class="topbar-info d-flex">
    <ul class="list-inline">
        <li class="location">
            <i class="fa fa-map-marker"></i>
            <span><?php esc_html_e('Location', 'royanwine')?></span>
            <?php echo trim($topbar_info_location);?>
        </li>
        <li class="phone"><?php echo trim($topbar_info_number);?></li>
        <li class="email">
            <a href=""></a><?php echo trim($topbar_info_email);?></li>
    </ul>
</div>