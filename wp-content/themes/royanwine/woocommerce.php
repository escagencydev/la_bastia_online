<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WPOPAL
 * @subpackage royanwine
 * @since Royanwine 1.0
 */
 
$royanwine_page_layouts = apply_filters( 'royanwine_fnc_get_woocommerce_sidebar_configs', null );

get_header( apply_filters( 'royanwine_fnc_get_header_layout', null ) ); ?>
<?php do_action( 'royanwine_woo_template_main_before' ); ?>
<section id="main-container" class="<?php echo apply_filters('royanwine_template_woocommerce_main_container_class','container');?>">
	
	<div class="row">

		<div id="main-content" class="main-content col-xs-12 <?php echo esc_attr($royanwine_page_layouts['main']['class']); ?>">

			<div id="primary" class="content-area">
				<div id="content" class="site-content" role="main">

					 <?php woocommerce_content(); ?>

				</div><!-- #content -->
			</div><!-- #primary -->
		</div><!-- #main-content -->

        <?php if( isset($royanwine_page_layouts['sidebars']) && !empty($royanwine_page_layouts['sidebars']) ) : ?>
            <?php get_sidebar('shop'); ?>
        <?php endif; ?>

	</div>	
</section>
<?php

get_footer();
