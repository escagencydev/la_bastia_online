<?php
/**
 * The Footer Sidebar
 *
 * @package WPOPAL
 * @subpackage royanwine
 * @since Royanwine 1.0
 */

?>

<div class="container">
    <section class="footer-top">
        <div class="row">
            <?php if (is_active_sidebar( 'footer-1' )) : ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="wow fadeInUp" data-wow-duration='0.8s' data-wow-delay="600ms">
                        <?php dynamic_sidebar( 'footer-1' ); ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (is_active_sidebar( 'footer-2' )) : ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="wow fadeInUp" data-wow-duration='0.8s' data-wow-delay="800ms">
                        <?php dynamic_sidebar( 'footer-2' ); ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (is_active_sidebar( 'footer-3' )) : ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="wow fadeInUp" data-wow-duration='0.8s' data-wow-delay="800ms">
                        <?php dynamic_sidebar( 'footer-3' ); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </section>
    <section class="pbr-copyright ">
        <div class="copyright-inner text-center">
            <?php
            $copyright_text = royanwine_fnc_theme_options( 'copyright_text', '' );
            if (!empty( $copyright_text )){
                echo do_shortcode( $copyright_text );
            } else{

                $devby = '<a target="_blank" href="http://wpopal.com">Opal Team</a>';
                printf( esc_html__( 'Proudly powered by %s. Developed by %s', 'royanwine' ), 'WordPress', $devby );
            }
            ?>
        </div>
    </section>
</div>
