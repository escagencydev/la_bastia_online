<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );


?>
<div class="pbr-our-team <?php echo esc_attr($style); ?>">
	<?php $img = wp_get_attachment_image_src($photo,'full'); ?>
	<?php if( isset($img[0]) )  { ?>
	<div class="team-image zoom-2">
		<img class="img-responsive" src="<?php echo esc_attr($img[0]);?>" alt="<?php echo esc_attr($title); ?>"  />
		<ul class="list-inline">
			<?php if( $facebook ){  ?>
			<li><a class="fa fa-facebook" href="<?php echo esc_url($facebook); ?>"> </a></li>
				<?php } ?>
			<?php if( $twitter ){  ?>
			<li><a class="fa fa-twitter" href="<?php echo esc_url($twitter); ?>"> </a></li>
			<?php } ?>
			<?php if( $linkedin ){  ?>
			<li><a class="fa fa-linkedin" href="<?php echo esc_url($linkedin); ?>"> </a></li>
			<?php } ?>
		</ul>
	</div>
	<?php } ?>
	<div class="team-footer text-center">
		<?php if( $title ){  ?>
			<h5><?php echo esc_html($title); ?></h5>
		<?php } ?>
		<?php if( $job ){  ?>
			<p><?php echo esc_html($job); ?></p>
		<?php } ?>
	</div>
</div>