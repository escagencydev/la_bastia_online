<?php 

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
?>

<div class="widget-product-info <?php echo esc_attr( $el_class ); ?> info-<?php echo esc_attr( $skin ); ?>">
    <div class="product-info-inner">
    	<?php if( !empty($image) ) : ?>
            <?php $img = wp_get_attachment_image_src($image,'full'); ?>
            <div class="pro-img text-center ">
                <img src="<?php echo esc_url_raw($img[0]); ?>" alt="">
            </div>
        <?php endif; ?>
    	<?php if($content!=''): ?>
            <div class="description">
                <?php echo trim( $content ); ?>
            </div>
        <?php endif; ?>
        <div class="product-id">
            
        </div>  
    </div>
</div>