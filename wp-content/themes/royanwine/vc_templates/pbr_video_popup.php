<?php

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

if(empty($video_link)) return;
$_id = royanwine_fnc_makeid();
$title = (!empty($title))? $title : esc_html__('How to made', 'royanwine');

?>

<div class="<?php echo esc_attr($style); ?> widget pbr-video-popup <?php echo (($el_class!='')?' '.$el_class:''); ?>">
	<div class="video-popup">
		<a class="button-video" href="<?php echo esc_url($video_link);?>" data-rel="prettyPhoto[videos]">
			<?php if ( !empty($subtitle)) { ?>
			    <span class="subtitle-content">
			    	<?php echo trim($subtitle); ?>
				</span>
			<?php } ?>
	        <span class="icon"><i class="fa fa-play" aria-hidden="true"></i></span>
	        <span class="title-content"><?php echo trim($title);?></span>
	    </a>
	    <?php if ( !empty($description)) { ?>
		    <div class="widget-content">
		    	<?php echo trim($description); ?>
			</div>
		<?php } ?>
	</div>
</div>