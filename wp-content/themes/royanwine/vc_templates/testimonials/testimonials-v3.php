<?php
$job = get_post_meta( get_the_ID(), 'testimonials_job', true );
?>
<div class="text-center">
    <div class="testimonials-quote"><?php the_content() ?></div>
    <div class="profile-group">
        <div class="testimonials-avatar">
            <?php the_post_thumbnail( 'widget', '', 'class=""' ); ?>
        </div>
        <h5 class="name"> <?php the_title(); ?></h5>
        <?php if (!empty( $job )): ?>
            <div class="job"><?php echo trim( $job ); ?></div>
        <?php endif; ?>
    </div>
</div>