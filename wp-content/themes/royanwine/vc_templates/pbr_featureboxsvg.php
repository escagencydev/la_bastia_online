<?php
$atts = shortcode_atts( array(
    'title'         => '',
    'information'         => '',
    'position'         => 'icon',
    'color'         => '#e1e1e1',
    'style'         => 'bottle',
    'svg_width' => '',
    'css'           => '',
    'el_class'      => '',
), $atts, 'pbr_featureboxsvg' );

extract( $atts );

$class_to_filter = vc_shortcode_custom_css_class( $css, ' ' );
$classes         = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, 'pbr_featureboxsvg', $atts );
$svg_icon         = royanwine_fnc_get_svg_icon( $style . '-' . $position, $color, $svg_width );

$classes .= ' dvr-style-' . $style;

( $el_class != '' ) ? $classes .= ' ' . $el_class : false;
?>
    <div class="pbr-svg-box <?php echo esc_attr( $classes ); ?>">
        <?php echo( $svg_icon ); ?>
        <h3><?php echo trim($title); ?></h3>
        <?php if(trim($information)!=''){ ?>
            <p class="description"><?php echo trim( $information );?></p>
        <?php } ?>
    </div>
<?php

