<?php

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

?>

<div class="widget-text-heading <?php echo esc_attr( $el_class ); ?> <?php echo esc_attr( $style ); ?> <?php echo esc_attr( $alignment ); ?>">
    <?php if (trim( $subtitle ) != ''){ ?>
        <div class="subtitle" <?php if ($subtitle_color != ''): ?> style="color: <?php echo esc_attr( $subtitle_color ); ?>;"<?php endif; ?>>
            <?php echo trim( $subtitle ); ?>
        </div>
    <?php } ?>

    <?php if ($title != ''): ?>

        <h2 class="widget-heading " <?php if ($font_color != ''): ?> style="color: <?php echo esc_attr( $font_color ); ?>;"<?php endif; ?>>
            <span class="heading-text"><?php echo trim( $title ); ?></span>
        </h2>

    <?php endif; ?>
    <?php if (trim( $descript ) != ''){ ?>
        <div class="description" <?php if ($descript_color != ''): ?> style="color: <?php echo esc_attr( $descript_color ); ?>;"<?php endif; ?>>
            <?php echo trim( $descript ); ?>
        </div>
    <?php } ?>
</div>