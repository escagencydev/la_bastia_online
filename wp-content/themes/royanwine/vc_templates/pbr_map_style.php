<?php

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$id = rand(time(),99999);
$google_api = royanwine_fnc_theme_options('pbr_theme_map_api');
?>

<div id="map<?php echo esc_attr($id) ; ?>" style="width: <?php echo esc_html($width); ?>;height: <?php echo esc_html($height); ?>;"></div>
    <script>
      function initMap() {
        // Styles a map in night mode.
        var map = new google.maps.Map(document.getElementById('map<?php echo esc_attr($id) ; ?>'), {
          center: {lat: <?php echo esc_js( $latitude ); ?>, lng: <?php echo esc_js( $longitude ); ?>},
          zoom: <?php echo esc_js( $zoom ); ?>,
          styles: [
    {
        "featureType": "administrative",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "water",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "transit",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "landscape",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.local",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "water",
        "stylers": [
            {
                "color": "#84afa3"
            },
            {
                "lightness": 52
            }
        ]
    },
    {
        "stylers": [
            {
                "saturation": -77
            }
        ]
    },
    {
        "featureType": "road"
    }
]
        });
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?php if($google_api){ echo esc_js( $google_api ); } ?>&callback=initMap"
    async defer></script>