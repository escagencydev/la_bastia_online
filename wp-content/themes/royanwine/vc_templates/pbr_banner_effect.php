<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );


$img = wp_get_attachment_image_src($photo,'full');
$skin = $skin ? 'banner-info-'.$skin:"";
?>
<div class="banner-info <?php echo esc_attr($skin); ?> <?php echo esc_attr($style); ?> <?php echo esc_attr($background); ?> <?php echo esc_attr($el_class) ?> <?php echo esc_attr($title_align); ?> zoom-2">
    <div class="banner-body">
        <h3><?php echo trim($title); ?></h3>
        <div>
            <?php
            if( ! empty ($button_text)) {
                $link = vc_build_link($link);
                $button_text = '<a class="" href="' . esc_attr($link['url']) . '"' . ($link['target'] ? ' target="' . esc_attr($link['target']) . '"' : '') . ($link['rel'] ? ' rel="' . esc_attr($link['rel']) . '"' : '') . ($link['title'] ? ' title="' . esc_attr($link['title']) . '"' : '') . '>' . $button_text . '</a>';
                echo trim( $button_text );
            }
            ?>
        </div>
    </div>
    <?php if(isset($img[0]) && $img[0]){?>
        <div class="banner-image">
            <img src="<?php echo esc_url_raw($img[0]);?>" alt="<?php echo esc_attr($title); ?>" />
        </div>
    <?php } ?>
</div>
