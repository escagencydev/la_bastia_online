<?php
$grid_link = $grid_layout_mode = $title = $filter = '';
$posts     = array();

$layout = '';
$atts   = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

if (empty( $loop ))
    return;
$this->getLoop( $loop );
$args = $this->loop_args;

if (is_front_page()){
    $paged = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;
} else{
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
}
$args['paged'] = $paged;
$post_per_page = $args['posts_per_page'];

$loop = new WP_Query( $args );
?>

    <section class="widget  widget-style  pbr-grid-posts layout-<?php echo esc_attr( $layout ); ?>  <?php echo esc_attr($style);?><?php echo( ( $el_class != '' ) ? ' ' . $el_class : '' ); ?>">
        <?php
        if ($title != ''){ ?>
            <h3 class="widget-title visual-title">
                <span><?php echo trim( $title ); ?></span>
            </h3>
        <?php }
        ?>
        <div class="widget-content">
            <?php
            /**
             * $loop
             * $class_column
             *
             */

            $_count = 0;

            $colums = $grid_columns;
            $bscol  = floor( 12 / $colums );

            ?>

            <div class="posts-grid">
                <?php
                $i = 0;
                while ($loop->have_posts()) {
                    $loop->the_post(); ?>
                    <?php $thumbsize = isset( $thumbsize ) ? $thumbsize : 'thumbnail'; ?>

                    <?php if ($i++ % $colums == 0){ ?>
                        <div class="post-item"><div class="row">
                    <?php } ?>
                    <div class="col-sm-<?php echo esc_attr( $bscol ); ?>">
                        <article class="post">
                            <?php
                            if (has_post_thumbnail()){
                                ?>
                                <figure class="entry-thumb zoom-2">
                                    <a href="<?php the_permalink(); ?>" title="" class="entry-image">
                                        <?php the_post_thumbnail( $thumbsize ); ?>
                                    </a>
                                    <!-- vote    -->
                                    <?php do_action( 'royanwine_fnc_rating' ) ?>
                                    <span class="first-category hidden">
                                        <?php
                                            $categories = get_the_category();
                                            if ( ! empty( $categories ) ) {
                                              echo '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
                                            }
                                        ?> 
                                    </span>
                                </figure>
                                <?php
                            }
                            ?>
                            <div class="entry-content <?php echo esc_attr($style);?>">

                                <div class="entry-meta clearfix">
                                    <div class="entry-category pull-left">
                                        <?php the_category(); ?>
                                    </div>
                                    <div class="entry-date pull-left">
                                        <span><em>,</em>&nbsp;<?php the_time( 'j F' ); ?>,&nbsp;<?php the_time( 'Y' ); ?>,&nbsp;</span>
                                    </div>
                                    <span class="author pull-left"><?php esc_html_e( 'by', 'royanwine' ); ?>&nbsp;<?php the_author_posts_link(); ?></span>
                                </div>

                                <?php if (get_the_title()){ ?>
                                    <h4 class="entry-title">
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </h4>
                                <?php } ?>

                                <div class="entry-description hidden">
                                    <?php
                                        if (! has_excerpt()) {
                                            echo "";
                                        } else {
                                            ?>
                                                <p><?php echo royanwine_fnc_excerpt(25,'...'); ?></p>
                                            <?php
                                        }
                                    ?>
                                </div>

                            </div>
                        </article>
                    </div>
                    <?php if (( $i % $colums == 0 ) || $i == $loop->post_count){ ?>
                        </div></div>
                    <?php } ?>
                <?php } ?>
            </div>


        </div>
        <?php if (isset( $show_pagination ) && $show_pagination): ?>
            <div class="w-pagination"><?php royanwine_fnc_pagination_nav( $post_per_page, $loop->found_posts, $loop->max_num_pages ); ?></div>
        <?php endif; ?>
    </section>
<?php wp_reset_postdata(); ?>