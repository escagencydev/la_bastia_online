(function () {
	"use strict";
	jQuery(document).ready(function($) {  
		$('body').delegate(".input_datetime", 'hover', function(e){
	            e.preventDefault();
	            $(this).datepicker({
		               defaultDate: "",
		               dateFormat: "yy-mm-dd",
		               numberOfMonths: 1,
		               showButtonPanel: true,
	            });
         });

		var hides = ['royanwine_audio_link','royanwine_link_link','royanwine_link_text','royanwine_video_link','royanwine_gallery_files'];
		var shows = {
			audio:['royanwine_audio_link'],
			video:['royanwine_video_link','royanwine_video_text'],
			link:['royanwine_link_link'],
			gallery:['royanwine_gallery_files']
		}
		$( '.post-type-post #post-formats-select input' ).on('click', function(){
			 $(hides).each( function( i, item ){
			 	$("[name="+item+']').parent().parent().hide();
			 } );
			 var s = $(this).val();
			 if( shows[s] != null ){
			 	$(shows[s]).each( function( i, is ){
			 		$("[name="+is+']').parent().parent().show();
				 } );
			 }
		} );
	});	
} )( jQuery );