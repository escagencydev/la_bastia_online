( function( $ ) {
    // royanwine_color_header_bg
    wp.customize( 'royanwine_color_header_bg', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-header_bg-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-header_bg-js-customizer">.opal-page-inner .site-header{background-color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_header_color
    wp.customize( 'royanwine_color_header_color', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-header_color-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-header_color-js-customizer">.opal-page-inner .site-header, .headerphone span{color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_headerlink_color
    wp.customize( 'royanwine_color_headerlink_color', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-headerlink_color-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-headerlink_color-js-customizer">.opal-page-inner .site-header a,.navbar-mega .navbar-nav > li > a{color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_headerlink_hover_color
    wp.customize( 'royanwine_color_headerlink_hover_color', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-headerlink_hover_color-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-headerlink_hover_color-js-customizer">.opal-page-inner .site-header a:hover,.navbar-mega .navbar-nav > li > a:hover, .navbar-mega .navbar-nav > li > a:focus,.navbar-mega .navbar-nav > li.active > a, .navbar-mega .navbar-nav > li:hover > a, .navbar-mega .navbar-nav > li:focus > a,.navbar-mega .navbar-nav > li.active > a .caret, .navbar-mega .navbar-nav > li:hover > a .caret, .navbar-mega .navbar-nav > li:focus > a .caret{color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_topbar_bg
    wp.customize( 'royanwine_color_topbar_bg', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-topbar_bg-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-topbar_bg-js-customizer">#opal-topbar{background-color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_topbartext_color
    wp.customize( 'royanwine_color_topbartext_color', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-topbartext_color-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-topbartext_color-js-customizer">#opal-topbar, #opal-topbar a{color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_topbartext_hover_color
    wp.customize( 'royanwine_color_topbartext_hover_color', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-topbartext_hover_color-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-topbartext_hover_color-js-customizer">#opal-topbar a:hover, .box-user .dropdown-toggle:hover{color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_breadscrumb_bg
    wp.customize( 'royanwine_color_breadscrumb_bg', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-breadscrumb_bg-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-breadscrumb_bg-js-customizer">#opal-breadscrumb{background:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_breadscrumb_link_hover
    wp.customize( 'royanwine_color_breadscrumb_link_hover', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-breadscrumb_link_hover-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-breadscrumb_link_hover-js-customizer">#opal-breadscrumb .breadcrumb, #opal-breadscrumb .breadcrumb *:last-child, #opal-breadscrumb .breadcrumb *:after{color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_breadscrumb_title
    wp.customize( 'royanwine_color_breadscrumb_title', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-breadscrumb_title-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-breadscrumb_title-js-customizer">#opal-breadscrumb h2.bread-title{color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_footer_bg
    wp.customize( 'royanwine_color_footer_bg', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-footer_bg-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-footer_bg-js-customizer">.opal-footer{background-color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_footer_color
    wp.customize( 'royanwine_color_footer_color', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-footer_color-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-footer_color-js-customizer">.opal-footer, .opal-footer a{color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_heading_color
    wp.customize( 'royanwine_color_heading_color', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-heading_color-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-heading_color-js-customizer">.opal-footer .widget .widget-title, .opal-footer .widget .widgettitle, .opal-footer-profile .kc-title-wrap h3.kc_title, .opal-footer-profile h3{color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_copyright_bg
    wp.customize( 'royanwine_color_copyright_bg', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-copyright_bg-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-copyright_bg-js-customizer">.opal-copyright{background-color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_copyright_color
    wp.customize( 'royanwine_color_copyright_color', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-copyright_color-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-copyright_color-js-customizer">.opal-copyright, .opal-copyright a{color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_product_bg
    wp.customize( 'royanwine_color_product_bg', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-product_bg-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-product_bg-js-customizer">.products-grid, .products-list, .products-carousel .products-collection .owl-item {background-color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_product_name
    wp.customize( 'royanwine_color_product_name', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-product_name-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-product_name-js-customizer">.product-block .name a{color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_price_color
    wp.customize( 'royanwine_color_price_color', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-price_color-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-price_color-js-customizer">.product-block .price > *{color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_price_old_color
    wp.customize( 'royanwine_color_price_old_color', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-price_old_color-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-price_old_color-js-customizer">.product-block .price del span{color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_button_groups_bgcolor
    wp.customize( 'royanwine_color_button_groups_bgcolor', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-button_groups_bgcolor-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-button_groups_bgcolor-js-customizer">.product-block .caption .button-action > div.yith-wcwl-add-to-wishlist a.add_to_wishlist i, .product-block .caption .button-action > div.yith-wcwl-add-to-wishlist a.add_to_wishlist em, .product-block .caption .button-action > div.yith-wcwl-add-to-wishlist a.compare i, .product-block .caption .button-action > div.yith-wcwl-add-to-wishlist a.compare em, .product-block .caption .button-action > div.yith-wcwl-add-to-wishlist a.quickview i, .product-block .caption .button-action > div.yith-wcwl-add-to-wishlist a.quickview em, .product-block .caption .button-action > div.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse a i, .product-block .caption .button-action > div.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse a em, .product-block .caption .button-action > div.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse a i, .product-block .caption .button-action > div.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse a em, .product-block .caption .button-action > div.yith-compare a.add_to_wishlist i, .product-block .caption .button-action > div.yith-compare a.add_to_wishlist em, .product-block .caption .button-action > div.yith-compare a.compare i, .product-block .caption .button-action > div.yith-compare a.compare em, .product-block .caption .button-action > div.yith-compare a.quickview i, .product-block .caption .button-action > div.yith-compare a.quickview em, .product-block .caption .button-action > div.yith-compare .yith-wcwl-wishlistaddedbrowse a i, .product-block .caption .button-action > div.yith-compare .yith-wcwl-wishlistaddedbrowse a em, .product-block .caption .button-action > div.yith-compare .yith-wcwl-wishlistexistsbrowse a i, .product-block .caption .button-action > div.yith-compare .yith-wcwl-wishlistexistsbrowse a em, .product-block .caption .button-action > div.quick-view a.add_to_wishlist i, .product-block .caption .button-action > div.quick-view a.add_to_wishlist em, .product-block .caption .button-action > div.quick-view a.compare i, .product-block .caption .button-action > div.quick-view a.compare em, .product-block .caption .button-action > div.quick-view a.quickview i, .product-block .caption .button-action > div.quick-view a.quickview em, .product-block .caption .button-action > div.quick-view .yith-wcwl-wishlistaddedbrowse a i, .product-block .caption .button-action > div.quick-view .yith-wcwl-wishlistaddedbrowse a em, .product-block .caption .button-action > div.quick-view .yith-wcwl-wishlistexistsbrowse a i, .product-block .caption .button-action > div.quick-view .yith-wcwl-wishlistexistsbrowse a em{background-color:'+newval.toString()+';}</style>');
            }
        } );
    } );
    // royanwine_color_button_groups_color
    wp.customize( 'royanwine_color_button_groups_color', function( value ) {
        value.bind( function( newval ) {
            $('#royanwine-button_groups_color-js-customizer').remove();
            if(newval != ''){
                $('body').append('<style id="royanwine-button_groups_color-js-customizer">.product-block .caption .button-action > div.yith-wcwl-add-to-wishlist a.add_to_wishlist i, .product-block .caption .button-action > div.yith-wcwl-add-to-wishlist a.add_to_wishlist em, .product-block .caption .button-action > div.yith-wcwl-add-to-wishlist a.compare i, .product-block .caption .button-action > div.yith-wcwl-add-to-wishlist a.compare em, .product-block .caption .button-action > div.yith-wcwl-add-to-wishlist a.quickview i, .product-block .caption .button-action > div.yith-wcwl-add-to-wishlist a.quickview em, .product-block .caption .button-action > div.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse a i, .product-block .caption .button-action > div.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistaddedbrowse a em, .product-block .caption .button-action > div.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse a i, .product-block .caption .button-action > div.yith-wcwl-add-to-wishlist .yith-wcwl-wishlistexistsbrowse a em, .product-block .caption .button-action > div.yith-compare a.add_to_wishlist i, .product-block .caption .button-action > div.yith-compare a.add_to_wishlist em, .product-block .caption .button-action > div.yith-compare a.compare i, .product-block .caption .button-action > div.yith-compare a.compare em, .product-block .caption .button-action > div.yith-compare a.quickview i, .product-block .caption .button-action > div.yith-compare a.quickview em, .product-block .caption .button-action > div.yith-compare .yith-wcwl-wishlistaddedbrowse a i, .product-block .caption .button-action > div.yith-compare .yith-wcwl-wishlistaddedbrowse a em, .product-block .caption .button-action > div.yith-compare .yith-wcwl-wishlistexistsbrowse a i, .product-block .caption .button-action > div.yith-compare .yith-wcwl-wishlistexistsbrowse a em, .product-block .caption .button-action > div.quick-view a.add_to_wishlist i, .product-block .caption .button-action > div.quick-view a.add_to_wishlist em, .product-block .caption .button-action > div.quick-view a.compare i, .product-block .caption .button-action > div.quick-view a.compare em, .product-block .caption .button-action > div.quick-view a.quickview i, .product-block .caption .button-action > div.quick-view a.quickview em, .product-block .caption .button-action > div.quick-view .yith-wcwl-wishlistaddedbrowse a i, .product-block .caption .button-action > div.quick-view .yith-wcwl-wishlistaddedbrowse a em, .product-block .caption .button-action > div.quick-view .yith-wcwl-wishlistexistsbrowse a i, .product-block .caption .button-action > div.quick-view .yith-wcwl-wishlistexistsbrowse a em{color:'+newval.toString()+';}</style>');
            }
        } );
    } );

    /* OpalTool: inject code */
    /* OpalTool: end inject code */
} )( jQuery );