(function () {
    "use strict";
    jQuery(document).ready(function ($) {
        //show popup video
        $("a[data-rel^='prettyPhoto[videos]']").prettyPhoto({
            animation_speed: 'normal',
            theme          : 'light_rounded',
            social_tools   : false,
            allow_resize   : true, /* Resize the photos bigger than viewport. true/false */
            default_width  : 800,
            default_height : 450,
        });

        $("a[rel^='prettyPhoto[pp_gal]']").prettyPhoto({
            animation_speed: 'normal',
            theme          : 'light_rounded',
            social_tools   : false,
            allow_resize   : true, /* Resize the photos bigger than viewport. true/false */
            default_width  : 800,
            default_height : 450,
        });


        /**********************************************************************
         * Login Ajax
         **********************************************************************/
        $('#pbrlostpasswordform').hide();
        $('#modalLoginForm form .btn-cancel').on('click', function () {
            $('#modalLoginForm').modal('hide');
        });


        // sign in proccess
        $('form.login-form').on('submit', function () {
            var $this = $(this);
            $('.alert', this).remove();
            $.ajax({
                url     : royanwineajax.ajaxurl,
                type    : 'POST',
                dataType: 'json',
                data    : $(this).serialize() + "&action=pbrajaxlogin"
            }).done(function (data) {
                if (data.loggedin) {
                    $this.prepend('<div class="alert alert-info">' + data.message + '</div>');
                    location.reload();
                } else {
                    $this.prepend('<div class="alert alert-warning">' + data.message + '</div>');
                }
            });
            return false;
        });
        $('form .toggle-links').on('click', function () {
            $('.form-wrapper').hide();
            $($(this).attr('href')).show();
            return false;
        });

        // sign in proccess
        $('form.lostpassword-form').on('submit', function () {
            var $this = $(this);
            $('.alert', this).remove();
            $.ajax({
                url     : royanwineajax.ajaxurl,
                type    : 'POST',
                dataType: 'json',
                data    : $(this).serialize() + "&action=pbrajaxlostpass"
            }).done(function (data) {
                if (data.loggedin) {
                    $this.prepend('<div class="alert alert-info">' + data.message + '</div>');
                    location.reload();
                } else {
                    $this.prepend('<div class="alert alert-warning">' + data.message + '</div>');
                }
            });
            return false;
        });


        //Sticky menu header
        var nav = jQuery('.has-sticky');
        if (nav) {
            jQuery(window).scroll(function () {
                if ($(this).scrollTop() > 52) {
                    nav.addClass("keeptop");
                } else {
                    nav.removeClass("keeptop");
                }
            });
        }


        //Offcanvas Menu
        $('[data-toggle="offcanvas"], .btn-offcanvas').on('click', function () {
            $('.row-offcanvas').toggleClass('active');
            $('#pbr-off-canvas').toggleClass('active');
        });

        //mobile click
        $('#main-menu-offcanvas .caret').on('click', function () {
            var $a = jQuery(this);
            $a.parent().find('> .dropdown-menu').slideToggle();
            if ($a.parent().hasClass('level-0')) {
                if ($a.parent().hasClass('show')) {
                    $a.parent().removeClass('show');
                } else {
                    $a.parents('li').siblings('li').find('ul:visible').slideUp().parent().removeClass('show');
                    $a.parent().addClass('show');
                }
            }

        });

        $('.showright').on('click', function () {
            $('.offcanvas-showright').toggleClass('active');
        });


        /*---------------------------------------------- 
         *    Apply Filter        
         *----------------------------------------------*/
        jQuery('.isotope-filter li a').on('click', function () {

            var parentul = jQuery(this).parents('ul.isotope-filter').data('related-grid');
            jQuery(this).parents('ul.isotope-filter').find('li a').removeClass('active');
            jQuery(this).addClass('active');
            var selector = jQuery(this).attr('data-option-value');
            jQuery('#' + parentul).isotope({filter: selector}, function () {
            });

            return (false);
        });

        /**
         *
         */
        $(".dropdown-toggle-overlay").on('click', function () {
            $($(this).data('target')).addClass("active");
        });

        $(".dropdown-toggle-button").on('click', function () {
            $($(this).data('target')).removeClass("active");
            return false;
        });

        /**
         *
         * Automatic apply  OWL carousel
         */
        $(".owl-carousel-play .owl-carousel").each(function () {
            var config = {
                slideSpeed     : 300,
                paginationSpeed: 400,
                pagination     : $(this).data('pagination'),
                autoHeight     : true,
                navigationText : ["<i class='fa fa-long-arrow-left'></i>", "<i class='fa fa-long-arrow-right'></i>"]
                //afterAction: afterAction
            };

            var owl = $(this);
            if ($(this).data('slide') == 1) {
                config.singleItem = true;
            } else {
                config.items = $(this).data('slide');
            }
            if ($(this).data('desktop')) {
                config.itemsDesktop = $(this).data('desktop');
            }
            if ($(this).data('desktopsmall')) {
                config.itemsDesktopSmall = $(this).data('desktopsmall');
            }
            if ($(this).data('tablet')) {
                config.itemsTablet = $(this).data('tablet');
            }
            if ($(this).data('tabletsmall')) {
                config.itemsTabletSmall = $(this).data('tabletsmall');
            }
            if ($(this).data('mobile')) {
                config.itemsMobile = $(this).data('mobile');
            }
            if ($('.pbr-owl-thumbs li', $(this).parent()).length > 0) {
                config.afterAction = function () {
                    $('.pbr-owl-thumbs li').removeClass('active');
                    $('.pbr-owl-thumbs li').eq(this.owl.currentItem).addClass('active');
                };
            }
            $(this).owlCarousel(config);
            $('.left', $(this).parent()).on('click', function () {
                owl.trigger('owl.prev');
                return false;
            });
            $('.right', $(this).parent()).on('click', function () {
                owl.trigger('owl.next');
                return false;
            });
            $('.thumbs li', $(this)).on('click', function () {
                owl.trigger('owl.next');
                return false;
            });
            $('.pbr-owl-thumbs li', $(this).parent()).on('click', function () {
                var index = $(this).index();
                owl.trigger('owl.goTo', index);
                $('.pbr-owl-thumbs li').removeClass('active');
                $(this).addClass('active');
                return false;
            });
            //owl.config.afterAction =

        });


        /**
         *
         */
        if ($('.page-static-left')) {
            $('.page-static-left .button-action').on('click', function () {
                $('.page-static-left').toggleClass('active');
            });
        }

        //fix map
        if ($('.wpb_map_wraper').length > 0) {
            $('.wpb_map_wraper').on('click', function () {
                $('.wpb_map_wraper iframe').css("pointer-events", "auto");
            });

            $(".wpb_map_wraper").mouseleave(function () {
                $('.wpb_map_wraper iframe').css("pointer-events", "none");
            });
        }
        //counter up
        if ($('.counterUp').length > 0) {
            $('.counterUp').counterUp({
                delay: 10,
                time : 1500
            });
        }


        if ($('html').attr('dir') === 'rtl') {
            $('[data-vc-full-width="true"]').each(function (i, v) {
                $(this).css('right', -$(this).offset().left).css('left', 'auto');
            });
        }

        jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 200) {
                jQuery('.scrollup').fadeIn();
            } else {
                jQuery('.scrollup').fadeOut();
            }
        });
        jQuery('.scrollup').on('click', function () {
            jQuery("html, body").animate({scrollTop: 0}, 600);
            return false;
        });

        if(jQuery('.schedulepopup')){
            jQuery('.schedulepopup .datepicker').datetimepicker({
                timepicker:false,
                format:'m/d/Y'
            });

            jQuery('.schedulepopup .timepicker').datetimepicker({
                datepicker:false,
                format:'H:i'
            });
        }

    });

})(jQuery);

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires     = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca   = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

// Woocommerce quantity

function opalAddQuantityBoxes() {
    var $quantitySelector = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '.qty';


    var $quantityBoxes = void 0;

    $quantityBoxes = jQuery('div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)').find($quantitySelector);

    if ($quantityBoxes && 'date' != $quantityBoxes.prop('type')) {

        // Add plus and minus boxes
        $quantityBoxes.parent().addClass('buttons_added').prepend('<input type="button" value="-" class="minus" />');
        $quantityBoxes.addClass('input-text').after('<input type="button" value="+" class="plus" />');

        // Target quantity inputs on product pages
        jQuery('input' + $quantitySelector + ':not(.product-quantity input' + $quantitySelector + ')').each(function () {
            var $min = parseFloat(jQuery(this).attr('min'));

            if ($min && $min > 0 && parseFloat(jQuery(this).val()) < $min) {
                $(this).val($min);
            }
        });

        jQuery('.plus, .minus').unbind('click');

        jQuery('.plus, .minus').on('click', function () {

            // Get values
            var $quantityBox     = jQuery(this).parent().find($quantitySelector),
                $currentQuantity = parseFloat($quantityBox.val()),
                $maxQuantity     = parseFloat($quantityBox.attr('max')),
                $minQuantity     = parseFloat($quantityBox.attr('min')),
                $step            = $quantityBox.attr('step');

            // Fallback default values
            if (!$currentQuantity || '' === $currentQuantity || 'NaN' === $currentQuantity) {
                $currentQuantity = 0;
            }
            if ('' === $maxQuantity || 'NaN' === $maxQuantity) {
                $maxQuantity = '';
            }

            if ('' === $minQuantity || 'NaN' === $minQuantity) {
                $minQuantity = 0;
            }
            if ('any' === $step || '' === $step || undefined === $step || 'NaN' === parseFloat($step)) {
                $step = 1;
            }

            // Change the value
            if (jQuery(this).is('.plus')) {

                if ($maxQuantity && ($maxQuantity == $currentQuantity || $currentQuantity > $maxQuantity)) {
                    $quantityBox.val($maxQuantity);
                } else {
                    $quantityBox.val($currentQuantity + parseFloat($step));
                }
            } else {

                if ($minQuantity && ($minQuantity == $currentQuantity || $currentQuantity < $minQuantity)) {
                    $quantityBox.val($minQuantity);
                } else if ($currentQuantity > 0) {
                    $quantityBox.val($currentQuantity - parseFloat($step));
                }
            }

            // Trigger change event
            $quantityBox.trigger('change');
        });
    }
}

jQuery(document).ready(function () {
    opalAddQuantityBoxes();
});
jQuery(document).ajaxComplete(function () {
    opalAddQuantityBoxes();
});
