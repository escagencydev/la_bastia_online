<?php

function royanwine_fnc_import_remote_demos() {
   $sample_packages = wp_remote_get( 'http://wpsampledemo.com/royanwine/package.json');
   if ( is_wp_error( $sample_packages ) ) {
      if ( $return ) {
         return false;
      }

      wp_send_json_error( esc_html__( 'Failed to retrieve available sample data packages.', 'royanwine' ) );
   }
   $sample_packages = $sample_packages['body'];
   $obj = json_decode($sample_packages);
   $package_r = array();
   if(!empty($obj)){
      $packages = $obj->package;
      for ($i = 0 ;$i < count($packages);$i++) {
         $package_r[$packages[$i]->name] = array(
               'name'      => $packages[$i]->name,
               'source' => $packages[$i]->source,
               'preview'   => $packages[$i]->preview
         );
      }
   }else{
      echo '<div class="error">'.esc_html__('Please check json is wrong! missing ","','royanwine').'</div>';
   }
   return $package_r;
}
add_filter( 'pbrthemer_import_remote_demos', 'royanwine_fnc_import_remote_demos' );



function royanwine_fnc_import_theme() {
   return 'royanwine';
}
add_filter( 'pbrthemer_import_theme', 'royanwine_fnc_import_theme' );

function royanwine_fnc_import_demos() {
   $folderes = glob( get_template_directory().'/inc/import/*', GLOB_ONLYDIR ); 

   $output = array(); 

   foreach( $folderes as $folder ){
      $output[basename( $folder )] = basename( $folder );
   }
   
   return $output;
}
add_filter( 'pbrthemer_import_demos', 'royanwine_fnc_import_demos' );

function royanwine_fnc_import_types() {
   return array(
         'all' => 'All',
         'content' => 'Content',
         'widgets' => 'Widgets',
         'page_options' => 'Theme + Page Options',
         'menus' => 'Menus',
         'rev_slider' => 'Revolution Slider',
         'vc_templates' => 'VC Templates'
      );
}
add_filter( 'pbrthemer_import_types', 'royanwine_fnc_import_types' );

/**
 * Matching and resizing images with url.
 *
 *
 */
function royanwine_import_attachment_image_size( $url ){  

   $name = basename( $url );   
 
   $ouput = array(
         'allowed' => 0
   );
    if( preg_match("#post#", $name) ) {
        $ouput = array(
            'allowed' => 1,
            'height'  => 659,
            'width'   => 1170,
            'file_name' => 'post_demo.jpg'
        );
    }
    elseif( preg_match("#team#", $name) ){
        $ouput = array(
            'allowed' => 1,
            'height'  => 440,
            'width'   => 290,
            'file_name' => 'team_demo.jpg'
        );
    }
    elseif( preg_match("#product#", $name) ){
        $ouput = array(
            'allowed' => 1,
            'height'  => 850,
            'width'   => 560,
            'file_name' => 'product_demo.jpg'
        );
    }
    elseif( preg_match("#slide#", $name) ){
        $ouput = array(
            'allowed' => 1,
            'height'  => 880,
            'width'   => 1920,
            'file_name' => 'slide_demo.jpg'
        );
    }
    return $ouput;
}

add_filter( 'pbrthemer_import_attachment_image_size', 'royanwine_import_attachment_image_size' , 1 , 999 );
