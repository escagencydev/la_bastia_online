<?php
if (class_exists( "WPBakeryShortCode" )) {
    /**
     * Class Royanwine_VC_Woocommerces
     *
     */
    class Royanwine_VC_Woocommerce extends Vc_Vendor_Woocommerce {

        /**
         * register and mapping shortcodes
         */
        public function product_category_field_search($search_string) {
            $data = array();
            $vc_taxonomies_types = array( 'product_cat' );
            $vc_taxonomies = get_terms( $vc_taxonomies_types, array(
                'hide_empty' => false,
                'search' => $search_string
            ) );
            if (is_array( $vc_taxonomies ) && !empty( $vc_taxonomies )) {
                foreach ($vc_taxonomies as $t) {
                    if (is_object( $t )) {
                        $data[] = vc_get_term_object( $t );
                    }
                }
            }
            return $data;
        }


        public function product_category_render($query) {
            $category = get_term_by( 'id', (int)$query['value'], 'product_cat' );
            if (!empty( $query ) && !empty( $category )) {
                $data = array();
                $data['value'] = $category->term_id;
                $data['label'] = $category->name;
                return !empty( $data ) ? $data : false;
            }
            return false;
        }

        public function product_sale_field_search($query){
            global $wpdb;

            $product_id = (int) $query;
            $product_on_sale = implode(',', wc_get_product_ids_on_sale());
            $post_meta_infos = $wpdb->get_results( $wpdb->prepare( "SELECT a.ID AS id, a.post_title AS title, b.meta_value AS sku
                        FROM {$wpdb->posts} AS a
                        LEFT JOIN ( SELECT meta_value, post_id  FROM {$wpdb->postmeta} WHERE `meta_key` = '_sku' ) AS b ON b.post_id = a.ID
                        WHERE a.post_type = 'product' AND a.ID IN ({$product_on_sale}) AND ( a.ID = '%d' OR b.meta_value LIKE '%%%s%%' OR a.post_title LIKE '%%%s%%' )", $product_id > 0 ? $product_id : - 1, stripslashes( $query ), stripslashes( $query ) ), ARRAY_A );

            $results = array();
            if ( is_array( $post_meta_infos ) && ! empty( $post_meta_infos ) ) {
                foreach ( $post_meta_infos as $value ) {
                    $data = array();
                    $data['value'] = $value['id'];
                    $data['label'] = esc_html__( 'Id', 'royanwine' ) . ': ' . $value['id'] . ( ( strlen( $value['title'] ) > 0 ) ? ' - ' . esc_html__( 'Title', 'royanwine' ) . ': ' . $value['title'] : '' ) . ( ( strlen( $value['sku'] ) > 0 ) ? ' - ' . esc_html__( 'Sku', 'royanwine' ) . ': ' . $value['sku'] : '' );
                    $results[] = $data;
                }
            }

            return $results;
        }

        public function product_sale_field_render($query){
            $query = trim( $query['value'] ); // get value from requested
            if ( ! empty( $query ) ) {
                $product_object = wc_get_product( (int) $query );
                if ( is_object( $product_object ) ) {
                    $product_sku = $product_object->get_sku();
                    $product_title = $product_object->get_title();
                    $product_id = $product_object->get_id();

                    $product_sku_display = '';
                    if ( ! empty( $product_sku ) ) {
                        $product_sku_display = ' - ' . esc_html__( 'Sku', 'royanwine' ) . ': ' . $product_sku;
                    }

                    $product_title_display = '';
                    if ( ! empty( $product_title ) ) {
                        $product_title_display = ' - ' . esc_html__( 'Title', 'royanwine' ) . ': ' . $product_title;
                    }

                    $product_id_display = esc_html__( 'Id', 'royanwine' ) . ': ' . $product_id;

                    $data = array();
                    $data['value'] = $product_id;
                    $data['label'] = $product_id_display . $product_title_display . $product_sku_display;

                    return ! empty( $data ) ? $data : false;
                }
                return false;
            }
            return false;
        }

        /**
         * register and mapping shortcodes
         */
        public function load() {

            $shortcodes = array( 'pbr_categoriestabs', 'pbr_products', 'pbr_products_collection' );

            //Filters For autocomplete param:
            //For suggestion: vc_autocomplete_[shortcode_name]_[param_name]_callback
            add_filter( 'vc_autocomplete_pbr_single_product_id_callback', array(
                &$this,
                'productIdAutocompleteSuggester',
            ), 10, 1 ); // Get suggestion(find). Must return an array
            add_filter( 'vc_autocomplete_pbr_single_product_id_render', array(
                &$this,
                'productIdAutocompleteRender',
            ), 10, 1 ); // Render exact product. Must return an array (label,value)
            //For param: ID default value filter
            add_filter( 'vc_form_fields_render_field_pbr_single_product_id_param_value', array(
                &$this,
                'productIdDefaultValue',
            ), 10, 4 ); // Defines default value for param if not provided. Takes from other param value.


            foreach ($shortcodes as $shortcode) {
                add_filter( 'vc_autocomplete_' . $shortcode . '_categories_callback', array( $this, 'product_category_field_search' ), 10, 1 );
                add_filter( 'vc_autocomplete_' . $shortcode . '_categories_render', array( $this, 'product_category_render' ), 10, 1 );
            }

            add_filter( 'vc_autocomplete_pbr_products_deals_list_productids_callback', array( $this, 'product_sale_field_search' ), 10, 1 );
            add_filter( 'vc_autocomplete_pbr_products_deals_list_productids_render', array( $this, 'product_sale_field_render' ), 10, 1 );


            $order_by_values = array(
                '',
                esc_html__( 'Date', 'royanwine' ) => 'date',
                esc_html__( 'ID', 'royanwine' ) => 'ID',
                esc_html__( 'Author', 'royanwine' ) => 'author',
                esc_html__( 'Title', 'royanwine' ) => 'title',
                esc_html__( 'Modified', 'royanwine' ) => 'modified',
                esc_html__( 'Random', 'royanwine' ) => 'rand',
                esc_html__( 'Comment count', 'royanwine' ) => 'comment_count',
                esc_html__( 'Menu order', 'royanwine' ) => 'menu_order',
            );

            $order_way_values = array(
                '',
                esc_html__( 'Descending', 'royanwine' ) => 'DESC',
                esc_html__( 'Ascending', 'royanwine' ) => 'ASC',
            );
            $product_categories_dropdown = array( '' => esc_html__( 'None', 'royanwine' ) );
            $block_styles = royanwine_fnc_get_widget_block_styles();

            $product_columns_deal = array( 1, 2, 3, 4 );

            if (is_admin()) {
                $args = array(
                    'type' => 'post',
                    'child_of' => 0,
                    'parent' => '',
                    'orderby' => 'name',
                    'order' => 'ASC',
                    'hide_empty' => false,
                    'hierarchical' => 1,
                    'exclude' => '',
                    'include' => '',
                    'number' => '',
                    'taxonomy' => 'product_cat',
                    'pad_counts' => false,

                );

                $categories = get_categories( $args );
                royanwine_fnc_woocommerce_getcategorychilds( 0, 0, $categories, 0, $product_categories_dropdown );

            }

            /**
             * pbr_productcategory
             */


            $product_layout = array( 
                'Grid' => 'grid', 
                'List' => 'list', 
                'Carousel' => 'carousel', 
                'Special' => 'special', 
                'List-v1' => 'list-v1' 
            );

            $image_size = array( 
                'Full' => 'full', 
                'Catalog' => 'catalog', 
                'Thumbnail' => 'thumbnail', 
            );
            $product_type = array( 'Best Selling' => 'best_selling', 'Featured Products' => 'featured_product', 'Top Rate' => 'top_rate', 'Recent Products' => 'recent_product', 'On Sale' => 'on_sale', 'Recent Review' => 'recent_review' );
            $product_columns = array( 6, 5, 4, 3, 2, 1 );
            $show_tab = array(
                array( 'recent', esc_html__( 'Latest Products', 'royanwine' ) ),
                array( 'featured_product', esc_html__( 'Featured Products', 'royanwine' ) ),
                array( 'best_selling', esc_html__( 'BestSeller Products', 'royanwine' ) ),
                array( 'top_rate', esc_html__( 'TopRated Products', 'royanwine' ) ),
                array( 'on_sale', esc_html__( 'Special Products', 'royanwine' ) )
            );

            vc_map( array(
                "name" => esc_html__( "PBR Product Category", 'royanwine' ),
                "base" => "pbr_productcategory",
                "class" => "",
                'icon' => get_template_directory_uri() . "/images/opal.png",
                "category" => esc_html__( 'PBR Woocommerce', 'royanwine' ),
                'description' => esc_html__( 'Show Products In Carousel, Grid, List, Special', 'royanwine' ),
                'php_class_name' => 'WPBakeryShortCode_Royanwine_Theme',
                "params" => array(
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__( 'Title', 'royanwine' ),
                        "param_name" => "title",
                        "value" => ''
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__( 'Sub Title', 'royanwine' ),
                        "param_name" => "subtitle",
                    ),
                    array(
                        "type" => "dropdown",
                        "class" => "",
                        "heading" => esc_html__( 'Category', 'royanwine' ),
                        "param_name" => "category",
                        "value" => $product_categories_dropdown,
                        "admin_label" => true
                    ),
                    array(
                        "type" => "dropdown",
                        "heading" => esc_html__( "Style", 'royanwine' ),
                        "param_name" => "style",
                        "value" => $product_layout
                    ),
                    array(
                        "type" => "attach_image",
                        "description" => esc_html__( "Upload an image for categories", 'royanwine' ),
                        "param_name" => "image_cat",
                        "value" => '',
                        'heading' => esc_html__( 'Image', 'royanwine' )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => esc_html__( "Number of products to show", 'royanwine' ),
                        "param_name" => "number",
                        "value" => '4'
                    ),
                    array(
                        "type" => "dropdown",
                        "heading" => esc_html__( "Columns count", 'royanwine' ),
                        "param_name" => "columns_count",
                        "value" => array( 6, 5, 4, 3, 2, 1 ),
                        "admin_label" => true,
                        "description" => esc_html__( "Select columns count.", 'royanwine' )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => esc_html__( "Extra class name", 'royanwine' ),
                        "param_name" => "el_class",
                        "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'royanwine' )
                    )
                )
            ) );

            /**
             * pbr_category_filter
             */

            vc_map( array(
                "name" => esc_html__( "PBR Product Categories Filter", 'royanwine' ),
                "base" => "pbr_category_filter",
                'description' => esc_html__( 'Show images and links of sub categories in block', 'royanwine' ),
                "class" => "",
                'icon' => get_template_directory_uri() . "/images/opal.png",
                "category" => esc_html__( 'PBR Woocommerce', 'royanwine' ),
                'php_class_name' => 'WPBakeryShortCode_Royanwine_Theme',
                "params" => array(

                    array(
                        "type" => "dropdown",
                        "heading" => esc_html__( 'Category', 'royanwine' ),
                        "param_name" => "term_id",
                        "value" => $product_categories_dropdown, "admin_label" => true
                    ),
                    array(
                        "type" => "dropdown",
                        "heading" => esc_html__( "Style", 'royanwine' ),
                        "param_name" => "style",
                        'value' => array(
                            esc_html__( 'Default', 'royanwine' ) => '',
                            esc_html__( 'style 1', 'royanwine' ) => 'style1',
                        ),
                        'std' => ''
                    ),
                    array(
                        "type" => "attach_image",
                        "description" => esc_html__( "Upload an image for categories (190px x 190px)", 'royanwine' ),
                        "param_name" => "image_cat",
                        "value" => '',
                        'heading' => esc_html__( 'Image', 'royanwine' )
                    ),

                    array(
                        "type" => "textfield",
                        "heading" => esc_html__( "Number of categories to show", 'royanwine' ),
                        "param_name" => "number",
                        "value" => '5',

                    ),

                    array(
                        "type" => "textfield",
                        "heading" => esc_html__( "Extra class name", 'royanwine' ),
                        "param_name" => "el_class",
                        "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'royanwine' )
                    )
                )
            ) );

            /**
             * pbr_products
             */
            vc_map( array(
                "name" => esc_html__( "PBR Products", 'royanwine' ),
                "base" => "pbr_products",
                'description' => esc_html__( 'Show products as bestseller, featured in block', 'royanwine' ),
                "class" => "",
                'icon' => get_template_directory_uri() . "/images/opal.png",
                "category" => esc_html__( 'PBR Woocommerce', 'royanwine' ),
                'php_class_name' => 'WPBakeryShortCode_Royanwine_Theme',
                "params" => array(
                    array(
                        "type" => "textfield",
                        "heading" => esc_html__( "Title", 'royanwine' ),
                        "param_name" => "title",
                        "admin_label" => true,
                        "value" => ''
                    ),
                    array(
                        "type" => "textfield",
                        "class" => "",
                        "heading" => esc_html__( 'Sub Title', 'royanwine' ),
                        "param_name" => "subtitle",
                    ),
                    array(
                        'type' => 'autocomplete',
                        'heading' => esc_html__( 'Categories', 'royanwine' ),
                        'value' => '',
                        'param_name' => 'categories',
                        "admin_label" => true,
                        'description' => esc_html__( 'Select Categories', 'royanwine' ),
                        'settings' => array(
                            'multiple' => true,
                            'unique_values' => true,
                            // In UI show results except selected. NB! You should manually check values in backend
                        ),
                    ),
                    array(
                        "type" => "dropdown",
                        "heading" => esc_html__( "Type", 'royanwine' ),
                        "param_name" => "type",
                        "value" => $product_type,
                        "admin_label" => true,
                        "description" => esc_html__( "Select columns count.", 'royanwine' )
                    ),
                    array(
                        "type" => "dropdown",
                        "heading" => esc_html__( "Style", 'royanwine' ),
                        "param_name" => "style",
                        "value" => $product_layout
                    ),
                    array(
                        "type" => "dropdown",
                        "heading" => esc_html__( "Columns count", 'royanwine' ),
                        "param_name" => "columns_count",
                        "value" => $product_columns,
                        "admin_label" => true,
                        "description" => esc_html__( "Select columns count.", 'royanwine' ),
                        "description" => esc_html__("Select columns count.",'royanwine'),
                        'dependency' => array(
                             'element' => 'style',
                             'value' => array('grid', 'carousel', 'special')
                        ),
                    ),
                    array(
                        "type" => "dropdown",
                        "heading" => esc_html__( "Image size", 'royanwine' ),
                        "param_name" => "image_size",
                        "value" => $image_size,
                        'dependency' => array(
                             'element' => 'style',
                             'value' => array('special')
                        ),
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => esc_html__( "Number of products to show", 'royanwine' ),
                        "param_name" => "number",
                        "value" => '4'
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => esc_html__( "Extra class name", 'royanwine' ),
                        "param_name" => "el_class",
                        "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'royanwine' )
                    )
                )
            ) );

            /* PBR Product Info
            ---------------------------------------------------------- */
            vc_map( array(
                'name' => esc_html__( 'PBR Product Info', 'royanwine' ),
                'base' => 'pbr_product_info',
                "class" => "",
                'icon' => get_template_directory_uri() . "/images/opal.png",
                "category" => esc_html__( 'PBR Woocommerce', 'royanwine' ),
                'description' => esc_html__( 'Show info with banner', 'royanwine' ),
                'php_class_name' => 'WPBakeryShortCode_Royanwine_Theme',
                "params" => array(
                    array(
                        "type" => "attach_image",
                        "description" => esc_html__( "If you upload an image, icon will not show.", 'royanwine' ),
                        "param_name" => "image",
                        "value" => '',
                        'heading' => esc_html__( 'Image', 'royanwine' )
                    ),

                    array(
                        "type" => "textarea_html",
                        'heading' => esc_html__( 'Description', 'royanwine' ),
                        "param_name" => "content",
                        "value" => '',
                        'description' => esc_html__( 'Enter description for title.', 'royanwine' )
                    ),

                    array(
                        "type" => "dropdown",
                        "heading" => esc_html__( "Skin", "royanwine" ),
                        "param_name" => "skin",
                        "value" => array(
                            'Light' => 'light',
                            'Dark' => 'dark',
                        ),
                        "admin_label" => true,
                        "description" => esc_html__( "Select Skin.", "royanwine" )
                    ),

                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__( 'Extra class name', 'royanwine' ),
                        'param_name' => 'el_class',
                        'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'royanwine' )
                    ),
                ),
            ) );


            /**
             * pbr_productcats_tabs
             */
            $sortby = array(
                array( 'recent_product', esc_html__( 'Latest Products', 'royanwine' ) ),
                array( 'featured_product', esc_html__( 'Featured Products', 'royanwine' ) ),
                array( 'best_selling', esc_html__( 'BestSeller Products', 'royanwine' ) ),
                array( 'top_rate', esc_html__( 'TopRated Products', 'royanwine' ) ),
                array( 'on_sale', esc_html__( 'Special Products', 'royanwine' ) ),
                array( 'recent_review', esc_html__( 'Recent Products Reviewed', 'royanwine' ) )
            );
            $layout_type = array(
                esc_html__( 'Carousel', 'royanwine' ) => 'carousel',
                esc_html__( 'Grid', 'royanwine' ) => 'grid'
            );

        }
    }

    /**
     * Register Woocommerce Vendor which will register list of shortcodes
     */
    function royanwine_fnc_init_vc_woocommerce_vendor() {

        $vendor = new Royanwine_VC_Woocommerce();
        add_action( 'vc_after_set_mode', array(
            $vendor,
            'load'
        ) );

    }

    add_action( 'after_setup_theme', 'royanwine_fnc_init_vc_woocommerce_vendor', 9 );
}		