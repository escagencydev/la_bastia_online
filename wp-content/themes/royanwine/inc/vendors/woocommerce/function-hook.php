<?php

function royanwine_woocommerce_enqueue_scripts() {
	wp_enqueue_script( 'royanwine-woocommerce', get_template_directory_uri() . '/js/woocommerce.js', array( 'jquery', 'suggest' ), '20131022', true );
}
add_action( 'wp_enqueue_scripts', 'royanwine_woocommerce_enqueue_scripts' );


/**
 */
add_filter('woocommerce_add_to_cart_fragments', 'royanwine_fnc_woocommerce_header_add_to_cart_fragment' );

function royanwine_fnc_woocommerce_header_add_to_cart_fragment( $fragments ){
	global $woocommerce;

	$fragments['#cart .mini-cart-items'] =  sprintf(_n(' <span class="mini-cart-items"> %d  </span> ', ' <span class="mini-cart-items"> %d <em>item</em> </span> ', $woocommerce->cart->cart_contents_count, 'royanwine'), $woocommerce->cart->cart_contents_count);
 	$fragments['#cart .mini-cart-total'] = trim( $woocommerce->cart->get_cart_total() );
    
    return $fragments;
}

/**
 * Mini Basket
 */
if(!function_exists('royanwine_fnc_minibasket')){
    function royanwine_fnc_minibasket( $style='' ){ 
        $template =  apply_filters( 'wpopal_fnc_minibasket_template', royanwine_fnc_get_header_layout( '' )  );  
        return get_template_part( 'woocommerce/cart/mini-cart-button', $template ); 
    }
}
if(royanwine_fnc_theme_options("woo-show-minicart",true)){
	add_action( 'royanwine_template_header_right', 'royanwine_fnc_minibasket', 30, 0 );
}
/******************************************************
 * 												   	  *
 * Hook functions applying in archive, category page  *
 *												      *
 ******************************************************/
function royanwine_template_woocommerce_main_container_class( $class ){ 
	if( is_product() ){
		$class .= ' '.  royanwine_fnc_theme_options('woocommerce-single-layout') ;
	}else if( is_product_category() || is_archive()  ){ 
		$class .= ' '.  royanwine_fnc_theme_options('woocommerce-archive-layout') ;
	}
	return $class;
}
add_filter( 'royanwine_template_woocommerce_main_container_class', 'royanwine_template_woocommerce_main_container_class' );


function royanwine_fnc_get_woocommerce_sidebar_configs( $configs='' ){

	global $post; 
	$right = null; $left = null; 

	if( is_product() ){
		$left  	=  royanwine_fnc_theme_options( 'woocommerce-single-left-sidebar', 'sidebar-left' ); 
		$right 	=  royanwine_fnc_theme_options( 'woocommerce-single-right-sidebar', 'sidebar-right' );
		$layout =  royanwine_fnc_theme_options( 'woocommerce-single-layout', 'mainright');  

	}else if( is_product_category() || is_archive() ){
		$left  	=  royanwine_fnc_theme_options( 'woocommerce-archive-left-sidebar', 'sidebar-left' ); 
		$right 	=  royanwine_fnc_theme_options( 'woocommerce-archive-right-sidebar', 'sidebar-right' );
		$layout =  royanwine_fnc_theme_options( 'woocommerce-archive-layout', 'mainright' ); 
	}

 
	return royanwine_fnc_get_layout_configs( $layout, $left, $right, $configs );
}

add_filter( 'royanwine_fnc_get_woocommerce_sidebar_configs', 'royanwine_fnc_get_woocommerce_sidebar_configs', 1, 1 );


function royanwine_woocommerce_breadcrumb_defaults( $args ){
    $parallax = royanwine_fnc_theme_options('parallax-breadcrumb') ? 'has-parallax' : 'no-parallax';
	$args['wrap_before'] = '<div class="pbr-breadscrumb '.$parallax.' "><div class="container"><ol class="pbr-woocommerce-breadcrumb breadcrumb" ' . ( is_single() ? 'itemprop="breadcrumb"' : '' ) . '>';
	$args['wrap_after'] = '</ol></div></div>';

	return $args;
}

add_filter( 'woocommerce_breadcrumb_defaults', 'royanwine_woocommerce_breadcrumb_defaults' );

add_action( 'royanwine_woo_template_main_before', 'woocommerce_breadcrumb', 30, 0 );
/**
 * Remove show page results which display on top left of listing products block.
 */
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
add_action( 'woocommerce_after_shop_loop', 'woocommerce_result_count', 10 );


function royanwine_fnc_woocommerce_after_shop_loop_start(){
	echo '<div class="products-bottom-wrap clearfix">';
}

add_action( 'woocommerce_after_shop_loop', 'royanwine_fnc_woocommerce_after_shop_loop_start', 1 );

function royanwine_fnc_woocommerce_after_shop_loop_after(){
	echo '</div>';
}

add_action( 'woocommerce_after_shop_loop', 'royanwine_fnc_woocommerce_after_shop_loop_after', 10000 );


/**
 * Wrapping all elements are wrapped inside Div Container which rendered in woocommerce_before_shop_loop hook
 */
function royanwine_woocommerce_before_shop_loop_start(){
	echo '<div class="products-top-wrap clearfix">';
}

function royanwine_woocommerce_before_shop_loop_end(){
	echo '</div>';
}


add_action( 'woocommerce_before_shop_loop', 'royanwine_woocommerce_before_shop_loop_start' , 0 );
add_action( 'woocommerce_before_shop_loop', 'royanwine_woocommerce_before_shop_loop_end' , 1000 );


function royanwine_fnc_woocommerce_display_modes(){
	$woo_display = 'grid';
	if (isset($_GET['display'])){
		$woo_display = $_GET['display'];
	}
	echo '<form class="display-mode" method="get">';
		echo '<button title="'.esc_html__('Grid','royanwine').'" class="btn '.($woo_display == 'grid' ? 'active' : '').'" value="grid" name="display" type="submit"><i class="fa fa-th"></i></button>';	
		echo '<button title="'.esc_html__( 'List', 'royanwine' ).'" class="btn '.($woo_display == 'list' ? 'active' : '').'" value="list" name="display" type="submit"><i class="fa fa-th-list"></i></button>';	
		// Keep query string vars intact
		foreach ( $_GET as $key => $val ) {
			if ( 'display' === $key || 'submit' === $key ) {
				continue;
			}
			if ( is_array( $val ) ) {
				foreach( $val as $innerVal ) {
					echo '<input type="hidden" name="' . esc_attr( $key ) . '[]" value="' . esc_attr( $innerVal ) . '" />';
				}
			
			} else {
				echo '<input type="hidden" name="' . esc_attr( $key ) . '" value="' . esc_attr( $val ) . '" />';
			}
		}
	echo '</form>';

}

add_action( 'woocommerce_before_shop_loop', 'royanwine_fnc_woocommerce_display_modes' , 20 );


add_filter( 'yith_wcwl_button_label',          'royanwine_fnc_woocomerce_icon_wishlist'  );
    add_filter( 'yith-wcwl-browse-wishlist-label', 'royanwine_fnc_woocomerce_icon_wishlist_add' );


    function royanwine_fnc_woocomerce_icon_wishlist( $value='' ){
    	return '<i class="fa fa-heart-o"></i><span>' .esc_html__('Wishlist','royanwine').'</span>';
    }

    function royanwine_fnc_woocomerce_icon_wishlist_add(){
    	return '<i class="fa fa-heart-o"></i><span>' .esc_html__('Wishlist','royanwine').'</span>';
    }


/**
 * Processing hook layout content
 */
function royanwine_fnc_layout_main_class( $class ){
	$sidebar = royanwine_fnc_theme_options( 'woo-sidebar-show', 1 ) ;
	if( is_single() ){
		$sidebar = royanwine_fnc_theme_options('woo-single-sidebar-show'); ;
	}
	else {
		$sidebar = royanwine_fnc_theme_options('woo-sidebar-show'); 
	}

	if( $sidebar ){
		return $class;
	}

	return 'col-lg-12 col-md-12 col-xs-12';
}
add_filter( 'royanwine_woo_layout_main_class', 'royanwine_fnc_layout_main_class', 4 );


/**
 *
 */
function royanwine_fnc_woocommerce_archive_image(){ 
	if ( is_tax( array( 'product_cat', 'product_tag' ) ) && get_query_var( 'paged' ) == 0 ) { 
		$thumb =  get_woocommerce_term_meta( get_queried_object()->term_id, 'thumbnail_id', true ) ;

		if( $thumb ){
			$img = wp_get_attachment_image_src( $thumb, 'full' ); 
		
			echo '<p class="category-banner"><img src="'.esc_url_raw( $img[0] ).'""></p>'; 
		}
	}
}
add_action( 'woocommerce_archive_description', 'royanwine_fnc_woocommerce_archive_image', 9 );


/**
 * Show/Hide related, upsells products
 */
function royanwine_woocommerce_related_upsells_products($located, $template_name) {
	$options      = get_option('pbr_theme_options');
	$content_none = get_template_directory() . '/woocommerce/content-none.php';

	if ( 'single-product/related.php' == $template_name ) {
		if ( isset( $options['wc_show_related'] ) && 
			( 1 == $options['wc_show_related'] ) ) {
			$located = $content_none;
		}
	} elseif ( 'single-product/up-sells.php' == $template_name ) {
		if ( isset( $options['wc_show_upsells'] ) && 
			( 1 == $options['wc_show_upsells'] ) ) {
			$located = $content_none;
		}
	}

	return apply_filters( 'royanwine_woocommerce_related_upsells_products', $located, $template_name );
}

add_filter( 'wc_get_template', 'royanwine_woocommerce_related_upsells_products', 10, 2 );

/**
 * Number of products per page
 */
function royanwine_woocommerce_shop_per_page($number) {
	$value = royanwine_fnc_theme_options('woo-number-page', get_option('posts_per_page'));
	if ( is_numeric( $value ) && $value ) {
		$number = absint( $value );
	}
	return $number;
}

add_filter( 'loop_shop_per_page', 'royanwine_woocommerce_shop_per_page' );

/**
 * Number of products per row
 */
function royanwine_woocommerce_shop_columns($number) {
	$value = royanwine_fnc_theme_options('wc_itemsrow', 3);
	if ( in_array( $value, array(2, 3, 4, 6) ) ) {
		$number = $value;
	}
	return $number;
}

add_filter( 'loop_shop_columns', 'royanwine_woocommerce_shop_columns' );

function royanwine_fnc_woocommerce_share_box() {
	if ( royanwine_fnc_theme_options('wc_show_share_social', 1) ) {
		get_template_part( 'page-templates/parts/sharebox' );
	}
}
add_filter( 'woocommerce_single_product_summary', 'royanwine_fnc_woocommerce_share_box', 100 );

//remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );


function royanwine_fnc_product_features() {
	wc_get_template( 'single-product/tabs/features.php' );
}

function royanwine_fnc_woocommerce_products_tab($tabs){
	global $post;

	if( isset($tabs['additional_information']) ){
		unset( $tabs['additional_information'] ); 
	}
	$features = get_post_meta( $post->ID, 'royanwine_product_features', true);
	if ( !empty($features) ) {
		$tabs['features'] = array(
			'title'    => esc_html__( 'Features', 'royanwine' ),
			'priority' => 11,
			'callback' => 'royanwine_fnc_product_features'
		);
	}

	return $tabs;
}
add_filter('woocommerce_product_tabs', 'royanwine_fnc_woocommerce_products_tab', 80);

function royanwine_fnc_woocommerce_product_thumbnails_columns() {
	return royanwine_fnc_theme_options('woo-number-thumbnail-single', 4);

}
add_filter('woocommerce_product_thumbnails_columns', 'royanwine_fnc_woocommerce_product_thumbnails_columns');

function royanwine_fnc_woocommerce_products_year(){

    $year = get_post_meta( get_the_ID(), 'royanwine_product_year', true);

    if ( !empty($year) ) {
        echo '<p class="product-year">' . trim( $year ) . '</p>';
    }

}
add_filter('woocommerce_single_product_summary', 'royanwine_fnc_woocommerce_products_year', 4);


add_filter( 'pbrthemer_woocommerce_show_product_images', 'prefix_fnc_woocommerce_show_product_images' );
function prefix_fnc_woocommerce_show_product_images(){
    return 'product-image-h';
}

add_filter( 'pbrthemer_woocommerce_show_product_thumbnails', 'prefix_fnc_woocommerce_show_product_thumbnails' );
function prefix_fnc_woocommerce_show_product_thumbnails(){
    return 'product-thumbnails-h';
}

