<?php
if (class_exists( "WPBakeryShortCode" )) {
    /*
     *
     */

    class WPBakeryShortCode_PBR_Tabs_Products extends WPBakeryShortCode {

        public function getListQuery($atts) {
            $this->atts = $atts;
            $list_query = array();
            $types = explode( ',', $this->atts['show_tab'] );
            foreach ($types as $type) {
                $list_query[$type] = $this->getTabTitle( $type );
            }
            return $list_query;
        }

        public function getTabTitle($type) {
            switch ($type) {
                case 'recent':
                    return array( 'title' => esc_html__( 'Latest Products', 'royanwine' ), 'title_tab' => esc_html__( 'Latest', 'royanwine' ) );
                case 'featured_product':
                    return array( 'title' => esc_html__( 'Featured Products', 'royanwine' ), 'title_tab' => esc_html__( 'Featured', 'royanwine' ) );
                case 'top_rate':
                    return array( 'title' => esc_html__( 'Top Rated Products', 'royanwine' ), 'title_tab' => esc_html__( 'Top Rated', 'royanwine' ) );
                case 'best_selling':
                    return array( 'title' => esc_html__( 'BestSeller Products', 'royanwine' ), 'title_tab' => esc_html__( 'BestSeller', 'royanwine' ) );
                case 'on_sale':
                    return array( 'title' => esc_html__( 'Special Products', 'royanwine' ), 'title_tab' => esc_html__( 'Special', 'royanwine' ) );
            }
        }
    }

    class WPBakeryShortCode_Royanwine_Single_Product extends WPBakeryShortCode {
        /**
         * @var $product WC_Product
         */
        public $product;

        public function get_link_product_review() {
            $link = get_permalink( $this->product->get_id() );
            ?>
            <?php if (comments_open()) :
                echo '<a href="' . esc_url( $link ) . '#reviews" class="woocommerce-review-link" rel = "nofollow">' . esc_html__( 'Write a review', 'royanwine' ) . '</a>';
            endif;
        }

        public function get_review_count() {
            if (get_option( 'woocommerce_enable_review_rating' ) === 'no') {
                return;
            }
            $rating_count = $this->product->get_rating_count();
            $average = $this->product->get_average_rating();
            ?>
            <div class="woocommerce-product-rating">
                <div class="star-rating">
			        <span style="width:<?php echo( ( $average / 5 ) * 100 ); ?>%">
                        <?php
                        /* translators: 1: average rating 2: max rating (i.e. 5) */
                        printf(
                            __( '%1$s out of %2$s', 'royanwine' ),
                            '<strong class="rating">' . esc_html( $average ) . '</strong>',
                            '<span>5</span>'
                        );
                        ?>
                        <?php
                        /* translators: %s: rating count */
                        printf(
                            _n( 'based on %s customer rating', 'based on %s customer ratings', $rating_count, 'royanwine' ),
                            '<span class="rating">' . esc_html( $rating_count ) . '</span>'
                        );
                        ?>
			        </span>
                </div>
            </div>
            <?php
        }
    }

}