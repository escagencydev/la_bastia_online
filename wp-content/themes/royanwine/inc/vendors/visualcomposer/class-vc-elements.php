<?php 

class Royanwine_VC_Elements implements Vc_Vendor_Interface {

	public function __construct() {
        if (is_admin()){
            add_action( 'admin_footer', array( $this, 'add_inline_script' ) );
        }
    }

    public function add_inline_script() {
        ?>
        <script>
            function vcRoyanwineBannerInfoAddCallback($elem, action) {
                if ("new" !== action && "clone" !== action || $elem.find(".vc_control.column_toggle").click(), "new" === action) {
                }
            }
        </script>
        <?php
    }

	public function load(){ 
		
		/*********************************************************************************************************************
		 *  Featured Box
		 *********************************************************************************************************************/
		vc_map( array(
		    "name" => esc_html__("PBR Featured Box",'royanwine'),
		    "base" => "pbr_featuredbox",
		    "class" => "",
		    "category" => esc_html__('PBR Widgets', 'royanwine'),
		    'icon' => get_template_directory_uri() . "/images/opal.png",
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'royanwine'),
					"param_name" => "title",
					"value" => '',    "admin_label" => true,
				),
				array(
				    'type' => 'colorpicker',
				    'heading' => esc_html__( 'Title Color', 'royanwine' ),
				    'param_name' => 'title_color',
				    'description' => esc_html__( 'Select font color', 'royanwine' )
				),

		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Sub Title", 'royanwine'),
					"param_name" => "subtitle",
					"value" => '',
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Style", 'royanwine'),
					"param_name" => 'style',
					'value' 	=> array(
						esc_html__('Default', 'royanwine') => 'default',
						esc_html__('Version 1', 'royanwine') => 'v1',
						esc_html__('Version 2', 'royanwine') => 'v2',
						esc_html__('Version 3', 'royanwine' )=> 'v3',
						esc_html__('Version 4', 'royanwine') => 'v4',
						esc_html__('Version 5', 'royanwine' )=> 'v5',
						esc_html__('Version 6', 'royanwine' )=> 'v6',
						esc_html__('Version 7', 'royanwine' )=> 'v7',
						esc_html__('Version 8', 'royanwine' )=> 'v8',
						esc_html__('Version 9', 'royanwine' )=> 'v9',
						esc_html__('Version 10', 'royanwine' )=> 'v10'
					),
					'std' => ''
				),

				array(
					'type'                           => 'dropdown',
					'heading'                        => esc_html__( 'Title Alignment', 'royanwine' ),
					'param_name'                     => 'title_align',
					'value'                          => array(
					esc_html__( 'Align left', 'royanwine' )   => 'separator_align_left',
					esc_html__( 'Align center', 'royanwine' ) => 'separator_align_center',
					esc_html__( 'Align right', 'royanwine' )  => 'separator_align_right'
					),
					'std' => 'separator_align_left'
				),

			 	array(
					"type" => "textfield",
					"heading" => esc_html__("FontAwsome Icon", 'royanwine'),
					"param_name" => "icon",
					"value" => 'fa fa-gear',
					'description'	=> esc_html__( 'This support display icon from FontAwsome, Please click', 'royanwine' )
									. '<a href="' . ( is_ssl()  ? 'https' : 'http') . '://fortawesome.github.io/Font-Awesome/" target="_blank">'
									. esc_html__( 'here to see the list', 'royanwine' ) . '</a>'
				),
				array(
				    'type' => 'colorpicker',
				    'heading' => esc_html__( 'Icon Color', 'royanwine' ),
				    'param_name' => 'color',
				    'description' => esc_html__( 'Select font color', 'royanwine' )
				),	
				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Background Icon', 'royanwine' ),
					'param_name' => 'background',
					'value' => array(
						esc_html__( 'None', 'royanwine' ) => 'nostyle',
						esc_html__( 'Success', 'royanwine' ) => 'bg-success',
						esc_html__( 'Info', 'royanwine' ) => 'bg-info',
						esc_html__( 'Danger', 'royanwine' ) => 'bg-danger',
						esc_html__( 'Warning', 'royanwine' ) => 'bg-warning',
						esc_html__( 'Light', 'royanwine' ) => 'bg-default',
					),
					'std' => 'nostyle',
				),

                array(
                    "type" => "attach_image",
                    "heading" => esc_html__( "Photo", 'royanwine' ),
                    "param_name" => "photo",
                    "value" => '',
                    'description' => ''
                ),

				array(
					"type" => "textarea",
					"heading" => esc_html__("information", 'royanwine'),
					"param_name" => "information",
					"value" => 'Your Description Here',
					'description'	=> esc_html__('Allow  put html tags', 'royanwine' )
				),

				

				array(
				    'type' => 'colorpicker',
				    'heading' => esc_html__( 'Information Color', 'royanwine' ),
				    'param_name' => 'information_color',
				    'description' => esc_html__( 'Select font color', 'royanwine' )
				),

				array(
                    "type" => "attach_image",
                    "heading" => esc_html__( "Background Box", 'royanwine' ),
                    "param_name" => "bg_img",
                    "value" => '',
                    'description' => ''
                ),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'royanwine'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'royanwine')
				)
		   	)
		));

        /*********************************************************************************************************************
         *  Featured Box With SVG
         *********************************************************************************************************************/
        vc_map( array(
            "name" => esc_html__("PBR Feature Box (with svg)",'royanwine'),
            "base" => "pbr_featureboxsvg",
            "class" => "",
            "category" => esc_html__('PBR Widgets', 'royanwine'),
            'icon' => get_template_directory_uri() . "/images/opal.png",
            "params" => array(
                array(
                    "type" => "textfield",
                    "heading" => esc_html__("Title", 'royanwine'),
                    "param_name" => "title",
                    "value" => '',    "admin_label" => true,
                ),

                array(
                    "type" => "textarea",
                    "heading" => esc_html__("information", 'royanwine'),
                    "param_name" => "information",
                    "value" => 'Your Description Here',
                    'description'	=> esc_html__('Allow  put html tags', 'royanwine' )
                ),

                array(
                    'type'       => 'colorpicker',
                    'heading'    => esc_html__('Color', 'royanwine'),
                    'param_name' => 'color',
                ),
                array(
                    'type'       => 'dropdown',
                    'heading'    => esc_html__('Style', 'royanwine'),
                    'param_name' => 'style',
                    'value'      => array(
                        esc_html__('Wines Bottle', 'royanwine')    => 'bottle',
                        esc_html__('Wines Barrel', 'royanwine')     => 'barrel',
                        esc_html__('Grapes Bunch', 'royanwine')    => 'bunch',
                    ),
                ),
                array(
                    'type'       => 'textfield',
                    'heading'    => esc_html__('SVG Width', 'royanwine'),
                    'param_name' => 'svg_width',
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__('Custom height', 'royanwine'),
                    'param_name'  => 'custom_height',
                    'dependency'  => array(
                        'element' => 'style',
                        'value'   => array('curved-line', 'diagonal-right', 'half-circle', 'diagonal-left')
                    ),
                    'description' => esc_html__('Enter divider height (Note: CSS measurement units allowed).', 'royanwine')
                ),

                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__('Extra class name', 'royanwine'),
                    'param_name'  => 'el_class',
                    'description' => esc_html__('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'royanwine')
                ),
                array(
                    'type' => 'css_editor',
                    'heading' => esc_html__( 'CSS box', 'royanwine' ),
                    'param_name' => 'css',
                    'group' => esc_html__( 'Design Options', 'royanwine' ),
                ),
            )
        ));

        //PBR Map
        vc_map(array(
            "name"     => esc_html__("PBR Map Style", 'royanwine'),
            "base"     => "pbr_map_style",
            'icon'        => get_template_directory_uri() . "/images/opal.png",
            "category" => esc_html__('PBR Widgets', 'royanwine'),

            "params" => array(
                array(
                    "type"        => "textfield",
                    "heading"     => esc_html__("The latitude", 'royanwine'),
                    "param_name"  => "latitude",
                    "value"       => '41.515449',
                    'description' => esc_html__('The latitude to center the map. Ex: 41.515449', 'royanwine'),
                    "admin_label" => true
                ),
                array(
                    "type"        => "textfield",
                    "heading"     => esc_html__("The longitude", 'royanwine'),
                    "param_name"  => "longitude",
                    "value"       => '-96.951482',
                    'description' => esc_html__('The longitude to center the map. Ex: -96.951482', 'royanwine'),
                    "admin_label" => true
                ),
                array(
                    "type"       => "textfield",
                    "heading"    => esc_html__("Zoom", 'royanwine'),
                    "param_name" => "zoom",
                    "value"      => '12',
                ),
                array(
                    "type"        => "textfield",
                    "heading"     => esc_html__("width", 'royanwine'),
                    "param_name"  => "width",
                    "value"       => '100%',
                    'description' => esc_html__('Width of map. ex: 1000px, 500px,.. or 100%, 50%...', 'royanwine'),
                ),
                array(
                    "type"        => "textfield",
                    "heading"     => esc_html__("height", 'royanwine'),
                    "param_name"  => "height",
                    "value"       => '400px',
                    'description' => esc_html__('height of map. ex: 400px, 500px...', 'royanwine'),
                ),
            )
        ));
		 
	   	/*********************************************************************************************************************
		 * Pricing Table
		 *********************************************************************************************************************/
		vc_map( array(
		    "name" => esc_html__("PBR Pricing",'royanwine'),
		    "base" => "pbr_pricing",
		    "description" => esc_html__('Make Plan for membership', 'royanwine' ),
		    "class" => "",
		    "category" => esc_html__('PBR Widgets', 'royanwine'),
		    'icon' => get_template_directory_uri() . "/images/opal.png",
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'royanwine'),
					"param_name" => "title",
					"value" => '',
						"admin_label" => true
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Price", 'royanwine'),
					"param_name" => "price",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Currency", 'royanwine'),
					"param_name" => "currency",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Period", 'royanwine'),
					"param_name" => "period",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Subtitle", 'royanwine'),
					"param_name" => "subtitle",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Is Featured", 'royanwine'),
					"param_name" => "featured",
					'value' 	=> array(  esc_html__('No', 'royanwine') => 0,  esc_html__('Yes', 'royanwine') => 1 ),
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Skin", 'royanwine'),
					"param_name" => "skin",
					'value' 	=> array(  esc_html__('Skin 1', 'royanwine') => 'v1',  esc_html__('Skin 2', 'royanwine') => 'v2', esc_html__('Skin 3', 'royanwine') => 'v3' ),
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Box Style", 'royanwine'),
					"param_name" => "style",
					'value' 	=> array( 'boxed' => esc_html__('Boxed', 'royanwine')),
				),

				array(
					"type" => "textarea_html",
					"heading" => esc_html__("Content", 'royanwine'),
					"param_name" => "content",
					"value" => '',
					'description'	=> esc_html__('Allow  put html tags', 'royanwine')
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Link Title", 'royanwine'),
					"param_name" => "linktitle",
					"value" => 'SignUp',
					'description'	=> ''
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Link", 'royanwine'),
					"param_name" => "link",
					"value" => 'http://yourdomain.com',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'royanwine'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'royanwine')
				)
		   	)
		));
 		
 		/*********************************************************************************************************************
		 *  PBR Counter
		 *********************************************************************************************************************/
		vc_map( array(
		    "name" => esc_html__("PBR Counter",'royanwine'),
		    "base" => "pbr_counter",
		    "class" => "",
		    "description"=> esc_html__('Counting number with your term', 'royanwine'),
		    "category" => esc_html__('PBR Widgets', 'royanwine'),
		    'icon' => get_template_directory_uri() . "/images/opal.png",
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'royanwine'),
					"param_name" => "title",
					"value" => '',
					"admin_label"	=> true
				),
				array(
					"type" => "textarea",
					"heading" => esc_html__("Description", 'royanwine'),
					"param_name" => "description",
					"value" => '',
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Number", 'royanwine'),
					"param_name" => "number",
					"value" => ''
				),

			 	array(
					"type" => "textfield",
					"heading" => esc_html__("FontAwsome Icon", 'royanwine'),
					"param_name" => "icon",
					"value" => '',
					'description'	=> esc_html__( 'This support display icon from FontAwsome, Please click', 'royanwine' )
									. '<a href="' . ( is_ssl()  ? 'https' : 'http') . '://fortawesome.github.io/Font-Awesome/" target="_blank">'
									. esc_html__( 'here to see the list', 'royanwine' ) . '</a>'
				),


				array(
					"type" => "attach_image",
					"description" => esc_html__("If you upload an image, icon will not show.", 'royanwine'),
					"param_name" => "image",
					"value" => '',
					'heading'	=> esc_html__('Image', 'royanwine' )
				),

		 

				array(
					"type" => "colorpicker",
					"heading" => esc_html__("Text Color", 'royanwine'),
					"param_name" => "text_color",
					'value' 	=> '',
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'royanwine'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'royanwine')
				)
		   	)
		));
		
		//PBR Video popup
		vc_map( array(
		    "name" => esc_html__("PBR Videos Popup",'royanwine'),
		    "base" => "pbr_video_popup",
		    "class" => "",
		    "category" => esc_html__('PBR Widgets', 'royanwine'),
		    'icon' => get_template_directory_uri() . "/images/opal.png",
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'royanwine'),
					"param_name" => "title",
					"value" => '',
					"admin_label"	=> true
				),
				array(
					"type" => "textarea",
					"heading" => esc_html__("Description", 'royanwine'),
					"param_name" => "description",
					"value" => '',
				),
				array(
					"type" => "textarea",
					"heading" => esc_html__("Sub Title", 'royanwine'),
					"param_name" => "subtitle",
					"value" => '',
				),
				array(
					"type" => "textfield",
					"description" => esc_html__("Add link video( support youtube and vimeo", 'royanwine'),
					"param_name" => "video_link",
					"std" => 'https://www.youtube.com/watch?v=dPL1-8ypnEs',
					'heading'	=> esc_html__('Link video', 'royanwine' )
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Style", 'royanwine'),
					"param_name" => 'style',
					'value' 	=> array(
						esc_html__('Default', 'royanwine') => 'default',
						esc_html__('Version 2', 'royanwine' )=> 'version2'
					),
					'std' => ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'royanwine'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'royanwine')
				)
	    	)
		));

        /*********************************************************************************************************************
         *  PBR Divider
         *********************************************************************************************************************/
        vc_map( array(
            "name" => esc_html__("PBR Divider",'royanwine'),
            "base" => "pbr_divider",
            "class" => "",
            "description"=> esc_html__('Divider for sections', 'royanwine'),
            "category" => esc_html__('PBR Widgets', 'royanwine'),
            'icon' => get_template_directory_uri() . "/images/opal.png",
            "params"      => array(
                array(
                    'type'       => 'dropdown',
                    'heading'    => esc_html__('Position', 'royanwine'),
                    'param_name' => 'position',
                    'value'      => array(
                        esc_html__('Top', 'royanwine')    => 'top',
                        esc_html__('Bottom', 'royanwine') => 'bottom',
                    ),
                ),
                array(
                    'type'       => 'colorpicker',
                    'heading'    => esc_html__('Color', 'royanwine'),
                    'param_name' => 'color',
                ),
                array(
                    'type'       => 'dropdown',
                    'heading'    => esc_html__('Style', 'royanwine'),
                    'param_name' => 'style',
                    'value'      => array(
                        esc_html__('Waves Small', 'royanwine')    => 'waves-small',
                        esc_html__('Waves Wide', 'royanwine')     => 'waves-wide',
                        esc_html__('Curved Line', 'royanwine')    => 'curved-line',
                        esc_html__('Triangle', 'royanwine')       => 'triangle',
                        esc_html__('Clouds', 'royanwine')         => 'clouds',
                        esc_html__('Diagonal Right', 'royanwine') => 'diagonal-right',
                        esc_html__('Diagonal Left', 'royanwine')  => 'diagonal-left',
                        esc_html__('Half Circle', 'royanwine')    => 'half-circle',
                        esc_html__('Paint Stroke', 'royanwine')   => 'paint-stroke',
                        esc_html__('Grime', 'royanwine')          => 'grime',
                    ),
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__('Custom height', 'royanwine'),
                    'param_name'  => 'custom_height',
                    'dependency'  => array(
                        'element' => 'style',
                        'value'   => array('curved-line', 'diagonal-right', 'half-circle', 'diagonal-left')
                    ),
                    'description' => esc_html__('Enter divider height (Note: CSS measurement units allowed).', 'royanwine')
                ),

                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__('Extra class name', 'royanwine'),
                    'param_name'  => 'el_class',
                    'description' => esc_html__('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'royanwine')
                ),
                array(
                    'type' => 'css_editor',
                    'heading' => esc_html__( 'CSS box', 'royanwine' ),
                    'param_name' => 'css',
                    'group' => esc_html__( 'Design Options', 'royanwine' ),
                ),
            ),
        ));

        vc_map( array(
            'name' => esc_html__( '(News) FrontPage 4', 'royanwine' ),
            'base' => 'pbr_listposts',
            'icon' => 'icon-wpb-news-4',
            "category" => esc_html__('PBR News', 'royanwine'),
            'description' => esc_html__( 'Create Post having blog styles', 'royanwine' ),

            'params' => array(
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Widget title', 'royanwine' ),
                    'param_name' => 'title',
                    'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'royanwine' ),
                    "admin_label" => true
                ),




                array(
                    'type' => 'loop',
                    'heading' => esc_html__( 'Grids content', 'royanwine' ),
                    'param_name' => 'loop',
                    'settings' => array(
                        'size' => array( 'hidden' => false, 'value' => 4 ),
                        'order_by' => array( 'value' => 'date' ),
                    ),
                    'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'royanwine' )
                ),

                array(
                    'type' => 'checkbox',
                    'heading' => esc_html__( 'Show Pagination Links', 'royanwine' ),
                    'param_name' => 'show_pagination',
                    'description' => esc_html__( 'Enables to show paginations to next new page.', 'royanwine' ),
                    'value' => array( esc_html__( 'Yes, please', 'royanwine' ) => 'yes' )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Thumbnail size', 'royanwine' ),
                    'param_name' => 'thumbsize',
                    'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'royanwine' )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Extra class name', 'royanwine' ),
                    'param_name' => 'el_class',
                    'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'royanwine' )
                )
            )
        ) );




        $layout_image = array(
            esc_html__('Grid', 'royanwine')             => 'grid-1',
            esc_html__('List', 'royanwine')             => 'list-1',
            esc_html__('List not image', 'royanwine')   => 'list-2',
        );

        vc_map( array(
            'name' => esc_html__( '(News) Grid Posts', 'royanwine' ),
            'base' => 'pbr_gridposts',
            'icon' => 'icon-wpb-news-2',
            "category" => esc_html__('PBR News', 'royanwine'),
            'description' => esc_html__( 'Post having news,managzine style', 'royanwine' ),

            'params' => array(
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Widget title', 'royanwine' ),
                    'param_name' => 'title',
                    'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'royanwine' ),
                    "admin_label" => true
                ),


                array(
                    'type' => 'loop',
                    'heading' => esc_html__( 'Grids content', 'royanwine' ),
                    'param_name' => 'loop',
                    'settings' => array(
                        'size' => array( 'hidden' => false, 'value' => 4 ),
                        'order_by' => array( 'value' => 'date' ),
                    ),
                    'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'royanwine' )
                ),
                array(
                    "type" => "dropdown",
                    "heading" => esc_html__("Layout Type", 'royanwine'),
                    "param_name" => "layout",
                    "layout_images" => $layout_image,
                    "value" => $layout_image,
                    "admin_label" => true,
                    "description" => esc_html__("Select Skin layout.", 'royanwine')
                ),

                array(
                    "type" => "dropdown",
                    "heading" => esc_html__("Style", 'royanwine'),
                    "param_name" => "style",
                    "value" => array(
                    	'Default' => 'default',
                    	'Overflow' => 'overflow',
                    	'No Image' => 'noimage'
                    ),
                    "admin_label" => true,
                    "description" => esc_html__("Select Skin layout.", 'royanwine')
                ),

                array(
                    'type' => 'checkbox',
                    'heading' => esc_html__( 'Show Pagination Links', 'royanwine' ),
                    'param_name' => 'show_pagination',
                    'description' => esc_html__( 'Enables to show paginations to next new page.', 'royanwine' ),
                    'value' => array( esc_html__( 'Yes, please', 'royanwine' ) => 'yes' )
                ),

                array(
                    "type" => "dropdown",
                    "heading" => esc_html__("Grid Columns", 'royanwine'),
                    "param_name" => "grid_columns",
                    "value" => array( 1 , 2 , 3 , 4 , 6),
                    "std" => 3
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Thumbnail size', 'royanwine' ),
                    'param_name' => 'thumbsize',
                    'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'royanwine' )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Extra class name', 'royanwine' ),
                    'param_name' => 'el_class',
                    'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'royanwine' )
                )
            )
        ) );

        /*********************************************************************************************************************
         *  Vertical menu
         *********************************************************************************************************************/
        $option_menu = array();
        if (is_admin()){
            $menus       = wp_get_nav_menus( array( 'orderby' => 'name' ) );
            $option_menu = array( '---Select Menu---' => '' );
            foreach ($menus as $menu) {
                $option_menu[$menu->name] = $menu->term_id;
            }
        }
        /*********************************************************************************************************************
         *  Vertical menu
         *********************************************************************************************************************/
        vc_map( array(
            "name"     => esc_html__( "PBR Vertical MegaMenu", 'royanwine' ),
            "base"     => "pbr_verticalmenu",
            "class"    => "",
            'icon'     => get_template_directory_uri() . "/images/opal.png",
            "category" => esc_html__( 'PBR Widgets', 'royanwine' ),
            "params"   => array(

                array(
                    "type"        => "textfield",
                    "heading"     => esc_html__( "Title", 'royanwine' ),
                    "param_name"  => "title",
                    "value"       => 'Vertical Menu',
                    "admin_label" => true,
                ),

                array(
                    "type"        => "dropdown",
                    "heading"     => esc_html__( "Menu", 'royanwine' ),
                    "param_name"  => "menu",
                    "value"       => $option_menu,
                    "description" => esc_html__( "Select menu.", 'royanwine' ),
                ),
                array(
                    "type"        => "dropdown",
                    "heading"     => esc_html__( "Position", 'royanwine' ),
                    "param_name"  => "postion",
                    "value"       => array(
                        'left'  => 'left',
                        'right' => 'right',
                    ),
                    'std'         => 'left',
                    "description" => esc_html__( "Postion Menu Vertical.", 'royanwine' ),
                ),
                array(
                    "type"        => "textfield",
                    "heading"     => esc_html__( "Extra class name", 'royanwine' ),
                    "param_name"  => "el_class",
                    "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'royanwine' ),
                ),
            ),
        ) );

        /******************************
         * Our Team
         ******************************/
        vc_map( array(
            "name"     => esc_html__( "PBR Our Team", 'royanwine' ),
            "base"     => "pbr_team_list",
            "class"    => "",
            'icon'     => get_template_directory_uri() . "/images/opal.png",
            "category" => esc_html__( 'PBR Widgets', 'royanwine' ),
            "params"   => array(
                array(
                    "type"        => "textfield",
                    "heading"     => esc_html__( "Title", 'royanwine' ),
                    "param_name"  => "title",
                    "value"       => '',
                    "admin_label" => true,
                ),
                array(
                    "type"        => "attach_image",
                    "heading"     => esc_html__( "Photo", 'royanwine' ),
                    "param_name"  => "photo",
                    "value"       => '',
                    'description' => '',
                ),
                array(
                    "type"        => "textfield",
                    "heading"     => esc_html__( "Job", 'royanwine' ),
                    "param_name"  => "job",
                    "value"       => 'CEO',
                    'description' => '',
                ),
                array(
					"type" => "textfield",
					"heading" => esc_html__("Facebook", 'royanwine'),
					"param_name" => "facebook",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Twitter", 'royanwine'),
					"param_name" => "twitter",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Linked In", 'royanwine'),
					"param_name" => "linkedin",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Style", 'royanwine'),
					"param_name" => 'style',
					'value' 	=> array(
						esc_html__('Default', 'royanwine') => 'default',
						esc_html__('Version 2', 'royanwine') => 'version2'
					),
					'std' => ''
				),
				 array(
                    "type"        => "textfield",
                    "heading"     => esc_html__( "Extra class name", 'royanwine' ),
                    "param_name"  => "el_class",
                    "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'royanwine' ),
                ),
            ),
        ) );


        /* Heading Text Block
        ---------------------------------------------------------- */
        vc_map( array(
            'name'        => esc_html__( 'PBR Widget Heading', 'royanwine' ),
            'base'        => 'pbr_title_heading',
            "class"       => "",
            'icon'        => get_template_directory_uri() . "/images/opal.png",
            "category"    => esc_html__( 'PBR Widgets', 'royanwine' ),
            'description' => esc_html__( 'Create title for one Widget', 'royanwine' ),
            "params"      => array(
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Widget title', 'royanwine' ),
                    'param_name'  => 'title',
                    'value'       => esc_html__( 'Title', 'royanwine' ),
                    'description' => esc_html__( 'Enter heading title.', 'royanwine' ),
                    "admin_label" => true,
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Title Color', 'royanwine' ),
                    'param_name'  => 'font_color',
                    'description' => esc_html__( 'Select font color', 'royanwine' ),
                ),

                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Sub title', 'royanwine' ),
                    'param_name'  => 'subtitle',
                    "value"       => '',
                    'description' => esc_html__( 'Enter heading sub title.', 'royanwine' ),
                    "admin_label" => true,
                ),

                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Sub Title Color', 'royanwine' ),
                    'param_name'  => 'subtitle_color',
                    'description' => esc_html__( 'Select color for subtitle', 'royanwine' ),
                ),

                array(
                    "type"        => "textarea",
                    'heading'     => esc_html__( 'Description', 'royanwine' ),
                    "param_name"  => "descript",
                    "value"       => '',
                    'description' => esc_html__( 'Enter description for title.', 'royanwine' ),
                ),

                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Descript Color', 'royanwine' ),
                    'param_name'  => 'descript_color',
                    'description' => esc_html__( 'Select color for descript', 'royanwine' ),
                ),

                array(
                    "type"       => "dropdown",
                    "heading"    => esc_html__( "Style", 'royanwine' ),
                    "param_name" => "style",
                    'value'      => array( 'Default' => esc_html__( 'heading-default', 'royanwine' ), 'Style1' => esc_html__( 'heading-style1', 'royanwine' ), 'Style2' => esc_html__( 'heading-style2', 'royanwine' ), 'Style3' => esc_html__( 'heading-style3', 'royanwine' ), 'Style4' => esc_html__( 'heading-style4', 'royanwine' ), 'Style5' => esc_html__( 'heading-style5', 'royanwine' ) ),
                ),
                array(
                    'type'       => 'dropdown',
                    'heading'    => esc_html__( 'Title Alignment', 'royanwine' ),
                    'param_name' => 'alignment',
                    'value'      => array(
                        esc_html__( 'Align left', 'royanwine' )   => 'text-left',
                        esc_html__( 'Align center', 'royanwine' ) => 'text-center',
                        esc_html__( 'Align right', 'royanwine' )  => 'text-right',
                    ),
                    'std'        => 'text-center',
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Extra class name', 'royanwine' ),
                    'param_name'  => 'el_class',
                    'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'royanwine' ),
                ),
            ),
        ) );

        /* PBR Banner CountDown
        ---------------------------------------------------------- */
        vc_map( array(
            'name'        => esc_html__( 'PBR Banner CountDown', 'royanwine' ),
            'base'        => 'pbr_banner_countdown',
            "class"       => "",
            'icon'        => get_template_directory_uri() . "/images/opal.png",
            "category"    => esc_html__( 'PBR Widgets', 'royanwine' ),
            'description' => esc_html__( 'Show CountDown with banner', 'royanwine' ),
            "params"      => array(
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Widget title', 'royanwine' ),
                    'param_name'  => 'title',
                    'value'       => esc_html__( 'Title', 'royanwine' ),
                    'description' => esc_html__( 'Enter heading title.', 'royanwine' ),
                    "admin_label" => true,
                ),


                array(
                    "type"        => "attach_image",
                    "description" => esc_html__( "If you upload an image, icon will not show.", 'royanwine' ),
                    "param_name"  => "image",
                    "value"       => '',
                    'heading'     => esc_html__( 'Image', 'royanwine' ),
                ),


                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Date Expired', 'royanwine' ),
                    'param_name'  => 'input_datetime',
                    'description' => esc_html__( 'Select font color', 'royanwine' ),
                ),


                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Title Color', 'royanwine' ),
                    'param_name'  => 'font_color',
                    'description' => esc_html__( 'Select font color', 'royanwine' ),
                    'class'       => 'hacongtien',
                ),

                array(
                    "type"        => "textarea",
                    'heading'     => esc_html__( 'Description', 'royanwine' ),
                    "param_name"  => "descript",
                    "value"       => '',
                    'description' => esc_html__( 'Enter description for title.', 'royanwine' ),
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Extra class name', 'royanwine' ),
                    'param_name'  => 'el_class',
                    'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'royanwine' ),
                ),


                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Text Link', 'royanwine' ),
                    'param_name'  => 'text_link',
                    'value'       => 'Find Out More',
                    'description' => esc_html__( 'Enter your link text', 'royanwine' ),
                ),


                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Link', 'royanwine' ),
                    'param_name'  => 'link',
                    'value'       => 'http://',
                    'description' => esc_html__( 'Enter your link to redirect', 'royanwine' ),
                ),
            ),
        ) );


        /* PBR Banner effect
        ---------------------------------------------------------- */
        vc_map( array(
            'name'        => esc_html__( 'PBR Banner Effect', 'royanwine' ),
            'base'        => 'pbr_banner_effect',
            "class"       => "",
            'icon'        => get_template_directory_uri() . "/images/opal.png",
            "category"    => esc_html__( 'PBR Widgets', 'royanwine' ),
            'description' => esc_html__( 'Show info with banner', 'royanwine' ),
            "params" => array(
                array(
                    "type" => "textfield",
                    "heading" => esc_html__("Title", 'royanwine'),
                    "param_name" => "title",
                    "value" => '',    "admin_label" => true,
                ),
                array(
                    "type" => "dropdown",
                    "heading" => esc_html__("Skin", 'royanwine'),
                    "param_name" => "skin",
                    'value' 	=> array(
                        esc_html__('Dark', 'royanwine') => 'default',
                        esc_html__('Light', 'royanwine') => 'light'
                    ),
                    'std' => 'default'
                ),

                array(
                    'type'                           => 'dropdown',
                    'heading'                        => esc_html__( 'Title Alignment', 'royanwine' ),
                    'param_name'                     => 'title_align',
                    'value'                          => array(
                        esc_html__( 'Align left', 'royanwine' )   => 'separator_align_left',
                        esc_html__( 'Align center', 'royanwine' ) => 'separator_align_center',
                        esc_html__( 'Align right', 'royanwine' )  => 'separator_align_right'
                    ),
                    'std' => 'separator_align_left'
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => esc_html__( 'Background hover', 'royanwine' ),
                    'param_name' => 'background',
                    'value' => array(
                        esc_html__( 'None', 'royanwine' ) => 'nostyle',
                        esc_html__( 'Success', 'royanwine' ) => 'bg-success',
                        esc_html__( 'Info', 'royanwine' ) => 'bg-info',
                        esc_html__( 'Danger', 'royanwine' ) => 'bg-danger',
                        esc_html__( 'Warning', 'royanwine' ) => 'bg-warning',
                        esc_html__( 'Secondary', 'royanwine' ) => 'bg-secondary',
                    ),
                    'std' => 'nostyle',
                ),

                array(
                    "type" => "attach_image",
                    "heading" => esc_html__("Photo", 'royanwine'),
                    "param_name" => "photo",
                    "value" => '',
                    'description'	=> ''
                ),

                array(
                    "type" => "dropdown",
                    "heading" => esc_html__("Reverse Image", 'royanwine'),
                    "param_name" => "style",
                    'value' 	=> array(
                        esc_html__('No', 'royanwine') => '',
                        esc_html__('Yes', 'royanwine') => 'banner-reverse'
                    ),
                    'std' => ''
                ),

                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Button Text', 'royanwine' ),
                    'param_name' => 'button_text',
                    'description' => esc_html__( 'Enter button text.', 'royanwine' ),
                    "admin_label" => true
                ),

                array(
                    'type' => 'vc_link',
                    'heading' => esc_html__( 'URL (Link)', 'royanwine' ),
                    'param_name' => 'link',
                    'description' => esc_html__( 'Add link to description.', 'royanwine' ),
                    // compatible with btn2 and converted from href{btn1}
                ),

                array(
                    "type" => "textfield",
                    "heading" => esc_html__("Extra class name", 'royanwine'),
                    "param_name" => "el_class",
                    "description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'royanwine')
                )
            )
        ) );
    }
}