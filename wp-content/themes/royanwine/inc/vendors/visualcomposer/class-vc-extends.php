<?php  

if ( file_exists( vc_path_dir('SHORTCODES_DIR', 'vc-posts-grid.php') ) ) {
	require_once vc_path_dir('SHORTCODES_DIR', 'vc-posts-grid.php');
} else {
	//Backup for VC v6.0.2
	require_once(  get_template_directory() . '/inc/vendors/visualcomposer/vc-posts-grid.php' );
}
class WPBakeryShortCode_PBR_Counter extends WPBakeryShortCode {

	public function __construct( $settings ) {
		parent::__construct( $settings );
		$this->jsCssScripts();
	}

	public function jsCssScripts() {

		wp_register_style('counterup_js',get_stylesheet_directory_uri().'/js/jquery.counterup.min.js',array(),false,true);
	 
	}
}

class WPBakeryShortCode_Royanwine_Theme extends WPBakeryShortCode {
}


/**
 * Elements
 */
class WPBakeryShortCode_Pbr_featuredbox  extends WPBakeryShortCode {}
class WPBakeryShortCode_Pbr_featureboxsvg  extends WPBakeryShortCode {}
class WPBakeryShortCode_Pbr_pricing 	 extends WPBakeryShortCode {}
class WPBakeryShortCode_Pbr_inforbox 	 extends WPBakeryShortCode {}
class WPBakeryShortCode_Pbr_title_heading   extends WPBakeryShortCode {}
class WPBakeryShortCode_Pbr_team_list extends WPBakeryShortCode {}
class WPBakeryShortCode_Pbr_verticalmenu extends WPBakeryShortCode {}
class WPBakeryShortCode_Pbr_banner_countdown extends WPBakeryShortCode {}
class WPBakeryShortCode_Pbr_banner_effect extends WPBakeryShortCode {}
class WPBakeryShortCode_Pbr_Video_Popup extends WPBakeryShortCode {}
class WPBakeryShortCode_Pbr_Divider extends WPBakeryShortCode {}
class WPBakeryShortCode_PBR_Listposts extends WPBakeryShortCode_VC_Posts_Grid {}
class WPBakeryShortCode_PBR_Gridposts extends WPBakeryShortCode_VC_Posts_Grid {}
class WPBakeryShortCode_Pbr_Map_Style extends WPBakeryShortCode {}