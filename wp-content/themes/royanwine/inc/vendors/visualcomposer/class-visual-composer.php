<?php

class Royanwine_Visual_Composer {
    public function __construct() {
        add_filter(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, array($this, 'add_classes_in_element'), 10, 3);
        add_action('vc_after_init', array($this, 'add_field_to_row_and_column'));
        add_action('vc_after_init', array($this, 'add_field_to_vc_btn'), 9);
        add_action('vc_after_init', array($this, 'add_field_to_tabs'));

        // Override Grid Template
        add_filter('vc_gitem_template_attribute_vc_btn', array($this, 'gitem_template_attribute_vc_btn'), 11, 2);
        add_filter('vc-tta-get-params-tabs-list', array($this, 'custom_markup_tabs'), 10, 4);
    }

    /**
     * @param $classes   string
     * @param $shortcode string
     * @param $atts      array
     * @return string
     */
    public function add_classes_in_element($classes, $shortcode, $atts) {
        if ($shortcode === 'vc_row'
            || $shortcode === 'vc_row_inner'
            || $shortcode === 'vc_section'
            || $shortcode === 'vc_column'
            || $shortcode === 'vc_column_inner'
            || $shortcode === 'vc_column_text'
        ) {
            if (!empty($atts['bg_position'])) {
                $classes .= ' pbr-bg-' . $atts['bg_position'];
            }
        } elseif ($shortcode === 'vc_btn') {
            if (in_array($atts['style'], array(
                'btn-primary',
                'btn-outline-primary',
                'btn-secondary',
                'btn-outline-secondary',
            ))) {
                $classes = str_replace('vc_general', $atts['style'], $classes);
                $classes = str_replace('vc_btn3-color-' . $atts['color'], '', $classes);
            }
        }
        return $classes;
    }

    public function add_field_to_tabs() {
        $params = vc_get_shortcode('vc_tta_tabs')['params'];
        foreach ($params as $key => $param) {
            if ($param['param_name'] === 'style') {
                $params[$key]['value']['Opal style'] = 'opal-style';
                $params[$key]['value']['Opal style v2'] = 'opal-style_v2';
                $params[$key]['value']['Opal style v3'] = 'opal-style_v3';
                $params[$key]['value']['Opal style v4'] = 'opal-style_v4';
                break;
            }
        }
        $args = array(
            array(
                'type'       => 'textfield',
                'heading'    => esc_html__('Heading', 'royanwine'),
                'param_name' => 'tabs_heading',
                'group'      => esc_html__('Opal Extras', 'royanwine'),
            ),
            array(
                'type'        => 'checkbox',
                'heading'     => esc_html__('Enable Underline Wrap', 'royanwine'),
                'group'       => esc_html__('Opal Extras', 'royanwine'),
                'param_name'  => 'enable_underline_wrap',
                'value'       => array(__('Yes', 'royanwine') => 'yes'),
                'save_always' => true,
            ),
            array(
                'type'       => 'dropdown',
                'heading'    => esc_html__('Underline Title', 'royanwine'),
                'param_name' => 'underline_title',
                'group'      => esc_html__('Opal Extras', 'royanwine'),
                'value'      => array(
                    esc_html__('None', 'royanwine')            => 'none',
                    esc_html__('Primary Color', 'royanwine')   => 'primary',
                    esc_html__('Secondary Color', 'royanwine') => 'secondary',
                ),
                'std'        => 'none'
            )
        );

        vc_add_params('vc_tta_tabs', $params);
        vc_add_params('vc_tta_tabs', $args);
    }

    public function add_field_to_row_and_column() {
        $args = array(
            array(
                'type'       => 'dropdown',
                'heading'    => esc_html__('Background position', 'royanwine'),
                'param_name' => 'bg_position',
                'group'      => esc_html__('Extras', 'royanwine'),
                'value'      => array(
                    esc_html__('None', 'royanwine')          => '',
                    esc_html__('Left top', 'royanwine')      => 'left-top',
                    esc_html__('Left center', 'royanwine')   => 'left-center',
                    esc_html__('Left bottom', 'royanwine')   => 'left-bottom',
                    esc_html__('Right top', 'royanwine')     => 'right-top',
                    esc_html__('Right center', 'royanwine')  => 'right-center',
                    esc_html__('Right bottom', 'royanwine')  => 'right-bottom',
                    esc_html__('Center top', 'royanwine')    => 'center-top',
                    esc_html__('Center center', 'royanwine') => 'center-center',
                    esc_html__('Center bottom', 'royanwine') => 'center-bottom',
                ),
            )
        );
        vc_add_params('vc_row', $args);
        vc_add_params('vc_row_inner', $args);
        vc_add_params('vc_section', $args);
        vc_add_params('vc_column', $args);
        vc_add_params('vc_column_inner', $args);
        vc_add_params('vc_column_text', $args);
    }

    public function gitem_template_attribute_vc_btn($value, $data) {
        /**
         * @var Wp_Post $post
         * @var string  $data
         */
        extract(array_merge(array(
            'post' => null,
            'data' => '',
        ), $data));

        return require include(get_template_directory()) . 'inc/verdors/visualcompomser/grid/vc_btn.php';
    }

    public function add_field_to_vc_btn() {
        $vc_config_path = vc_path_dir('CONFIG_DIR');
        $params = include $vc_config_path . '/buttons/shortcode-vc-btn.php';
        $params = $params['params'];

        foreach ($params as $key => $param) {
            if ($param['param_name'] === 'style') {
                $params[$key]['value']['Primary'] = 'btn-primary';
                $params[$key]['value']['Primary Outline'] = 'btn-outline-primary';
                $params[$key]['value']['Secondary'] = 'btn-secondary';
                $params[$key]['value']['Secondary Outline'] = 'btn-outline-secondary';
                continue;
            }

            if ($param['param_name'] === 'color') {
                $params[$key]['dependency']['value_not_equal_to'][] = 'btn-primary';
                $params[$key]['dependency']['value_not_equal_to'][] = 'btn-secondary';
                $params[$key]['dependency']['value_not_equal_to'][] = 'btn-outline-primary';
                $params[$key]['dependency']['value_not_equal_to'][] = 'btn-outline-secondary';
                continue;
            }
        }

        vc_add_params('vc_btn', $params);
    }

    public function custom_markup_tabs($html, $atts, $content, $obj) {
        $custom = array();
        foreach ($html as $key => $value) {
            $custom[] = $value;
            if (!empty($atts['tabs_heading']) && $key == 0) {
                $heading_classes = '';
                if (!empty($atts['enable_underline_wrap'])) {
                    $heading_classes .= ' underline-wrap';
                }
                if (!empty($atts['underline_title'])) {
                    $heading_classes .= ' underline-title-' . esc_attr($atts['underline_title']);
                }
                $custom[] = '<div class="otf_custom_tabs"><div class="vc_custom_heading' . esc_attr($heading_classes) . '"><h4>' . esc_html($atts["tabs_heading"]) . '</h4></div>';
            }
        }
        if (!empty($atts['tabs_heading'])) {
            $custom[] = '</div>';
        }
        return $custom;
    }
}

new Royanwine_Visual_Composer();
