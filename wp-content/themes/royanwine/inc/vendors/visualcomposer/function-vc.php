<?php

 /**
  * Register Woocommerce Vendor which will register list of shortcodes
  */
function royanwine_fnc_init_vc_vendors(){
	$vendor = new Royanwine_VC_Elements();
	add_action( 'vc_after_set_mode', array(
		$vendor,
		'load'
	), 99 );

}
add_action( 'after_setup_theme', 'royanwine_fnc_init_vc_vendors' , 99 );   

/**
 * Add parameters for row
 */
function royanwine_fnc_add_params(){
	vc_remove_param( 'pbr_testimonials', 'skin' );

	vc_add_param( 'pbr_testimonials', array(
	      "type" => "dropdown",
	      "heading" => esc_html__("Layout", "royanwine"),
	      "param_name" => "layout",
	      "value" => array(
	      	esc_html__('Version Layout boxed', 'royanwine')=>'left',
	      	esc_html__('Version Layout fullwidth', 'royanwine')=>'v2',
	      	esc_html__('Version 3', 'royanwine')=>'v3'
	      	),
	      "admin_label" => true,
	      "description" => esc_html__("Select Layout.", "royanwine"),
    ));

	vc_add_param( 'pbr_testimonials', array(
	      "type" => "dropdown",
	      "heading" => esc_html__("Skin", "royanwine"),
	      "param_name" => "skin",
	      "value" => array(
	      	esc_html__('Light', 'royanwine')=>'light',
	      	esc_html__('Dark', 'royanwine')=>'dark', 
	      	esc_html__('V3', 'royanwine')=>'v3', 
	      	),
	      "admin_label" => true,
	      "description" => esc_html__("Select Skin.", "royanwine")
    ));
	

 	/**
	 * add new params for row
	 */
	vc_add_param( 'vc_row', array(
	    "type" => "checkbox",
	    "heading" => esc_html__("Parallax", 'royanwine'),
	    "param_name" => "parallax",
	    "value" => array(
	        esc_html__('Yes, please', 'royanwine') => true
	    )
	));
 

	 vc_add_param( 'vc_row', array(
	     "type" => "dropdown",
	     "heading" => esc_html__("Is Boxed", 'royanwine'),
	     "param_name" => "isfullwidth",
	     "value" => array(
	     				esc_html__('Yes, Boxed', 'royanwine') => '1',
	     				esc_html__('No, Wide', 'royanwine') => '0'
	     			)
	));

	
}
add_action( 'after_setup_theme', 'royanwine_fnc_add_params', 99 );
