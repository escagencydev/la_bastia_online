<?php

class Royanwine_VC_Theme implements Vc_Vendor_Interface {

    public function __construct() {
        if (is_admin()){
            add_action( 'admin_footer', array( $this, 'add_inline_script' ) );
        }
    }

    public function add_inline_script() {
        ?>
        <script>
            function vcRoyanwineBannerInfoAddCallback($elem, action) {
                if ("new" !== action && "clone" !== action || $elem.find(".vc_control.column_toggle").click(), "new" === action) {
                }
            }
        </script>
        <?php
    }

    public function load() {
        /*********************************************************************************************************************
         *  Vertical menu
         *********************************************************************************************************************/
        $option_menu = array();
        if (is_admin()){
            $menus       = wp_get_nav_menus( array( 'orderby' => 'name' ) );
            $option_menu = array( '---Select Menu---' => '' );
            foreach ($menus as $menu) {
                $option_menu[$menu->name] = $menu->term_id;
            }
        }
        /*********************************************************************************************************************
         *  Vertical menu
         *********************************************************************************************************************/
        vc_map( array(
            "name"     => esc_html__( "PBR Vertical MegaMenu", 'royanwine' ),
            "base"     => "pbr_verticalmenu",
            "class"    => "",
            'icon'     => get_template_directory_uri() . "/images/opal.png",
            "category" => esc_html__( 'PBR Widgets', 'royanwine' ),
            "params"   => array(

                array(
                    "type"        => "textfield",
                    "heading"     => esc_html__( "Title", 'royanwine' ),
                    "param_name"  => "title",
                    "value"       => 'Vertical Menu',
                    "admin_label" => true,
                ),

                array(
                    "type"        => "dropdown",
                    "heading"     => esc_html__( "Menu", 'royanwine' ),
                    "param_name"  => "menu",
                    "value"       => $option_menu,
                    "description" => esc_html__( "Select menu.", 'royanwine' ),
                ),
                array(
                    "type"        => "dropdown",
                    "heading"     => esc_html__( "Position", 'royanwine' ),
                    "param_name"  => "postion",
                    "value"       => array(
                        'left'  => 'left',
                        'right' => 'right',
                    ),
                    'std'         => 'left',
                    "description" => esc_html__( "Postion Menu Vertical.", 'royanwine' ),
                ),
                array(
                    "type"        => "textfield",
                    "heading"     => esc_html__( "Extra class name", 'royanwine' ),
                    "param_name"  => "el_class",
                    "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'royanwine' ),
                ),
            ),
        ) );

        /******************************
         * Our Team
         ******************************/
        vc_map( array(
            "name"     => esc_html__( "PBR Our Team", 'royanwine' ),
            "base"     => "pbr_team_list",
            "class"    => "",
            'icon'     => get_template_directory_uri() . "/images/opal.png",
            "category" => esc_html__( 'PBR Widgets', 'royanwine' ),
            "params"   => array(
                array(
                    "type"        => "textfield",
                    "heading"     => esc_html__( "Title", 'royanwine' ),
                    "param_name"  => "title",
                    "value"       => '',
                    "admin_label" => true,
                ),
                array(
                    "type"        => "attach_image",
                    "heading"     => esc_html__( "Photo", 'royanwine' ),
                    "param_name"  => "photo",
                    "value"       => '',
                    'description' => '',
                ),
                array(
                    "type"        => "textfield",
                    "heading"     => esc_html__( "Job", 'royanwine' ),
                    "param_name"  => "job",
                    "value"       => 'CEO',
                    'description' => '',
                ),
                array(
                    "type"        => "textfield",
                    "heading"     => esc_html__( "Extra class name", 'royanwine' ),
                    "param_name"  => "el_class",
                    "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'royanwine' ),
                ),

            ),
        ) );


        /* Heading Text Block
        ---------------------------------------------------------- */
        vc_map( array(
            'name'        => esc_html__( 'PBR Widget Heading', 'royanwine' ),
            'base'        => 'pbr_title_heading',
            "class"       => "",
            'icon'        => get_template_directory_uri() . "/images/opal.png",
            "category"    => esc_html__( 'PBR Widgets', 'royanwine' ),
            'description' => esc_html__( 'Create title for one Widget', 'royanwine' ),
            "params"      => array(
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Widget title', 'royanwine' ),
                    'param_name'  => 'title',
                    'value'       => esc_html__( 'Title', 'royanwine' ),
                    'description' => esc_html__( 'Enter heading title.', 'royanwine' ),
                    "admin_label" => true,
                ),
                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Title Color', 'royanwine' ),
                    'param_name'  => 'font_color',
                    'description' => esc_html__( 'Select font color', 'royanwine' ),
                ),

                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Sub title', 'royanwine' ),
                    'param_name'  => 'subtitle',
                    "value"       => '',
                    'description' => esc_html__( 'Enter heading sub title.', 'royanwine' ),
                    "admin_label" => true,
                ),

                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Sub Title Color', 'royanwine' ),
                    'param_name'  => 'subtitle_color',
                    'description' => esc_html__( 'Select color for subtitle', 'royanwine' ),
                ),

                array(
                    "type"        => "textarea",
                    'heading'     => esc_html__( 'Description', 'royanwine' ),
                    "param_name"  => "descript",
                    "value"       => '',
                    'description' => esc_html__( 'Enter description for title.', 'royanwine' ),
                ),

                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Descript Color', 'royanwine' ),
                    'param_name'  => 'descript_color',
                    'description' => esc_html__( 'Select color for descript', 'royanwine' ),
                ),

                array(
                    "type"       => "dropdown",
                    "heading"    => esc_html__( "Style", 'royanwine' ),
                    "param_name" => "style",
                    'value'      => array( 'Default' => esc_html__( 'heading-default', 'royanwine' ), 'Style1' => esc_html__( 'heading-style1', 'royanwine' ), 'Style2' => esc_html__( 'heading-style2', 'royanwine' ), 'Style3' => esc_html__( 'heading-style3', 'royanwine' ) ),
                ),
                array(
                    'type'       => 'dropdown',
                    'heading'    => esc_html__( 'Title Alignment', 'royanwine' ),
                    'param_name' => 'alignment',
                    'value'      => array(
                        esc_html__( 'Align left', 'royanwine' )   => 'text-left',
                        esc_html__( 'Align center', 'royanwine' ) => 'text-center',
                        esc_html__( 'Align right', 'royanwine' )  => 'text-right',
                    ),
                    'std'        => 'text-center',
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Extra class name', 'royanwine' ),
                    'param_name'  => 'el_class',
                    'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'royanwine' ),
                ),
            ),
        ) );

        /* PBR Banner CountDown
        ---------------------------------------------------------- */
        vc_map( array(
            'name'        => esc_html__( 'PBR Banner CountDown', 'royanwine' ),
            'base'        => 'pbr_banner_countdown',
            "class"       => "",
            'icon'        => get_template_directory_uri() . "/images/opal.png",
            "category"    => esc_html__( 'PBR Widgets', 'royanwine' ),
            'description' => esc_html__( 'Show CountDown with banner', 'royanwine' ),
            "params"      => array(
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Widget title', 'royanwine' ),
                    'param_name'  => 'title',
                    'value'       => esc_html__( 'Title', 'royanwine' ),
                    'description' => esc_html__( 'Enter heading title.', 'royanwine' ),
                    "admin_label" => true,
                ),


                array(
                    "type"        => "attach_image",
                    "description" => esc_html__( "If you upload an image, icon will not show.", 'royanwine' ),
                    "param_name"  => "image",
                    "value"       => '',
                    'heading'     => esc_html__( 'Image', 'royanwine' ),
                ),


                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Date Expired', 'royanwine' ),
                    'param_name'  => 'input_datetime',
                    'description' => esc_html__( 'Select font color', 'royanwine' ),
                ),


                array(
                    'type'        => 'colorpicker',
                    'heading'     => esc_html__( 'Title Color', 'royanwine' ),
                    'param_name'  => 'font_color',
                    'description' => esc_html__( 'Select font color', 'royanwine' ),
                    'class'       => 'hacongtien',
                ),

                array(
                    "type"        => "textarea",
                    'heading'     => esc_html__( 'Description', 'royanwine' ),
                    "param_name"  => "descript",
                    "value"       => '',
                    'description' => esc_html__( 'Enter description for title.', 'royanwine' ),
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Extra class name', 'royanwine' ),
                    'param_name'  => 'el_class',
                    'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'royanwine' ),
                ),


                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Text Link', 'royanwine' ),
                    'param_name'  => 'text_link',
                    'value'       => 'Find Out More',
                    'description' => esc_html__( 'Enter your link text', 'royanwine' ),
                ),


                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Link', 'royanwine' ),
                    'param_name'  => 'link',
                    'value'       => 'http://',
                    'description' => esc_html__( 'Enter your link to redirect', 'royanwine' ),
                ),
            ),
        ) );


        /* PBR Banner effect
        ---------------------------------------------------------- */
        vc_map( array(
            'name'        => esc_html__( 'PBR Banner Effect', 'royanwine' ),
            'base'        => 'pbr_banner_effect',
            "class"       => "",
            'icon'        => get_template_directory_uri() . "/images/opal.png",
            "category"    => esc_html__( 'PBR Widgets', 'royanwine' ),
            'description' => esc_html__( 'Show info with banner', 'royanwine' ),
            "params" => array(
                array(
                    "type" => "textfield",
                    "heading" => esc_html__("Title", 'royanwine'),
                    "param_name" => "title",
                    "value" => '',    "admin_label" => true,
                ),
                array(
                    "type" => "dropdown",
                    "heading" => esc_html__("Skin", 'royanwine'),
                    "param_name" => "skin",
                    'value' 	=> array(
                        esc_html__('Dark', 'royanwine') => 'default',
                        esc_html__('Light', 'royanwine') => 'light'
                    ),
                    'std' => 'default'
                ),

                array(
                    'type'                           => 'dropdown',
                    'heading'                        => esc_html__( 'Title Alignment', 'royanwine' ),
                    'param_name'                     => 'title_align',
                    'value'                          => array(
                        esc_html__( 'Align left', 'royanwine' )   => 'separator_align_left',
                        esc_html__( 'Align center', 'royanwine' ) => 'separator_align_center',
                        esc_html__( 'Align right', 'royanwine' )  => 'separator_align_right'
                    ),
                    'std' => 'separator_align_left'
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => esc_html__( 'Background hover', 'royanwine' ),
                    'param_name' => 'background',
                    'value' => array(
                        esc_html__( 'None', 'royanwine' ) => 'nostyle',
                        esc_html__( 'Success', 'royanwine' ) => 'bg-success',
                        esc_html__( 'Info', 'royanwine' ) => 'bg-info',
                        esc_html__( 'Danger', 'royanwine' ) => 'bg-danger',
                        esc_html__( 'Warning', 'royanwine' ) => 'bg-warning',
                        esc_html__( 'Secondary', 'royanwine' ) => 'bg-secondary',
                    ),
                    'std' => 'nostyle',
                ),

                array(
                    "type" => "attach_image",
                    "heading" => esc_html__("Photo", 'royanwine'),
                    "param_name" => "photo",
                    "value" => '',
                    'description'	=> ''
                ),

                array(
                    "type" => "dropdown",
                    "heading" => esc_html__("Reverse Image", 'royanwine'),
                    "param_name" => "style",
                    'value' 	=> array(
                        esc_html__('No', 'royanwine') => '',
                        esc_html__('Yes', 'royanwine') => 'banner-reverse'
                    ),
                    'std' => ''
                ),

                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Button Text', 'royanwine' ),
                    'param_name' => 'button_text',
                    'description' => esc_html__( 'Enter button text.', 'royanwine' ),
                    "admin_label" => true
                ),

                array(
                    'type' => 'vc_link',
                    'heading' => esc_html__( 'URL (Link)', 'royanwine' ),
                    'param_name' => 'link',
                    'description' => esc_html__( 'Add link to description.', 'royanwine' ),
                    // compatible with btn2 and converted from href{btn1}
                ),

                array(
                    "type" => "textfield",
                    "heading" => esc_html__("Extra class name", 'royanwine'),
                    "param_name" => "el_class",
                    "description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'royanwine')
                )
            )
        ) );

        /* PBR Map Style
        ---------------------------------------------------------- */
        vc_map( array(
            'name'        => esc_html__( 'PBR Map Style', 'royanwine' ),
            'base'        => 'pbr_map_style',
            "class"       => "",
            'icon'        => get_template_directory_uri() . "/images/opal.png",
            "category"    => esc_html__( 'PBR Widgets', 'royanwine' ),
            'description' => esc_html__( 'Map Style Dark', 'royanwine' ),
            "params"      => array(
                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Widget title', 'royanwine' ),
                    'param_name'  => 'title',
                    'value'       => esc_html__( 'Title', 'royanwine' ),
                    'description' => esc_html__( 'Enter heading title.', 'royanwine' ),
                    "admin_label" => true,
                ),

                array(
                    "type"        => "textfield",
                    "description" => sprintf( esc_html__( "The latitude and longitude to center the map. Ex: 21.030497, 105.852510. Please go to %s to find your location and get latitude and longitude ", 'royanwine' ), '<a href="https://www.google.com/maps" target="_blank">' . esc_html__( 'Google maps', 'royanwine' ) . '</a>' ),
                    "param_name"  => "code_number",
                    "value"       => '',
                    'heading'     => esc_html__( 'The latitude and longitude', 'royanwine' ),
                ),

                array(
                    "type"        => "textfield",
                    'heading'     => esc_html__( 'Width', 'royanwine' ),
                    "param_name"  => "width",
                    "value"       => '',
                    'description' => esc_html__( 'Width of map. ex: 1000px, 500px,.. or 100%, 50%...', 'royanwine' ),
                ),
                array(
                    "type"        => "textfield",
                    'heading'     => esc_html__( 'Height', 'royanwine' ),
                    "param_name"  => "height",
                    "value"       => '',
                    'description' => esc_html__( 'height of map. ex: 400px, 500px...', 'royanwine' ),
                ),
                array(
                    "type"        => "textfield",
                    'heading'     => esc_html__( 'Google API key', 'royanwine' ),
                    "param_name"  => "api_key",
                    "value"       => '',
                    'description' => sprintf( esc_html__( 'Please go to %s to get API key. Ex: AIzaSyDRKqMOV24XuzaRMpLKiLnGwDEfauduJ1A', 'royanwine' ), '<a href="https://developers.google.com/maps/documentation/javascript/get-api-key">' . esc_html__( 'Google developers page', 'royanwine' ) . '</a>' ),
                ),

                array(
                    'type'        => 'textfield',
                    'heading'     => esc_html__( 'Extra class name', 'royanwine' ),
                    'param_name'  => 'el_class',
                    'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'royanwine' ),
                ),

            ),
        ) );

    }
}