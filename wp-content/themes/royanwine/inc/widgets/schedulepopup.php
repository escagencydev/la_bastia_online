<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author      Team <opalwordpressl@gmail.com >
 * @copyright  Copyright (C) 2015  prestabrain.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/questions/
 */
class Royanwine_Schedulepopup_Widget extends WP_Widget {
    public function __construct() {
        parent::__construct(
            // Base ID of your widget
            'Royanwine_schedulepopup_widget',
            // Widget name will appear in UI
            esc_html__('PBR Schedule Popup Form', 'royanwine'),
            // Widget description
            array( 'description' => esc_html__( 'Schedulepopup', 'royanwine' ), )
        );
    }

    /**
     * The main widget output function.
     * @param array $args
     * @param array $instance
     * @return string The widget output (html).
     */
    public function widget( $args, $instance ) {
        extract( $args, EXTR_SKIP );
        extract( $instance, EXTR_SKIP );

        $title = apply_filters( 'widget_title', $instance['title'] );
        if(!$contact_form_id) return;
        ?>
        <div class="schedulepopup">
            <a class="schedule btn btn-secondary" data-toggle="modal" data-target="#mySchedule"><?php echo esc_html($title); ?></a>
            <!-- Modal -->
            <div class="modal fade" id="mySchedule" tabindex="-1" role="dialog" aria-labelledby="mySchedule" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                        <!-- Modal Body -->
                        <div class="modal-body">
                            <?php echo royanwine_fnc_js_remove_wpautop('[contact-form-7 id="'.$contact_form_id.'" title="Schedule A Visit"]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php
    
    }

    /**
     * The function for saving widget updates in the admin section.
     * @param array $new_instance
     * @param array $old_instance
     * @return array The new widget settings.
     */
     
    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $new_instance = $this->default_instance_args( $new_instance );

        /* Strip tags (if needed) and update the widget settings. */
        $instance['title']   = $new_instance['title'];
        $instance['contact_form_id']   = $new_instance['contact_form_id'];

        return $instance;
    }

    /**
     * Output the admin form for the widget.
     * @param array $instance
     * @return string The output for the admin widget form.
     */
    public function form( $instance ) {
        $instance  = $this->default_instance_args( $instance );
        $forms = get_posts( array('posts_per_page' => -1, 'post_type' => 'wpcf7_contact_form') );
        if(empty($forms)) $forms= array();
    ?>
    <div class="widget-schedule-popup-form">
       <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'royanwine' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'contact_form_id' ) ); ?>"><?php esc_html_e( 'ID contact form', 'royanwine' ); ?></label>
            <select class="widefat" id="<?php echo  esc_attr($this->get_field_id('contact_form_id')); ?>" name="<?php echo esc_attr( $this->get_field_name('contact_form_id') ); ?>">
                <?php foreach($forms as $form): ?>
                    <option <?php if($instance['contact_form_id'] == $form->ID) echo 'selected="selected"';?>  value="<?php echo esc_attr( $form->ID);?>"><?php echo esc_attr( $form->post_title);?></option>
                <?php endforeach; ?>
            </select>
        </p>
    </div>
<?php
    }

    /**
     * Accepts and returns the widget's instance array - ensuring any missing
     * elements are generated and set to their default value.
     * @param array $instance
     * @return array
     */
    protected function default_instance_args( array $instance ) {
        return wp_parse_args( $instance, array(
            'title'   => esc_html__( 'Schedule A Visit', 'royanwine' ),
            'contact_form_id'   => '',
        ) );
    }
}

register_widget( 'Royanwine_Schedulepopup_Widget' );
