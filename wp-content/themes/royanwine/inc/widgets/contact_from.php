<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author      Team <opalwordpressl@gmail.com >
 * @copyright  Copyright (C) 2015  http://wpopal.com/. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://wpopal.com/
 * @support  http://www.wpopal.com/questions/
 */

class Royanwine_Contact_form extends WP_Widget {
    public function __construct() {
        parent::__construct(
            // Base ID of your widget
            'royanwine_contact_form',
            // Widget name will appear in UI
            esc_html__('PBR Contact Form 7', 'royanwine'),
            // Widget description
            array( 'description' => esc_html__( 'Display contact form from Contact From 7. ', 'royanwine' ), )
        );
        $this->widgetName = 'contact_form';
    }

    public function widget( $args, $instance ) {
        extract( $args );
        extract( $instance );

        echo ($before_widget);
            //wc_get_template( 'single-product/accessories.php', $instance );
            set_query_var('contact_form_id', $instance['contact_form_id']);
            set_query_var('title', $instance['title']);
            get_template_part( 'widgets/contact_form/default' );
        echo ($after_widget);
    }

    public function form( $instance ) {
        $defaults = array( 'title' => 'Contact From 7', 'contact_form_id' => '' );
        $instance = wp_parse_args((array) $instance, $defaults);

        $cf7 = get_posts( 'post_type="wpcf7_contact_form"&numberposts=-1' );
        $contact_forms = array();
        if ( $cf7 ) {
            foreach ( $cf7 as $cform ) {
                $contact_forms[ $cform->ID ] = $cform->post_title;
            }
        }
        ?>
        
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'royanwine'); ?></label>
            <input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id( 'Choose a contact form' )); ?>"><?php esc_html_e('Category:', 'royanwine'); ?></label>
            <br>
            <select name="<?php echo esc_attr($this->get_field_name( 'contact_form_id' )); ?>" id="<?php echo esc_attr($this->get_field_id( 'contact_form_id' )); ?>">
                <?php foreach ($contact_forms as $key => $value): ?>
                    <option value="<?php echo esc_attr( $key ); ?>" <?php selected( $instance['contact_form_id'], $key ); ?>><?php echo esc_html( $value ); ?></option>
                <?php endforeach; ?>
            </select>
        </p>

<?php
    }

    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['contact_form_id'] = $new_instance['contact_form_id'];

        return $instance;
    }
}

register_widget( 'Royanwine_Contact_form' );