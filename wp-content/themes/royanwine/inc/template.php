<?php 
/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Presence of header image except in Multisite signup and activate pages.
 * 3. Index views.
 * 4. Full-width content layout.
 * 5. Presence of footer widgets.
 * 6. Single views.
 * 7. Featured content layout.
 *
 * @since Royanwine 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function royanwine_fnc_body_classes( $classes ) {
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	if ( get_header_image() ) {
		$classes[] = 'header-image';
	} elseif ( ! in_array( $GLOBALS['pagenow'], array( 'wp-activate.php', 'wp-signup.php' ) ) ) {
		$classes[] = 'masthead-fixed';
	}

	 
	if ( is_singular() && ! is_front_page() ) {
		$classes[] = 'singular';
	}

	$currentSkin = str_replace( '.css','',royanwine_fnc_theme_options('skin','default') ); 
	
	if( $currentSkin ){
		$class[] = 'skin-'.$currentSkin;
	}

	return $classes;
}
add_filter( 'body_class', 'royanwine_fnc_body_classes' );

/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Royanwine 1.0
 *
 * @global int $paged WordPress archive pagination page count.
 * @global int $page  WordPress paginated post page count.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function royanwine_fnc_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
		$title = "$title $sep " . sprintf( esc_html__( 'Page %s', 'royanwine' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'royanwine_fnc_wp_title', 10, 2 );

 
 
/**
 * upbootwp_breadcrumbs function.
 * Edit the standart breadcrumbs to fit the bootstrap style without producing more css
 * @access public
 * @return void
 */
function royanwine_fnc_breadcrumbs() {

	$delimiter = '&nbsp;&#62;&nbsp;';
	$home = esc_html__('Home', 'royanwine');
	$before = '<li>';
	$after = '</li>';
	$title = '';
	if (!is_home() && !is_front_page() || is_paged()) {

		echo '<ol class="breadcrumb">';

		global $post;
		$homeLink = esc_url( home_url() );
		echo '<li><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . '</li> ';

		if (is_category()) {

			global $wp_query;
			$cat_obj = $wp_query->get_queried_object();
			$thisCat = $cat_obj->term_id;
			$thisCat = get_category($thisCat);
			$parentCat = get_category($thisCat->parent);
			if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
			echo trim($before) . single_cat_title('', false) . $after;
			$title = esc_html__('From Our Blog', 'royanwine');
		} elseif (is_day()) {
			echo '<li><a href="' . esc_url( get_year_link(get_the_time('Y')) ) . '">' . get_the_time('Y') . '</a></li> ' . $delimiter . ' ';
			echo '<li><a href="' . esc_url( get_month_link(get_the_time('Y'),get_the_time('m')) ) . '">' . get_the_time('F') . '</a></li> ' . $delimiter . ' ';
			echo trim($before) . get_the_time('d') . $after;
			$title = get_the_time('d');
		} elseif (is_month()) {
			echo '<li><a href="' . esc_url( get_year_link(get_the_time('Y')) ) . '">' . get_the_time('Y') . '</a></li> ' . $delimiter . ' ';
			echo trim($before) . get_the_time('F') . $after;
			$title = get_the_time('F');
		} elseif (is_year()) {
			echo trim($before) . get_the_time('Y') . $after;
			$title = get_the_time('Y');
		} elseif (is_single() && !is_attachment()) {
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				echo '<li><a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a></li> ' . $delimiter . ' ';
				echo trim($before) . get_the_title() . $after;
			} else {
				$cat = get_the_category(); $cat = $cat[0];
				echo get_category_parents($cat, TRUE, '');
			}
			$title = esc_html__('From Our Blog', 'royanwine');
		} elseif ( is_search() ) {
			echo trim($before) . esc_html__('Search results for "','royanwine')  . get_search_query() . '"' . $after;
			$title = esc_html__('Search results for "','royanwine')  . get_search_query() . '"';
		} elseif ( is_tag() ) {
			echo trim($before) . esc_html__('Posts tagged "', 'royanwine'). single_tag_title('', false) . '"' . $after;
			$title = esc_html__('Posts tagged "', 'royanwine'). single_tag_title('', false) . '"';
		} elseif ( is_author() ) {
			global $author;
			$userdata = get_userdata($author);
			echo trim($before) . esc_html__('Articles posted by ', 'royanwine') . $userdata->display_name . $after;
			$title = esc_html__('Articles posted by ', 'royanwine') . $userdata->display_name;
		} elseif (is_attachment()) {
			$parent = get_post($post->post_parent);
			$cat = get_the_category($parent->ID); $cat = $cat[0];
			echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
			echo '<li><a href="' . esc_url( get_permalink($parent) ) . '">' . $parent->post_title . '</a></li> ' . $delimiter . ' ';
			echo trim($before) . get_the_title() . $after;
			$title = get_the_title();
		} elseif ( is_tax()) {
			if ( is_tax( 'post_format', 'post-format-aside' ) ) :
				$title_item = esc_html__( 'Asides', 'royanwine' );

			elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
				$title_item = esc_html__( 'Images', 'royanwine' );

			elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
				$title_item = esc_html__( 'Videos', 'royanwine' );

			elseif ( is_tax( 'post_format', 'post-format-audio' ) ) :
				$title_item = esc_html__( 'Audio', 'royanwine' );

			elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
				$title_item = esc_html__( 'Quotes', 'royanwine' );

			elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
				$title_item = esc_html__( 'Links', 'royanwine' );

			elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) :
				$title_item = esc_html__( 'Galleries', 'royanwine' );

			else :
				$title_item = esc_html__( 'Asides', 'royanwine' );

			endif;
			echo trim($before) . $title_item . $after;
			$title = $title_item;


		} elseif ( is_page() && !$post->post_parent ) {
			echo trim($before) . get_the_title() . $after;
			$title = get_the_title();

		} elseif ( is_page() && $post->post_parent ) {
			$parent_id  = $post->post_parent;
			$breadcrumbs = array();
			while ($parent_id) {
				$page = get_page($parent_id);
				$breadcrumbs[] = '<li><a href="' . esc_url( get_permalink($page->ID) ) . '">' . get_the_title($page->ID) . '</a></li>';
				$parent_id  = $page->post_parent;
			}
			$breadcrumbs = array_reverse($breadcrumbs);
			foreach ($breadcrumbs as $crumb) echo trim($crumb) . ' ' . $delimiter . ' ';
			echo trim($before) . get_the_title() . $after;
			$title = get_the_title();
		} elseif ( is_404() ) {
			echo trim($before) . esc_html__('Error 404', 'royanwine') . $after;
			$title = esc_html__('Error 404', 'royanwine');
		}elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
			$post_type = get_post_type_object(get_post_type());
			if (is_object($post_type)) {
				echo trim($before) . $post_type->labels->singular_name . $after;
				$title = $post_type->labels->singular_name;
			}
		}

		echo '</ol>';

        if( is_page() && !is_home() ){
            echo '<h1>'.$title.'</h1>';
        }else {
            echo '<h1>'.$title.'</h1>';
        }
		
	}
}

 
if(!function_exists('royanwine_fnc_categories_searchform')){
    function royanwine_fnc_categories_searchform(){
        if( class_exists('WooCommerce') ){
			$dropdown_args = array(
			    'show_count'        => false,
			    'hierarchical'       => true,
			    'show_uncategorized' => 0
			);
		?>
		<form method="get" class="input-group search-category" action="<?php echo esc_url( home_url('/') ); ?>"><div class="input-group-addon search-category-container">
		  <div class="select">
		    <?php wc_product_dropdown_categories( $dropdown_args ); ?>
		  </div>
		</div>
		<input name="s" maxlength="60" class="form-control search-category-input" type="text" size="20" placeholder="<?php esc_attr_e('What do you need...', 'royanwine'); ?>"> 

		<div class="input-group-btn">
		    <label class="btn btn-link btn-search">
		      <span class="title-search hidden"><?php esc_html_e('Search', 'royanwine') ?></span>
		      <input type="submit" class="fa searchsubmit" value="&#xf002;"/>
		    </label>
		    <input type="hidden" name="post_type" value="product"/>
		</div>
		</form>
		<?php
		}else{
			get_search_form();
		}
    }
}

if(!function_exists('royanwine_pbr_string_limit_words')){
    function royanwine_pbr_string_limit_words($string, $word_limit)
    {
		$words = explode(' ', $string, ($word_limit + 1));

		if(count($words) > $word_limit) {
		array_pop($words);
		}

		return implode(' ', $words);
    }
}

function royanwine_fnc_show_image_footer(){
	$image_footer = royanwine_fnc_theme_options('image-payment', '');
	if($image_footer && !empty($image_footer)):
		echo '<img src="'. esc_url_raw($image_footer ).'">';
	endif;
}
add_action('royanwine_fnc_credits', 'royanwine_fnc_show_image_footer');


if (!function_exists('royanwine_nav_menu_social_icons')) {

    /**
     * Display SVG icons in social links menu.
     *
     * @param  string  $item_output The menu item output.
     * @param  WP_Post $item        Menu item object.
     * @param  int     $depth       Depth of the menu.
     * @param  object  $args        wp_nav_menu() arguments.
     *
     * @return string  $item_output The menu item output with social icon.
     */
    function royanwine_nav_menu_social_icons($item_output, $item, $depth, $args) {
        // Get supported social icons.
        $social_icons = royanwine_social_links_icons();

        // Change SVG icon inside social links menu if there is supported URL.
        if ('social' === $args->theme_location) {
            foreach ($social_icons as $attr => $value) {
                if (false !== strpos($item_output, $attr)) {
                    $item_output = str_replace($args->link_after, '</span><i class="' . esc_attr($value) . '" aria-hidden="true"></i>', $item_output);
                }
            }
        }

        return $item_output;
    }
}
add_filter('walker_nav_menu_start_el', 'royanwine_nav_menu_social_icons', 10, 4);


if (!function_exists('royanwine_social_links_icons')) {

    /**
     * Returns an array of supported social links (URL and icon name).
     *
     * @return array $social_links_icons
     */
    function royanwine_social_links_icons() {
        // Supported social links icons.
        $social_links_icons = array(
            'behance.net'     => 'fa fa-behance',
            'codepen.io'      => 'fa fa-codepen',
            'deviantart.com'  => 'fa fa-deviantart',
            'digg.com'        => 'fa fa-digg',
            'dribbble.com'    => 'fa fa-dribbble',
            'dropbox.com'     => 'fa fa-dropbox',
            'facebook.com'    => 'fa fa-facebook',
            'flickr.com'      => 'fa fa-flickr',
            'foursquare.com'  => 'fa fa-foursquare',
            'plus.google.com' => 'fa fa-google-plus',
            'github.com'      => 'fa fa-github',
            'instagram.com'   => 'fa fa-instagram',
            'linkedin.com'    => 'fa fa-linkedin',
            'mailto:'         => 'fa fa-envelope-o',
            'medium.com'      => 'fa fa-medium',
            'pinterest.com'   => 'fa fa-pinterest-p',
            'getpocket.com'   => 'fa fa-get-pocket',
            'reddit.com'      => 'fa fa-reddit-alien',
            'skype.com'       => 'fa fa-skype',
            'skype:'          => 'fa fa-skype',
            'slideshare.net'  => 'fa fa-slideshare',
            'snapchat.com'    => 'fa fa-snapchat-ghost',
            'soundcloud.com'  => 'fa fa-soundcloud',
            'spotify.com'     => 'fa fa-spotify',
            'stumbleupon.com' => 'fa fa-stumbleupon',
            'tumblr.com'      => 'fa fa-tumblr',
            'twitch.tv'       => 'fa fa-twitch',
            'twitter.com'     => 'fa fa-twitter',
            'vimeo.com'       => 'fa fa-vimeo',
            'vine.co'         => 'fa fa-vine',
            'vk.com'          => 'fa fa-vk',
            'wordpress.org'   => 'fa fa-wordpress',
            'wordpress.com'   => 'fa fa-wordpress',
            'yelp.com'        => 'fa fa-yelp',
            'youtube.com'     => 'fa fa-youtube',
        );

        return apply_filters('royanwine_social_links_icons', $social_links_icons);
    }
}