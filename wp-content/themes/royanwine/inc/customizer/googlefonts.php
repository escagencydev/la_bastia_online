<?php 

function royanwine_google_fonts_customizer( $wp_customize ){

	$wp_customize -> add_section( 'typography_options', array(
		'title' => esc_html__( 'Typography Options', 'royanwine' ),
		'description' => esc_html__('Modify Fonts','royanwine'),
		'priority' => 6
	));
}
add_action( 'customize_register', 'royanwine_google_fonts_customizer' );