<?php 


/**
 * function to integrate with WPML which will display languages as buttons
 */

if (!function_exists( "royanwine_fnc_wpml_language_buttons" )) {
    function royanwine_fnc_wpml_language_buttons() {
        if (function_exists( 'icl_get_languages' )) {
            $languages = icl_get_languages( 'skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str' );
            if (is_array( $languages )) {
                echo '<div class="language wpml-languages">';
                echo '<ul class="wpml-lang-dropdown">';
                foreach ($languages as $lang) {
                    $lang_active = ( $lang['active'] ) ? 'active' : '';
                    echo '<li class="' . esc_attr( $lang_active ) . '"><a href="' . esc_url( $lang['url'] ) . '"><img src="' . esc_url( $lang['country_flag_url'] ) . '" alt="' . esc_attr( $lang['translated_name'] ) . '"/>' . esc_attr( $lang['translated_name'] ) . '</a></li>';
                }
                echo '</ul>';
                echo '</div>';
            }
        }
    }
}


 /**
  * find all header files with prefix name having header-
  */
function royanwine_fnc_get_header_layouts(){
    $path = get_template_directory().'/header-*.php';
    $files = glob( $path  );
    $headers = array( '' => esc_html__('Default', 'royanwine') );
    if( count($files)>0 ){
        foreach ($files as $key => $file) {
            $header = str_replace( "header-", '', str_replace( '.php', '', basename($file) ) );
            $headers[$header] = esc_html__( 'Header', 'royanwine' ) . ' ' .str_replace( '-',' ', ucfirst( $header ) );
        }
    }

    return $headers;
}

 /**
  * Get list of footer profile as array. they are post from  post type 'footer'
  */
function royanwine_fnc_get_footer_profiles(){
    
    $footers_type = get_posts( array('posts_per_page' => -1, 'post_type' => 'footer') );
    $footers = array(  '' => esc_html__('None', 'royanwine') );
    if($footers_type && is_array($footers_type)){
        foreach ($footers_type as $key => $value) {
            $footers[$value->ID] = $value->post_title;
        }
    }
    wp_reset_query();

    return $footers;
}

/**
 * get list of menu group
 */
function royanwine_fnc_get_menugroups(){
    $menus       = wp_get_nav_menus( );
    $option_menu = array( '' => '---Select Menu---' );
    foreach ($menus as $menu) {
        $option_menu[$menu->term_id]=$menu->name;
    }
    return $option_menu;
}

/**
 *
 */
function royanwine_fnc_cst_skins(){
    $path = get_template_directory() .'/css/skins/*';
    $files = glob($path , GLOB_ONLYDIR );
    $skins = array( 'default' => 'default' );
    if( count($files) > 0 ){
      foreach ($files as $key => $file) {
          $skin = str_replace( '.css', '', basename($file) );
          $skins[$skin] =  $skin;
      }
    }

    return $skins;
}

/**
 * Footer builder profile is custom post type, its content is shortcode rendering with visual composer 
 *
 * @param $footer 
 * 
 */
function royanwine_fnc_render_post_content( $footer ){

  global $royanwine_wpopconfig; 

	$post = get_post( $footer );
    $royanwine_wpopconfig['type'] = 'footer';
	if(is_object($post) && $post){
    	echo do_shortcode( $post->post_content ); 
  }
  
  $royanwine_wpopconfig['type'] = '';

  wp_reset_query();	
}


/**
 * create a random key to use as primary key.
 */
if(!function_exists('royanwine_fnc_makeid')){
    function royanwine_fnc_makeid($length = 5){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
}



if(!function_exists('royanwine_fnc_excerpt')){
    //Custom Excerpt Function
    function royanwine_fnc_excerpt($limit,$afterlimit='[...]') {
        $excerpt = get_the_excerpt();
        if( $excerpt != ''){
           $excerpt = explode(' ', strip_tags( $excerpt ), $limit);
        }else{
            $excerpt = explode(' ', strip_tags(get_the_content( )), $limit);
        }
        if (count($excerpt)>=$limit) {
            array_pop($excerpt);
            $excerpt = implode(" ",$excerpt).' '.$afterlimit;
        } else {
            $excerpt = implode(" ",$excerpt);
        }
        $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
        return strip_shortcodes( $excerpt );
    }
}


function royanwine_fnc_get_widget_block_styles(){
   return array(  'default' , 'primary', 'danger' , 'success', 'warning', 'coffe', 'bluesky' );
}

function royanwine_fnc_get_svg_divider($name, $color, $custom_height = '') {
    $folder = trailingslashit( get_template_directory()) . 'images/svg';
    $file = $folder . '/' . $name . '.svg';
    if (file_exists($file)) {
        include_once( ABSPATH . 'wp-admin/includes/file.php' );
        WP_Filesystem();
        global $wp_filesystem;

        $content = $wp_filesystem->get_contents($file);
        $style = 'fill:  ' . esc_html($color) . ';';
        if ($name === 'grime-top.svg') {
            $style .= 'margin-top: -1px;';
        }
        $style .= ($custom_height) ? 'height: ' . esc_html($custom_height) . ';' : '';
        return preg_replace("/<svg/", "<svg style=\"" . $style . "\"", $content);
    }
    return false;
}

function royanwine_fnc_get_svg_icon($name, $color, $svg_width = '') {
    $folder = trailingslashit( get_template_directory()) . 'images/svg';
    $file = $folder . '/' . $name . '.svg';
    if (file_exists($file)) {
        include_once( ABSPATH . 'wp-admin/includes/file.php' );
        WP_Filesystem();
        global $wp_filesystem;

        $content = $wp_filesystem->get_contents($file);
        $style = 'fill:  ' . esc_html($color) . ';';
        if ($name === 'grime-top.svg') {
            $style .= 'margin-top: -1px;';
        }
        $style .= ($svg_width) ? 'width: ' . esc_html($svg_width) . ';' : '';
        return preg_replace("/<svg/", "<svg style=\"" . $style . "\"", $content);
    }
    return false;
}
