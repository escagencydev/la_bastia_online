<?php
/**
 * mode Customizer support
 *
 * @package WpOpal
 * @subpackage royanwine
 * @since royanwine 1.0
 */
//Update logo wordpress 4.5
if (version_compare($GLOBALS['wp_version'], '4.5', '>=')) {
    function royanwine_fnc_setup_logo()
    {
        add_theme_support('custom-logo');
    }

    add_action('after_setup_theme', 'royanwine_fnc_setup_logo');
}
if ( ! function_exists( 'royanwine_fnc_customize_register' ) ) :
function royanwine_fnc_customize_register($wp_customize){
    $wp_customize->remove_section('colors');

    // Add Panel Colors
    $wp_customize->add_panel('colors', array(
        'priority' => 15,
        'capability' => 'edit_theme_options',
        'theme_supports' => '',
        'title' => esc_html__('Colors', 'royanwine'),
    ));
    /* OpalTool: inject code */
    /* OpalTool: end inject code */
        // Add Section header
    $wp_customize->add_section('royanwine_color_header', array(
        'title'      => esc_html__('Header', 'royanwine'),
        'transport'  => 'postMessage',
        'priority'   => 10,
        'panel'      => 'colors'
    ));    // Add setting header_bg
    $wp_customize->add_setting('royanwine_color_header_bg', array(
        'default'    => get_option('royanwine_color_header_bg'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control header_bg
    $wp_customize->add_control('royanwine_color_header_bg', array(
        'label'    => esc_html__('Header Background', 'royanwine'),
        'section'  => 'royanwine_color_header',
        'type'      => 'color',
    ) );
    // Add setting header_color
    $wp_customize->add_setting('royanwine_color_header_color', array(
        'default'    => get_option('royanwine_color_header_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control header_color
    $wp_customize->add_control('royanwine_color_header_color', array(
        'label'    => esc_html__('Header Color', 'royanwine'),
        'section'  => 'royanwine_color_header',
        'type'      => 'color',
    ) );
    // Add setting headerlink_color
    $wp_customize->add_setting('royanwine_color_headerlink_color', array(
        'default'    => get_option('royanwine_color_headerlink_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control headerlink_color
    $wp_customize->add_control('royanwine_color_headerlink_color', array(
        'label'    => esc_html__('Header Link Color', 'royanwine'),
        'section'  => 'royanwine_color_header',
        'type'      => 'color',
    ) );
    // Add setting headerlink_hover_color
    $wp_customize->add_setting('royanwine_color_headerlink_hover_color', array(
        'default'    => get_option('royanwine_color_headerlink_hover_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control headerlink_hover_color
    $wp_customize->add_control('royanwine_color_headerlink_hover_color', array(
        'label'    => esc_html__('Header Link Hover Color', 'royanwine'),
        'section'  => 'royanwine_color_header',
        'type'      => 'color',
    ) );
    // Add setting topbar_bg
    $wp_customize->add_setting('royanwine_color_topbar_bg', array(
        'default'    => get_option('royanwine_color_topbar_bg'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control topbar_bg
    $wp_customize->add_control('royanwine_color_topbar_bg', array(
        'label'    => esc_html__('Topbar Background', 'royanwine'),
        'section'  => 'royanwine_color_header',
        'type'      => 'color',
    ) );
    // Add setting topbartext_color
    $wp_customize->add_setting('royanwine_color_topbartext_color', array(
        'default'    => get_option('royanwine_color_topbartext_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control topbartext_color
    $wp_customize->add_control('royanwine_color_topbartext_color', array(
        'label'    => esc_html__('Topbar Text', 'royanwine'),
        'section'  => 'royanwine_color_header',
        'type'      => 'color',
    ) );
    // Add setting topbartext_hover_color
    $wp_customize->add_setting('royanwine_color_topbartext_hover_color', array(
        'default'    => get_option('royanwine_color_topbartext_hover_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control topbartext_hover_color
    $wp_customize->add_control('royanwine_color_topbartext_hover_color', array(
        'label'    => esc_html__('Topbar Hover Link', 'royanwine'),
        'section'  => 'royanwine_color_header',
        'type'      => 'color',
    ) );
    // Add Section breadscrumb
    $wp_customize->add_section('royanwine_color_breadscrumb', array(
        'title'      => esc_html__('Breadscrumb', 'royanwine'),
        'transport'  => 'postMessage',
        'priority'   => 10,
        'panel'      => 'colors'
    ));    // Add setting breadscrumb_bg
    $wp_customize->add_setting('royanwine_color_breadscrumb_bg', array(
        'default'    => get_option('royanwine_color_breadscrumb_bg'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control breadscrumb_bg
    $wp_customize->add_control('royanwine_color_breadscrumb_bg', array(
        'label'    => esc_html__('Breadscrumb BG', 'royanwine'),
        'section'  => 'royanwine_color_breadscrumb',
        'type'      => 'color',
    ) );
    // Add setting breadscrumb_link_hover
    $wp_customize->add_setting('royanwine_color_breadscrumb_link_hover', array(
        'default'    => get_option('royanwine_color_breadscrumb_link_hover'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control breadscrumb_link_hover
    $wp_customize->add_control('royanwine_color_breadscrumb_link_hover', array(
        'label'    => esc_html__('Breadscrumb Link Color', 'royanwine'),
        'section'  => 'royanwine_color_breadscrumb',
        'type'      => 'color',
    ) );
    // Add setting breadscrumb_title
    $wp_customize->add_setting('royanwine_color_breadscrumb_title', array(
        'default'    => get_option('royanwine_color_breadscrumb_title'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control breadscrumb_title
    $wp_customize->add_control('royanwine_color_breadscrumb_title', array(
        'label'    => esc_html__('Breadscrumb Title Color', 'royanwine'),
        'section'  => 'royanwine_color_breadscrumb',
        'type'      => 'color',
    ) );
    // Add Section footer
    $wp_customize->add_section('royanwine_color_footer', array(
        'title'      => esc_html__('Footer', 'royanwine'),
        'transport'  => 'postMessage',
        'priority'   => 10,
        'panel'      => 'colors'
    ));    // Add setting footer_bg
    $wp_customize->add_setting('royanwine_color_footer_bg', array(
        'default'    => get_option('royanwine_color_footer_bg'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control footer_bg
    $wp_customize->add_control('royanwine_color_footer_bg', array(
        'label'    => esc_html__('Footer BG', 'royanwine'),
        'section'  => 'royanwine_color_footer',
        'type'      => 'color',
    ) );
    // Add setting footer_color
    $wp_customize->add_setting('royanwine_color_footer_color', array(
        'default'    => get_option('royanwine_color_footer_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control footer_color
    $wp_customize->add_control('royanwine_color_footer_color', array(
        'label'    => esc_html__('Footer Color', 'royanwine'),
        'section'  => 'royanwine_color_footer',
        'type'      => 'color',
    ) );
    // Add setting heading_color
    $wp_customize->add_setting('royanwine_color_heading_color', array(
        'default'    => get_option('royanwine_color_heading_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control heading_color
    $wp_customize->add_control('royanwine_color_heading_color', array(
        'label'    => esc_html__('Heading Color', 'royanwine'),
        'section'  => 'royanwine_color_footer',
        'type'      => 'color',
    ) );
    // Add Section copyright
    $wp_customize->add_section('royanwine_color_copyright', array(
        'title'      => esc_html__('Copyright', 'royanwine'),
        'transport'  => 'postMessage',
        'priority'   => 10,
        'panel'      => 'colors'
    ));    // Add setting copyright_bg
    $wp_customize->add_setting('royanwine_color_copyright_bg', array(
        'default'    => get_option('royanwine_color_copyright_bg'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control copyright_bg
    $wp_customize->add_control('royanwine_color_copyright_bg', array(
        'label'    => esc_html__('Copyright BG', 'royanwine'),
        'section'  => 'royanwine_color_copyright',
        'type'      => 'color',
    ) );
    // Add setting copyright_color
    $wp_customize->add_setting('royanwine_color_copyright_color', array(
        'default'    => get_option('royanwine_color_copyright_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control copyright_color
    $wp_customize->add_control('royanwine_color_copyright_color', array(
        'label'    => esc_html__('Copyright Color', 'royanwine'),
        'section'  => 'royanwine_color_copyright',
        'type'      => 'color',
    ) );
    // Add Section woocommerce
    $wp_customize->add_section('royanwine_color_woocommerce', array(
        'title'      => esc_html__('WooCommerce', 'royanwine'),
        'transport'  => 'postMessage',
        'priority'   => 10,
        'panel'      => 'colors'
    ));    // Add setting product_bg
    $wp_customize->add_setting('royanwine_color_product_bg', array(
        'default'    => get_option('royanwine_color_product_bg'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control product_bg
    $wp_customize->add_control('royanwine_color_product_bg', array(
        'label'    => esc_html__('Product BG', 'royanwine'),
        'section'  => 'royanwine_color_woocommerce',
        'type'      => 'color',
    ) );
    // Add setting product_name
    $wp_customize->add_setting('royanwine_color_product_name', array(
        'default'    => get_option('royanwine_color_product_name'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control product_name
    $wp_customize->add_control('royanwine_color_product_name', array(
        'label'    => esc_html__('Product Name', 'royanwine'),
        'section'  => 'royanwine_color_woocommerce',
        'type'      => 'color',
    ) );
    // Add setting price_color
    $wp_customize->add_setting('royanwine_color_price_color', array(
        'default'    => get_option('royanwine_color_price_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control price_color
    $wp_customize->add_control('royanwine_color_price_color', array(
        'label'    => esc_html__('Price Color', 'royanwine'),
        'section'  => 'royanwine_color_woocommerce',
        'type'      => 'color',
    ) );
    // Add setting price_old_color
    $wp_customize->add_setting('royanwine_color_price_old_color', array(
        'default'    => get_option('royanwine_color_price_old_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control price_old_color
    $wp_customize->add_control('royanwine_color_price_old_color', array(
        'label'    => esc_html__('Price Old Color', 'royanwine'),
        'section'  => 'royanwine_color_woocommerce',
        'type'      => 'color',
    ) );
    // Add setting button_groups_bgcolor
    $wp_customize->add_setting('royanwine_color_button_groups_bgcolor', array(
        'default'    => get_option('royanwine_color_button_groups_bgcolor'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control button_groups_bgcolor
    $wp_customize->add_control('royanwine_color_button_groups_bgcolor', array(
        'label'    => esc_html__('Background(Wishlist, Compare, Quickview)', 'royanwine'),
        'section'  => 'royanwine_color_woocommerce',
        'type'      => 'color',
    ) );
    // Add setting button_groups_color
    $wp_customize->add_setting('royanwine_color_button_groups_color', array(
        'default'    => get_option('royanwine_color_button_groups_color'),
        'type'       => 'option',
        'capability' => 'manage_options',
        'transport'  => 'postMessage',
        'sanitize_callback' => 'sanitize_hex_color'
    ) );

    // Add Control button_groups_color
    $wp_customize->add_control('royanwine_color_button_groups_color', array(
        'label'    => esc_html__('Color(Wishlist, Compare, Quickview)', 'royanwine'),
        'section'  => 'royanwine_color_woocommerce',
        'type'      => 'color',
    ) );

    /* 
     /*****************************************************************
     * Google map API
     *****************************************************************/   
     $wp_customize->add_section('google_map_api', array(
        'title'      => esc_html__('Google map API', 'brize'),
        'transport'  => 'postMessage',
        'priority'   => 121,
    ));
     $wp_customize->add_setting( 'pbr_theme_map_api', array(
        'type'           => 'option',
        'capability'     => 'manage_options',
        'default'        => ''   ,
        'sanitize_callback' => 'sanitize_text_field'
        //  'theme_supports' => 'static-front-page',
    ) );
    
     $wp_customize->add_control( 'pbr_theme_map_api', array(
        'label'      => esc_html__( 'Google map API Key', 'brize' ),
        'section'    => 'google_map_api',
        'type'       => 'text',
    ) );

}
endif;
add_action('customize_register', 'royanwine_fnc_customize_register', 99);