<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOPAL Team <opalwordpress@gmail.com?>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/questions/
 */

/**
 * WPOPAL Category Drop Down List Class
 * modified dropdown-pages from wp-includes/class-wp-customize-control.php
 *
 * @since WPOPAL v1.0
 */
if (class_exists( "WP_Customize_Control" )){

    include_once( get_template_directory() . '/inc/customizer/googlefonts.php' );

    class Royanwine_Sidebar_DropDown extends WP_Customize_Control {

        public function render_content() {

            global $wp_registered_sidebars;

            $output = array();

            $output[] = '<option value="">' . esc_html__( 'No Sidebar', 'royanwine' ) . '</option>';

            foreach ($wp_registered_sidebars as $sidebar) {
                $selected = ( $this->value() == $sidebar['id'] ) ? ' selected="selected" ' : '';
                $output[] = '<option value="' . $sidebar['id'] . '" ' . $selected . '>' . $sidebar['name'] . '</option>';
            }

            $dropdown = '<select>' . implode( " ", $output ) . '</select>';
            $dropdown = str_replace( '<select', '<select ' . $this->get_link(), $dropdown );

            printf(
                '<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
                $this->label,
                $dropdown
            );

        }
    }

    ///
    class Royanwine_Layout_DropDown extends WP_Customize_Control {
        public $type = "PBR_Layout";

        public function render_content() {

            $layouts = array(
                'fullwidth'     => esc_html__( 'Full width', 'royanwine' ),
                'leftmain'      => esc_html__( 'Left - Main Sidebar', 'royanwine' ),
                'mainright'     => esc_html__( 'Main - Right Sidebar', 'royanwine' ),
                'leftmainright' => esc_html__( 'Left - Main - Right Sidebar', 'royanwine' ),

            );
            printf(
                '<label class="customize-control-select"><span class="customize-control-title">%s</span>',
                $this->label

            );
            ?>
            <div class="page-layout">


            <?php
            $output = array();

            foreach ($layouts as $key => $layout) {
                $v        = $this->value() ? $this->value() : 'fullwidth';
                $selected = ( $v == $key ) ? ' selected="selected" ' : '';
                $output[] = '<option value="' . $key . '" ' . $selected . '>' . $layout . '</option>';
            }

            $dropdown = '<select>' . implode( " ", $output ) . '</select>';
            $dropdown = str_replace( '<select', '<select ' . $this->get_link(), $dropdown );

            printf(
                '%s</label>',

                $dropdown
            ) . '</div>';
        }
    }

}
if (class_exists( 'WP_Customize_Control' )){
    class WP_Customize_Dropdown_Categories_Control extends WP_Customize_Control {
        public $type = 'dropdown-categories';

        public function render_content() {
            $dropdown = wp_dropdown_categories(
                array(
                    'name'             => '_customize-dropdown-categories-' . $this->id,
                    'echo'             => 0,
                    'hide_empty'       => false,
                    'show_option_none' => '&mdash; ' . esc_html__( 'Select', 'royanwine' ) . ' &mdash;',
                    'hide_if_empty'    => false,
                    'selected'         => $this->value(),
                )
            );

            $dropdown = str_replace( '<select', '<select ' . $this->get_link(), $dropdown );

            printf(
                '<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
                $this->label,
                $dropdown
            );
        }
    }
}

/**
 * WPOPAL TextArea Control Class
 * create custom textarea input field
 *
 * @since WPOPAL v0.5
 **/

if (class_exists( 'WP_Customize_Control' )){
    # Adds textarea support to the theme customizer
    class PBRTextAreaControl extends WP_Customize_Control {
        public $type = 'textarea'; # can change to 'number' for input[type=number] field

        public function __construct($manager, $id, $args = array()) {
            $this->statuses = array( '' => esc_html__( 'Default', 'royanwine' ) );
            parent::__construct( $manager, $id, $args );
        }

        public function render_content() {
            echo '<label>
                <span class="customize-control-title">' . esc_html( $this->label ) . '</span>
                <textarea rows="5" style="width:100%;" ';
            $this->link();
            echo '>' . esc_textarea( $this->value() ) . '</textarea>
                </label>';
        }
    }

}

if (class_exists( 'WP_Customize_Control' )){


    /**
     * Class to create a custom tags control
     */
    class Text_Editor_Custom_Control extends WP_Customize_Control {
        /**
         * Render the content on the theme customizer page
         */
        public function render_content() {
            ?>
            <label>
                <span class="customize-text_editor"><?php echo esc_html( $this->label ); ?></span>
                <?php
                $settings = array(
                    'textarea_name' => $this->id,
                );

                wp_editor( $this->value(), $this->id, $settings );
                ?>
            </label>
            <?php
        }
    }
}

/**
 *
 */
add_action( 'customize_register', 'royanwine_fnc_cst_customizer' );

function royanwine_fnc_cst_customizer($wp_customize) {

    # General Settings
    // Panel Header
    $wp_customize->add_section( 'pbr_cst_general_settings', array(
        'title'       => esc_html__( 'General Settings', 'royanwine' ),
        'description' => esc_html__( 'Website General Settings', 'royanwine' ),
        'transport'   => 'postMessage',
        'priority'    => 10,
    ) );

    // Parameter Options
    $wp_customize->add_setting( 'blogname', array(
        'default'           => get_option( 'blogname' ),
        'type'              => 'option',
        'capability'        => 'manage_options',
        'transport'         => 'postMessage',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'blogname', array(
        'label'    => esc_html__( 'Site Title', 'royanwine' ),
        'section'  => 'pbr_cst_general_settings',
        'priority' => 1,
    ) );

    //
    $wp_customize->add_setting( 'blogdescription', array(
        'default'           => get_option( 'blogdescription' ),
        'type'              => 'option',
        'capability'        => 'manage_options',
        'transport'         => 'postMessage',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'blogdescription', array(
        'label'    => esc_html__( 'Tagline', 'royanwine' ),
        'section'  => 'pbr_cst_general_settings',
        'priority' => 2,
    ) );


    // 
    $wp_customize->add_setting( 'display_header_text', array(
        'default'           => 1,
        'type'              => 'option',
        'capability'        => 'manage_options',
        'transport'         => 'postMessage',
        'sanitize_callback' => 'sanitize_text_field',
    ) );
    $wp_customize->add_control( 'display_header_text', array(
        'settings' => 'header_textcolor',
        'label'    => esc_html__( 'Show Title & Tagline', 'royanwine' ),
        'section'  => 'pbr_cst_general_settings',
        'type'     => 'checkbox',
        'priority' => 4,
    ) );


    /*
    *** Header info
    */
    $wp_customize->add_setting( 'pbr_theme_options[header_info_title]', array(
        'default'           => 'Call us',
        'type'              => 'option',
        'capability'        => 'manage_options',
        'transport'         => 'postMessage',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'pbr_theme_options[header_info_title]', array(
        'label'    => esc_html__( 'Header Info Title', 'royanwine' ),
        'section'  => 'pbr_cst_general_settings',
        'settings' => 'pbr_theme_options[header_info_title]',
        'priority' => 7,
    ) );

    $wp_customize->add_setting( 'pbr_theme_options[header_info_number]', array(
        'default'           => '123-456-7899',
        'type'              => 'option',
        'capability'        => 'manage_options',
        'transport'         => 'postMessage',
        'sanitize_callback' => 'sanitize_text_field',
    ) );
    $wp_customize->add_control( 'pbr_theme_options[header_info_number]', array(
        'label'    => esc_html__( 'Phone', 'royanwine' ),
        'section'  => 'pbr_cst_general_settings',
        'settings' => 'pbr_theme_options[header_info_number]',
        'priority' => 8,
    ) );


    /*
     * Breadcrumb background
     */
    $wp_customize->add_setting( 'pbr_theme_options[breadcrumb-bg]', array(
        'default'           => '',
        'type'              => 'option',
        'capability'        => 'manage_options',
        'sanitize_callback' => 'esc_url_raw',
    ) );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'pbr_theme_options[breadcrumb-bg]', array(
        'label'    => esc_html__( 'Background Breadcrumb', 'royanwine' ),
        'section'  => 'pbr_cst_general_settings',
        'settings' => 'pbr_theme_options[breadcrumb-bg]',
        'priority' => 10,
    ) ) );

    $wp_customize->add_setting( 'pbr_theme_options[parallax-breadcrumb]', array(
        'capability'        => 'edit_theme_options',
        'type'              => 'option',
        'default'           => 0,
        'checked'           => 1,
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'pbr_theme_options[parallax-breadcrumb]', array(
        'settings'  => 'pbr_theme_options[parallax-breadcrumb]',
        'label'     => esc_html__( 'Parallax Breadcrumb', 'royanwine' ),
        'section'   => 'pbr_cst_general_settings',
        'type'      => 'checkbox',
        'transport' => 4,
    ) );


    /* 
     * Custom Logo 
     */
    if (version_compare( $GLOBALS['wp_version'], '4.5', '<' )){
        $wp_customize->add_setting( 'pbr_theme_options[logo]', array(
            'default'           => '',
            'type'              => 'option',
            'capability'        => 'manage_options',
            'sanitize_callback' => 'esc_url_raw',
        ) );

        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'pbr_theme_options[logo]', array(
            'label'    => esc_html__( 'Logo', 'royanwine' ),
            'section'  => 'pbr_cst_general_settings',
            'settings' => 'pbr_theme_options[logo]',
            'priority' => 10,
        ) ) );
    }

    //
    $wp_customize->add_setting( 'pbr_theme_options[copyright_text]', array(
        'default'           => 'Copyright 2018 - Royan Wine - All Rights Reserved.',
        'type'              => 'option',
        'transport'         => 'refresh',
        'sanitize_callback' => 'sanitize_textarea_field',
    ) );

    $wp_customize->add_control( new PBRTextAreaControl( $wp_customize, 'pbr_theme_options[copyright_text]', array(
        'label'    => esc_html__( 'Copyright text', 'royanwine' ),
        'section'  => 'pbr_cst_general_settings',
        'settings' => 'pbr_theme_options[copyright_text]',
        'priority' => 12,
    ) ) );


    /***************************************************************************
     * Theme Settings
     ***************************************************************************/


    /**
     * General Setting
     */
    $wp_customize->add_section( 'ts_general_settings', array(
        'priority'       => 12,
        'capability'     => 'edit_theme_options',
        'theme_supports' => '',
        'title'          => esc_html__( 'Themes And Layouts Setting', 'royanwine' ),
        'description'    => '',
    ) );

    //
    $wp_customize->add_setting( 'pbr_theme_options[skin]', array(
        'type'              => 'option',
        'capability'        => 'manage_options',
        'default'           => 'default',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'pbr_theme_options[skin]', array(
        'label'   => esc_html__( 'Active Skin', 'royanwine' ),
        'section' => 'ts_general_settings',
        'type'    => 'select',
        'choices' => royanwine_fnc_cst_skins(),
    ) );

    $wp_customize->add_setting( 'pbr_theme_options[headerlayout]', array(
        'type'              => 'option',
        'capability'        => 'manage_options',
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'pbr_theme_options[headerlayout]', array(
        'label'   => esc_html__( 'Header Layout Style', 'royanwine' ),
        'section' => 'ts_general_settings',
        'type'    => 'select',
        'choices' => array( '' => 'Default' ),
        'choices' => royanwine_fnc_get_header_layouts(),
    ) );


    $wp_customize->add_setting( 'pbr_theme_options[footer-style]', array(
        'type'              => 'option',
        'capability'        => 'manage_options',
        'default'           => 'default',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'pbr_theme_options[footer-style]', array(
        'label'   => esc_html__( 'Footer Styles Builder', 'royanwine' ),
        'section' => 'ts_general_settings',
        'type'    => 'select',
        'choices' => royanwine_fnc_get_footer_profiles(),
    ) );

    $wp_customize->add_setting( 'pbr_theme_options[keepheader]', array(
        'capability'        => 'edit_theme_options',
        'type'              => 'option',
        'default'           => 0,
        'checked'           => 1,
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'pbr_theme_options[keepheader]', array(
        'settings'  => 'pbr_theme_options[keepheader]',
        'label'     => esc_html__( 'Keep Header', 'royanwine' ),
        'section'   => 'ts_general_settings',
        'type'      => 'checkbox',
        'transport' => 4,
    ) );

    $wp_customize->add_setting( 'pbr_theme_options[topbar]', array(
        'capability'        => 'edit_theme_options',
        'type'              => 'option',
        'default'           => 0,
        'checked'           => 1,
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'pbr_theme_options[topbar]', array(
        'settings'  => 'pbr_theme_options[topbar]',
        'label'     => esc_html__( 'Show Topbar Header', 'royanwine' ),
        'section'   => 'ts_general_settings',
        'type'      => 'checkbox',
        'transport' => 4,
    ) );


    /******************************************************************
     * Social share
     ******************************************************************/
    $wp_customize->add_section( 'social_share_settings', array(
        'priority'       => 50,
        'capability'     => 'edit_theme_options',
        'theme_supports' => '',
        'title'          => esc_html__( 'Social Share setting', 'royanwine' ),
        'description'    => '',
    ) );

    // Share facebook
    pbr_social_config( $wp_customize, 'facebook_share_blog', esc_html__( 'Share facebook ', 'royanwine' ), 'social_share_settings' );
    //share twitter
    pbr_social_config( $wp_customize, 'twitter_share_blog', esc_html__( 'Share twitter ', 'royanwine' ), 'social_share_settings' );
    //share linkedin
    pbr_social_config( $wp_customize, 'linkedin_share_blog', esc_html__( 'Share linkedin ', 'royanwine' ), 'social_share_settings' );
    //share tumblr
    pbr_social_config( $wp_customize, 'tumblr_share_blog', esc_html__( 'Share tumblr ', 'royanwine' ), 'social_share_settings' );
    //share google plus
    pbr_social_config( $wp_customize, 'google_share_blog', esc_html__( 'Share google plus ', 'royanwine' ), 'social_share_settings' );
    //share pinterest
    pbr_social_config( $wp_customize, 'pinterest_share_blog', esc_html__( 'Share pinterest ', 'royanwine' ), 'social_share_settings' );
    //share mail
    pbr_social_config( $wp_customize, 'mail_share_blog', esc_html__( 'Share mail ', 'royanwine' ), 'social_share_settings' );


    /******************************************************************
     * Navigation
     ******************************************************************/

    # Sticky Top Bar Option
    $wp_customize->add_setting( 'pbr_theme_options[verticalmenu]', array(
        'capability'        => 'edit_theme_options',
        'type'              => 'option',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'pbr_theme_options[verticalmenu]', array(
        'settings' => 'pbr_theme_options[verticalmenu]',
        'label'    => esc_html__( 'Vertical Megamenu', 'royanwine' ),
        'section'  => 'nav',
        'type'     => 'select',
        'choices'  => royanwine_fnc_get_menugroups(),
    ) );


    # Sticky Top Bar Option
    $wp_customize->add_setting( 'pbr_theme_options[megamenu-is-sticky]', array(
        'capability'        => 'edit_theme_options',
        'type'              => 'option',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'pbr_theme_options[megamenu-is-sticky]', array(
        'settings'  => 'pbr_theme_options[megamenu-is-sticky]',
        'label'     => esc_html__( 'Sticky Top Bar', 'royanwine' ),
        'section'   => 'nav',
        'type'      => 'checkbox',
        'transport' => 4,
    ) );

    $wp_customize->add_setting( 'pbr_theme_options[megamenu-duration]', array(
        'type'              => 'option',
        'capability'        => 'manage_options',
        'default'           => '300',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'pbr_theme_options[megamenu-duration]', array(
        'label'   => esc_html__( 'Megamenu Duration', 'royanwine' ),
        'section' => 'nav',
        'type'    => 'text',
    ) );

    /*****************************************************************
     * Front Page Settings Panel
     *****************************************************************/
    $wp_customize->add_section( 'static_front_page', array(
        'title'       => esc_html__( 'Front Page Settings', 'royanwine' ),
        'priority'    => 120,
        'description' => esc_html__( 'Your theme supports a static front page.', 'royanwine' ),
    ) );

    $wp_customize->add_setting( 'pbr_theme_options[sidebar_position]', array(
        'default'           => 'left',
        'capability'        => 'edit_theme_options',
        'type'              => 'option',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'pbr_theme_options[sidebar_position]', array(
        'type'     => 'radio',
        'label'    => 'Sidebar Position',
        'section'  => 'static_front_page',
        'priority' => 1,
        'choices'  => array(
            'left'  => 'Left',
            'right' => 'Right',
        ),
    ) );

    $wp_customize->add_setting( 'show_on_front', array(
        'default'           => get_option( 'show_on_front' ),
        'capability'        => 'manage_options',
        'type'              => 'option',
        //  'theme_supports' => 'static-front-page',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'show_on_front', array(
        'label'   => esc_html__( 'Front page displays', 'royanwine' ),
        'section' => 'static_front_page',
        'type'    => 'radio',
        'choices' => array(
            'posts' => esc_html__( 'Your latest posts', 'royanwine' ),
            'page'  => esc_html__( 'A static page', 'royanwine' ),
        ),
    ) );

    $wp_customize->add_setting( 'page_on_front', array(
        'type'              => 'option',
        'capability'        => 'manage_options',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'page_on_front', array(
        'label'   => esc_html__( 'Front page', 'royanwine' ),
        'section' => 'static_front_page',
        'type'    => 'dropdown-pages',
    ) );

    $wp_customize->add_setting( 'page_for_posts', array(
        'type'              => 'option',
        'capability'        => 'manage_options',
        //  'theme_supports' => 'static-front-page',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'page_for_posts', array(
        'label'   => esc_html__( 'Posts page', 'royanwine' ),
        'section' => 'static_front_page',
        'type'    => 'dropdown-pages',
    ) );


    /* 
     /*****************************************************************
     * Front Page Settings Panel
     *****************************************************************/
    $wp_customize->add_section( 'pages_setting', array(
        'title'       => esc_html__( 'Pages Settings', 'royanwine' ),
        'priority'    => 120,
        'description' => esc_html__( 'Your theme supports a static front page.', 'royanwine' ),
    ) );


    $wp_customize->add_setting( 'pbr_theme_options[404_post]', array(
        'type'              => 'option',
        'capability'        => 'manage_options',
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field'
        //  'theme_supports' => 'static-front-page',
    ) );

    $wp_customize->add_control( 'pbr_theme_options[404_post]', array(
        'label'   => esc_html__( '404 Page', 'royanwine' ),
        'section' => 'pages_setting',
        'type'    => 'dropdown-pages',
    ) );

    //
}


function pbr_social_config($wp_customize, $id, $name_social, $section) {
    $wp_customize->add_setting( 'pbr_theme_options[' . $id . ']', array(
        'capability'        => 'edit_theme_options',
        'type'              => 'option',
        'default'           => 1,
        'checked'           => 1,
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'pbr_theme_options[' . $id . ']', array(
        'settings'  => 'pbr_theme_options[' . $id . ']',
        'label'     => $name_social,
        'section'   => $section,
        'type'      => 'checkbox',
        'transport' => 4,
    ) );
}


add_action( 'customize_register', 'royanwine_fnc_blog_settings' );
function royanwine_fnc_blog_settings($wp_customize) {

    $wp_customize->add_panel( 'panel_blog', array(
        'priority'       => 80,
        'capability'     => 'edit_theme_options',
        'theme_supports' => '',
        'title'          => esc_html__( 'Blog & Post', 'royanwine' ),
        'description'    => esc_html__( 'Make default setting for page, general', 'royanwine' ),
    ) );

    /**
     * General Setting
     */
    $wp_customize->add_section( 'blog_general_settings', array(
        'priority'       => 10,
        'capability'     => 'edit_theme_options',
        'theme_supports' => '',
        'title'          => esc_html__( 'General Setting', 'royanwine' ),
        'description'    => '',
        'panel'          => 'panel_blog',
    ) );

    /**
     * Archive Setting
     */
    $wp_customize->add_section( 'archive_general_settings', array(
        'priority'       => 11,
        'capability'     => 'edit_theme_options',
        'theme_supports' => '',
        'title'          => esc_html__( 'Archive & Categgory Setting', 'royanwine' ),
        'description'    => '',
        'panel'          => 'panel_blog',
    ) );

    $wp_customize->add_setting( 'pbr_theme_options[blog-archive-column]', array(
        'capability'        => 'edit_theme_options',
        'type'              => 'option',
        'default'           => '1',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'pbr_theme_options[blog-archive-column]', array(
        'label'   => esc_html__( 'Select column', 'royanwine' ),
        'section' => 'archive_general_settings',
        'type'    => 'select',
        'choices' => array(
            '1' => esc_html__( '1 column', 'royanwine' ),
            '2' => esc_html__( '2 columns', 'royanwine' ),
            '3' => esc_html__( '3 columns', 'royanwine' ),
            '4' => esc_html__( '4 columns', 'royanwine' ),
            '6' => esc_html__( '6 columns', 'royanwine' ),
        ),
    ) );

    ///  Archive layout setting
    $wp_customize->add_setting( 'pbr_theme_options[blog-archive-layout]', array(
        'capability'        => 'edit_theme_options',
        'type'              => 'option',
        'default'           => 'mainright',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( new Royanwine_Layout_DropDown( $wp_customize, 'pbr_theme_options[blog-archive-layout]', array(
        'settings' => 'pbr_theme_options[blog-archive-layout]',
        'label'    => esc_html__( 'Archive Layout', 'royanwine' ),
        'section'  => 'archive_general_settings',
        'priority' => 1,

    ) ) );


    $wp_customize->add_setting( 'pbr_theme_options[blog-archive-left-sidebar]', array(
        'capability'        => 'edit_theme_options',
        'type'              => 'option',
        'default'           => 'blog-sidebar-left',
        'sanitize_callback' => 'sanitize_text_field',
    ) );


    $wp_customize->add_control( new Royanwine_Sidebar_DropDown( $wp_customize, 'pbr_theme_options[blog-archive-left-sidebar]', array(
        'settings' => 'pbr_theme_options[blog-archive-left-sidebar]',
        'label'    => esc_html__( 'Archive Left Sidebar', 'royanwine' ),
        'section'  => 'archive_general_settings',
        'priority' => 2,
    ) ) );

    ///
    $wp_customize->add_setting( 'pbr_theme_options[blog-archive-right-sidebar]', array(
        'capability'        => 'edit_theme_options',
        'type'              => 'option',
        'default'           => 'blog-sidebar-right',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( new Royanwine_Sidebar_DropDown( $wp_customize, 'pbr_theme_options[blog-archive-right-sidebar]', array(
        'settings' => 'pbr_theme_options[blog-archive-right-sidebar]',
        'label'    => esc_html__( 'Archive Right Sidebar', 'royanwine' ),
        'section'  => 'archive_general_settings',
        'priority' => 2,
    ) ) );

    /**
     * Single post Setting
     */
    $wp_customize->add_section( 'blog_single_settings', array(
        'priority'       => 12,
        'capability'     => 'edit_theme_options',
        'theme_supports' => '',
        'title'          => esc_html__( 'Single post Setting', 'royanwine' ),
        'description'    => '',
        'panel'          => 'panel_blog',
    ) );


    $wp_customize->add_setting( 'pbr_theme_options[blog-show-share-post]', array(
        'capability'        => 'edit_theme_options',
        'type'              => 'option',
        'default'           => 0,
        'checked'           => 0,
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'pbr_theme_options[blog-show-share-post]', array(
        'settings'  => 'pbr_theme_options[blog-show-share-post]',
        'label'     => esc_html__( 'Show share post', 'royanwine' ),
        'section'   => 'blog_single_settings',
        'type'      => 'checkbox',
        'transport' => 4,
    ) );

    $wp_customize->add_setting( 'pbr_theme_options[blog-show-related-post]', array(
        'capability'        => 'edit_theme_options',
        'type'              => 'option',
        'default'           => 1,
        'checked'           => 1,
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'pbr_theme_options[blog-show-related-post]', array(
        'settings'  => 'pbr_theme_options[blog-show-related-post]',
        'label'     => esc_html__( 'Show related post', 'royanwine' ),
        'section'   => 'blog_single_settings',
        'type'      => 'checkbox',
        'transport' => 4,
    ) );


    $wp_customize->add_setting( 'pbr_theme_options[blog-items-show]', array(
        'type'              => 'option',
        'default'           => 4,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( 'pbr_theme_options[blog-items-show]', array(
        'label'   => esc_html__( 'Number of related posts to show', 'royanwine' ),
        'section' => 'blog_single_settings',
        'type'    => 'select',
        'choices' => array(
            '2' => esc_html__( '2 posts', 'royanwine' ),
            '3' => esc_html__( '3 posts', 'royanwine' ),
            '4' => esc_html__( '4 posts', 'royanwine' ),
            '6' => esc_html__( '6 posts', 'royanwine' ),
        ),
    ) );


    ///  single layout setting
    $wp_customize->add_setting( 'pbr_theme_options[blog-single-layout]', array(
        'capability'        => 'edit_theme_options',
        'type'              => 'option',
        'default'           => 'fullwidth',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( new Royanwine_Layout_DropDown( $wp_customize, 'pbr_theme_options[blog-single-layout]', array(
        'settings' => 'pbr_theme_options[blog-single-layout]',
        'label'    => esc_html__( 'Single Blog Layout', 'royanwine' ),
        'section'  => 'blog_single_settings',
    ) ) );


    $wp_customize->add_setting( 'pbr_theme_options[blog-single-left-sidebar]', array(
        'capability'        => 'edit_theme_options',
        'type'              => 'option',
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( new Royanwine_Sidebar_DropDown( $wp_customize, 'pbr_theme_options[blog-single-left-sidebar]', array(
        'settings' => 'pbr_theme_options[blog-single-left-sidebar]',
        'label'    => esc_html__( 'Single blog Left Sidebar', 'royanwine' ),
        'section'  => 'blog_single_settings',
    ) ) );

    $wp_customize->add_setting( 'pbr_theme_options[blog-single-right-sidebar]', array(
        'capability'        => 'edit_theme_options',
        'type'              => 'option',
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
    ) );

    $wp_customize->add_control( new Royanwine_Sidebar_DropDown( $wp_customize, 'pbr_theme_options[blog-single-right-sidebar]', array(
        'settings' => 'pbr_theme_options[blog-single-right-sidebar]',
        'label'    => esc_html__( 'Single blog Right Sidebar', 'royanwine' ),
        'section'  => 'blog_single_settings',
    ) ) );


}


/**
 * Sanitize the Featured Content layout value.
 *
 * @since Royanwine 1.0
 *
 * @param string $layout Layout type.
 *
 * @return string Filtered layout type (grid|slider).
 */
function royanwine_fnc_sanitize_layout($layout) {
    if (!in_array( $layout, array( 'grid', 'slider' ) )){
        $layout = 'grid';
    }

    return $layout;
}

/**
 * Bind JS handlers to make Customizer preview reload changes asynchronously.
 *
 * @since Royanwine 1.0
 */
function royanwine_fnc_customize_preview_js() {
    wp_enqueue_script( 'royanwine_fnc_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20131205', true );
}

add_action( 'customize_preview_init', 'royanwine_fnc_customize_preview_js' );

/**
 * Add contextual help to the Themes and Post edit screens.
 *
 * @since Royanwine 1.0
 */
function royanwine_fnc_contextual_help() {
    if ('admin_head-edit.php' === current_filter() && 'post' !== $GLOBALS['typenow']){
        return;
    }

    get_current_screen()->add_help_tab( array(
        'id'      => 'royanwine',
        'title'   => esc_html__( 'Royanwine', 'royanwine' ),
        'content' =>
            '<ul>' .
            '<li>' . sprintf( esc_html__( 'The home page features your choice of up to 6 posts prominently displayed in a grid or slider, controlled by a <a href="%1$s">tag</a>; you can change the tag and layout in <a href="%2$s">Appearance &rarr; Customize</a>. If no posts match the tag, <a href="%3$s">sticky posts</a> will be displayed instead.', 'royanwine' ), esc_url( add_query_arg( 'tag', _x( 'featured', 'featured content default tag slug', 'royanwine' ), admin_url( 'edit.php' ) ) ), admin_url( 'customize.php' ), admin_url( 'edit.php?show_sticky=1' ) ) . '</li>' .
            '<li>' . sprintf( esc_html__( 'Enhance your site design by using <a href="%s">Featured Images</a> for posts you&rsquo;d like to stand out (also known as post thumbnails). This allows you to associate an image with your post without inserting it. Royanwine uses featured images for posts and pages&mdash;above the title&mdash;and in the Featured Content area on the home page.', 'royanwine' ), 'https://codex.wordpress.org/Post_Thumbnails#Setting_a_Post_Thumbnail' ) . '</li>' .
            '<li>' . sprintf( esc_html__( 'For an in-depth tutorial, and more tips and tricks, visit the <a href="%s">Royanwine documentation</a>.', 'royanwine' ), 'https://codex.wordpress.org/royanwine' ) . '</li>' .
            '</ul>',
    ) );
}

add_action( 'admin_head-themes.php', 'royanwine_fnc_contextual_help' );
add_action( 'admin_head-edit.php', 'royanwine_fnc_contextual_help' );
