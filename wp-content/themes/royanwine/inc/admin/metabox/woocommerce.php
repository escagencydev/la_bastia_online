<?php
/**
 * Register meta boxes
 *
 * Remember to change "your_prefix" to actual prefix in your project
 *
 * @param array $meta_boxes List of meta boxes
 *
 * @return array
 */
function royanwine_func_register_woocommerce_meta_boxes( $meta_boxes )
{
	$prefix = 'royanwine_product_';

	
	// 1st meta box
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'standard',
		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => esc_html__( 'Product Information', 'royanwine' ),
		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array('product' ),
		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'normal',
		// Order of meta box: high (default), low. Optional.
		'priority'   => 'low',
		// Auto save: true, false (default). Optional.
		'autosave'   => true,
		// List of meta fields
		'fields'     => array(
			array(
				'name' => esc_html__( 'Year', 'royanwine' ),
				'id'   => "{$prefix}year",
				'type' => 'text',
				'std'  => '',
			),
		 	array(
				'name' => esc_html__( 'Image Gallery', 'royanwine' ),
				'id'   => "{$prefix}galleries",
				'type' => 'file_advanced',
				'std'  => '',
			),
			array(
				'name' => esc_html__( 'Product Features', 'royanwine' ),
				'id'   => "{$prefix}features",
				'type' => 'wysiwyg',
				'std'  => '',
			),
		)
	);	
 	
	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'royanwine_func_register_woocommerce_meta_boxes' , 102 );