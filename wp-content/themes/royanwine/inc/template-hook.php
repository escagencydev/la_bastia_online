<?php 
/**
 * Remove javascript and css files not use
 */


/**
 * Hoo to top bar layout
 */
function royanwine_fnc_topbar_layout(){
	$layout = royanwine_fnc_get_header_layout();
	if(royanwine_fnc_theme_options('topbar', true)){
		get_template_part( 'page-templates/parts/topbar', $layout );
	}
	get_template_part( 'page-templates/parts/topbar', 'mobile' );
}

add_action( 'royanwine_template_header_before', 'royanwine_fnc_topbar_layout' );

/**
 * Hook to select header layout for archive layout
 */
function royanwine_fnc_get_header_layout( $layout='' ){
	global $post; 

	$layout = $post && get_post_meta( $post->ID, 'royanwine_header_layout', 1 ) ? get_post_meta( $post->ID, 'royanwine_header_layout', 1 ) : royanwine_fnc_theme_options( 'headerlayout' );
	 
 	if( $layout ){
 		return trim( $layout );
 	}elseif ( $layout = royanwine_fnc_theme_options('header_skin','') ){
 		return trim( $layout );
 	}

	return $layout;
} 

add_filter( 'royanwine_fnc_get_header_layout', 'royanwine_fnc_get_header_layout' );

/**
 * Hook to select header layout for archive layout
 */
function royanwine_fnc_get_footer_profile( $profile='default' ){

	global $post; 

	$profile =  $post? get_post_meta( $post->ID, 'royanwine_footer_profile', 1 ):null ;

 	if( $profile ){
 		return trim( $profile );
 	}elseif ( $profile = royanwine_fnc_theme_options('footer-style', $profile ) ){
 		return trim( $profile );
 	}

	return $profile;
} 

add_filter( 'royanwine_fnc_get_footer_profile', 'royanwine_fnc_get_footer_profile' );


/**
 * Render Custom Css Renderig by Visual composer
 */
if ( !function_exists( 'royanwine_fnc_print_style_footer' ) ) {
	function royanwine_fnc_print_style_footer(){
		$footer =  royanwine_fnc_get_footer_profile( 'default' );
		if($footer!='default'){
			$shortcodes_custom_css = get_post_meta( $footer, '_wpb_shortcodes_custom_css', true );
			if ( ! empty( $shortcodes_custom_css ) ) {
				echo '<style>
					'.$shortcodes_custom_css.'
					</style>';
			}
		}
	}
	add_action('wp_head','royanwine_fnc_print_style_footer', 18);
}


/**
 * Hook to show breadscrumbs
 */
function wpopal_fnc_render_breadcrumbs(){
	
	global $post;

	if( is_object($post) ){
		$disable = get_post_meta( $post->ID, 'royanwine_disable_breadscrumb', 1 );
		if(  $disable || is_front_page() ){
			return true; 
		}
		$bgimage = get_post_meta( $post->ID, 'royanwine_image_breadscrumb', 1 );
		$color 	 = get_post_meta( $post->ID, 'royanwine_color_breadscrumb', 1 );
		$bgcolor = get_post_meta( $post->ID, 'royanwine_bgcolor_breadscrumb', 1 );
		$style = array();
		if( $bgcolor  ){
			$style[] = 'background-color:'.$bgcolor;
		}
		if( $bgimage  ){ 
			$style[] = 'background-image:url(\''.wp_get_attachment_url($bgimage).'\')';
		}else{
            $bgimage = royanwine_fnc_theme_options( 'breadcrumb-bg' );
            $style[] =  $bgimage  ? 'background-image:url(\''.$bgimage.'\')' : "";
        }

		if( $color  ){ 
			$style[] = 'color:'.$color;
		}

		$estyle = !empty($style)? 'style="'.implode(";", $style).'"':"";
	}else {
		$estyle = ''; 
	}

	$parallax = royanwine_fnc_theme_options('parallax-breadcrumb') ? 'has-parallax' : 'no-parallax';
	
	echo '<section id="pbr-breadscrumb" class="pbr-breadscrumb '.$parallax.' " '.$estyle.'><div class="container">';
			royanwine_fnc_breadcrumbs();
	echo '</div></section>';

}

add_action( 'royanwine_template_main_before', 'wpopal_fnc_render_breadcrumbs' );

 
/**
 * Main Container
 */

function royanwine_template_main_container_class( $class ){
	global $post; 
	global $royanwine_wpopconfig;

	if(is_object($post)){
		$layoutcls = get_post_meta( $post->ID, 'royanwine_enable_fullwidth_layout', 1 );
		if( $layoutcls ) {
			$royanwine_wpopconfig['layout'] = 'fullwidth';
			return 'container-fluid';
		}
	}
	
	return $class;
}
add_filter( 'royanwine_template_main_container_class', 'royanwine_template_main_container_class', 1 , 1  );
add_filter( 'royanwine_template_main_content_class', 'royanwine_template_main_container_class', 1 , 1  );



function royanwine_template_footer_before(){
	return get_sidebar( 'newsletter' );
}

add_action( 'royanwine_template_footer_before', 'royanwine_template_footer_before' );


/**
 * Get Configuration for Page Layout
 *
 */
function royanwine_fnc_get_page_sidebar_configs( $configs='' ){

	global $post; 

	$left  	=  get_post_meta( $post->ID, 'royanwine_leftsidebar', 1 );
	$right 	=  get_post_meta( $post->ID, 'royanwine_rightsidebar', 1 );
	$layout =  get_post_meta( $post->ID, 'royanwine_page_layout', 1 );

	return royanwine_fnc_get_layout_configs($layout, $left, $right, $configs );
}

add_filter( 'royanwine_fnc_get_page_sidebar_configs', 'royanwine_fnc_get_page_sidebar_configs', 1, 1 );


function royanwine_fnc_get_single_sidebar_configs( $configs='' ){

	global $post; 

	$layout =  royanwine_fnc_theme_options( 'blog-single-layout', 'fullwidth');
	$left  	=  royanwine_fnc_theme_options( 'blog-single-left-sidebar', 'blog-sidebar-left');
	$right 	=  royanwine_fnc_theme_options( 'blog-single-right-sidebar', 'blog-sidebar-right');

	return royanwine_fnc_get_layout_configs($layout, $left, $right, $configs );
}

add_filter( 'royanwine_fnc_get_single_sidebar_configs', 'royanwine_fnc_get_single_sidebar_configs', 1, 1 );

function royanwine_fnc_get_archive_sidebar_configs( $configs='' ){

	$left  	=  royanwine_fnc_theme_options( 'blog-archive-left-sidebar', 'blog-sidebar-left');
	$right 	=  royanwine_fnc_theme_options( 'blog-archive-right-sidebar', 'blog-sidebar-right');
	$layout =  royanwine_fnc_theme_options( 'blog-archive-layout', 'fullwidth');
 	
	return royanwine_fnc_get_layout_configs($layout, $left, $right, $configs);
}

add_filter( 'royanwine_fnc_get_archive_sidebar_configs', 'royanwine_fnc_get_archive_sidebar_configs', 1, 1 );
add_filter( 'royanwine_fnc_get_category_sidebar_configs', 'royanwine_fnc_get_archive_sidebar_configs', 1, 1 );

function royanwine_fnc_sidebars_others_configs(){
	
	global $royanwine_page_layouts;
	
	return $royanwine_page_layouts;
}

add_filter("royanwine_fnc_sidebars_others_configs", "royanwine_fnc_sidebars_others_configs");


function royanwine_fnc_get_layout_configs($layout, $left, $right, $configs= array()){
	
    switch ($layout) {
    
	    // Two Sidebar
	    case 'leftmainright':
	    	$configs['sidebars'] = array(
	    		'left'  => array(
	    			'show'  	=> true, 
	    			'sidebar' 	=> $left, 
	    			'active'  	=> is_active_sidebar( $left ), 
	    			'class' 	=> 'col-lg-3 col-md-3 col-xs-12'
				),
				'right' => array(
					'show'  	=> true,
					'sidebar' 	=> $right,
					'active' 	=> is_active_sidebar( $right ),
					'class' 	=> 'col-xs-12 col-md-3'
				)
			);
			$configs['main'] = array('class' => 'col-xs-12 col-md-6');
	    break;

	    //One Sidebar Right
	    case 'mainright':
	        $configs['sidebars'] = array(
	    		'left'  => array('show'  	=> false),
				'right' => array(
					'show'  	=> true,
					'sidebar' 	=> $right,
					'active' 	=> is_active_sidebar( $right ),
					'class' 	=> 'col-xs-12 col-md-3'
				)
			);
			$configs['main'] = array('class' => 'col-xs-12 col-md-9');
	    break;

	    // One Sidebar Left
	    case 'leftmain':
	        $configs['sidebars'] = array(
	    		'left'  => array(
	    			'show'  	=> true, 
	    			'sidebar' 	=> $left, 
	    			'active'  	=> is_active_sidebar( $left ), 
	    			'class' 	=> 'col-xs-12 col-md-3'
				),
				'right' => array('show'  	=> false)
			);
			$configs['main'] = array('class' => 'col-xs-12 col-md-9');
	    break;

	    // Fullwidth
	    default:
	        $configs['sidebars'] = array(
	    		'left'  => array('show'  	=> false),
				'right' => array('show'  	=> false )
			);
			$configs['main'] = array('class' => 'col-xs-12 col-md-12');
	        break;
	    }

    return $configs;
}

if(!function_exists('royanwine_fnc_related_post')){
    function royanwine_fnc_related_post( $relate_count = 4, $posttype = 'post', $taxonomy = 'category' ){
      
        $terms = get_the_terms( get_the_ID(), $taxonomy );
        $termids =array();

        if($terms){
            foreach($terms as $term){
                $termids[] = $term->term_id;
            }
        }

        $args = array(
            'post_type' => $posttype,
            'posts_per_page' => $relate_count,
            'post__not_in' => array( get_the_ID() ),
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => $taxonomy,
                    'field' => 'id',
                    'terms' => $termids,
                    'operator' => 'IN'
                )
            )
        );
        $template_name = 'related-'.$posttype.'.php';

        $relates = new WP_Query( $args );

        if (is_file(get_template_directory().'/page-templates/' . $template_name)) {
            include(get_template_directory().'/page-templates/' . $template_name);
        }
    }
}